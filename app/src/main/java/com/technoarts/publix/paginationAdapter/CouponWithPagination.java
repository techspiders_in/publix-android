package com.technoarts.publix.paginationAdapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.technoarts.publix.R;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.CouponPojo;

import java.util.ArrayList;

public class CouponWithPagination extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final int TYPE_MOVIE = 0;
    public final int TYPE_LOAD = 1;

    Context context;
    ArrayList<CouponPojo> data;
    onClickCoupon click;
    boolean isFromDrawer;

    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;

    public CouponWithPagination(Context context, ArrayList<CouponPojo> data, boolean isFromDrawer, onClickCoupon click) {
        this.context = context;
        this.data = data;
        this.isFromDrawer = isFromDrawer;
        this.click = click;
    }

    public interface onClickCoupon {
        void onClick(int position);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == TYPE_MOVIE) {
            return new MovieHolder(inflater.inflate(R.layout.coupon_adapter, parent, false));
        } else {
            return new LoadHolder(inflater.inflate(R.layout.row_load, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if (getItemViewType(position) == TYPE_MOVIE) {

            new SizeUtil(context).setLinearLayoutMatchWidth(((MovieHolder) holder).mainCard, 440, 10, 20, 20, 0, Gravity.CENTER);

            if (isFromDrawer) {
                ((MovieHolder) holder).applyButton.setVisibility(View.GONE);
            } else {
                ((MovieHolder) holder).applyButton.setVisibility(View.VISIBLE);
            }
            ((MovieHolder) holder).navigationTitle.setText(data.get(position).getCp_code());
            ((MovieHolder) holder).expireTv.setText(data.get(position).getCp_date());
            ((MovieHolder) holder).schemeTv.setText("get ₹" + data.get(position).getCp_price() + " off on every ₹" + data.get(position).getCp_min_price() + " & above");
            ((MovieHolder) holder).applyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.onClick(position);
                }
            });


        }
        //No else part needed as load holder doesn't bind any data
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).type == null)
            return TYPE_MOVIE;
        else if (data.get(position).type.equals("movie")) {
            return TYPE_MOVIE;
        } else {
            return TYPE_LOAD;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    /* VIEW HOLDERS */

    static class MovieHolder extends RecyclerView.ViewHolder {
        TextView navigationTitle, expireTv, schemeTv;
        CardView mainCard;
        LinearLayout applyButton;

        public MovieHolder(View itemView) {
            super(itemView);
            mainCard = itemView.findViewById(R.id.mainCard);
            navigationTitle = itemView.findViewById(R.id.navigationTitle);
            applyButton = itemView.findViewById(R.id.applyButton);
            expireTv = itemView.findViewById(R.id.expireTv);
            schemeTv = itemView.findViewById(R.id.schemeTv);

        }

    }

    static class LoadHolder extends RecyclerView.ViewHolder {
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}
