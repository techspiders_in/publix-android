package com.technoarts.publix.paginationAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RelatedWithPagination extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final int TYPE_MOVIE = 0;
    public final int TYPE_LOAD = 1;

    static Context context;
    ArrayList<ProductPojo> movies;
    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;
    List<String> imageList;

    onProductClick click;

    public RelatedWithPagination(Context context, ArrayList<ProductPojo> data, onProductClick click) {
        this.context = context;
        this.movies = data;
        this.click = click;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == TYPE_MOVIE) {
            return new MovieHolder(inflater.inflate(R.layout.related_product_adapter, parent, false));
        } else {
            return new LoadHolder(inflater.inflate(R.layout.row_load, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if (getItemViewType(position) == TYPE_MOVIE) {

            //new SizeUtil(context).setLinearLayout(((MovieHolder) holder).mainCard, 260, 350, 0, 5, 5, 0, Gravity.CENTER);
            //new SizeUtil(context).setLinearLayout(((MovieHolder) holder).profile_image, 260, 200, 0, 0, 0, 0, Gravity.CENTER);


            ((MovieHolder) holder).headingTv.setText(movies.get(position).getPro_name());
            ((MovieHolder) holder).description.setText(movies.get(position).getPro_desc());
            ((MovieHolder) holder).priceTv.setText("₹ " + movies.get(position).getPro_mrp_ammount());

            System.out.println("PRODUCR RELETED : " + movies.get(position).getPro_name());

            imageList = Arrays.asList(movies.get(position).getPro_img().split("\\s*,\\s*"));

            if (imageList.size() > 0) {
                Glide.with(context).load(imageList.get(0)).into(((MovieHolder) holder).profile_image);
            }

            ((MovieHolder) holder).mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.click(position);
                }
            });

        }
        //No else part needed as load holder doesn't bind any data
    }

    @Override
    public int getItemViewType(int position) {
        if (movies.get(position).type == null)
            return TYPE_MOVIE;
        else if (movies.get(position).type.equals("movie")) {
            return TYPE_MOVIE;
        } else {
            return TYPE_LOAD;
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    /* VIEW HOLDERS */

    static class MovieHolder extends RecyclerView.ViewHolder {
        TextView headingTv, description, priceTv;
        LinearLayout mainLayout;
        ImageView profile_image, alphaThumb;
        LinearLayout mainCard;

        public MovieHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            mainCard = itemView.findViewById(R.id.mainCard);
            headingTv = itemView.findViewById(R.id.headingTv);
            profile_image = itemView.findViewById(R.id.profile_image);
            alphaThumb = itemView.findViewById(R.id.alphaThumb);
            description = itemView.findViewById(R.id.description);
            priceTv = itemView.findViewById(R.id.priceTv);

        }

    }

    static class LoadHolder extends RecyclerView.ViewHolder {
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}
