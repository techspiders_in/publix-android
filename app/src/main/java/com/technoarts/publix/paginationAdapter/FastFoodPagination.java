package com.technoarts.publix.paginationAdapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.PojoDaily;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class FastFoodPagination extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    public final int TYPE_MOVIE = 0;
    public final int TYPE_LOAD = 1;

    static Context context;
    ArrayList<PojoDaily> movies;
    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;
    List<String> imageList;

    onProductClick click;

    public FastFoodPagination(Context context, ArrayList<PojoDaily> data, onProductClick click) {
        this.context = context;
        this.movies = data;
        this.click = click;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        LayoutInflater inflater = LayoutInflater.from(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

        if (viewType == TYPE_MOVIE) {
            return new MovieHolder(inflater.inflate(R.layout.product_adapter, parent, false));
        } else {
            return new LoadHolder(inflater.inflate(R.layout.row_load, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if (getItemViewType(position) == TYPE_MOVIE) {

            new SizeUtil(context).setLinearLayout(((ProductWithPagination.MovieHolder) holder).mainCard, 525, 650, 20, 10, 10, 0, Gravity.CENTER);
            new SizeUtil(context).setLinearLayout(((ProductWithPagination.MovieHolder) holder).profile_image, 525, 350, 0, 0, 0, 0, Gravity.CENTER);

            System.out.println("PRODUCT : original amount : " + movies.get(position).getPro_id());
            System.out.println("PRODUCT : sale amount : " + movies.get(position).getPro_mrp_ammount());

            ((ProductWithPagination.MovieHolder) holder).originalAmt.setPaintFlags(((ProductWithPagination.MovieHolder) holder).originalAmt.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            ((MovieHolder) holder).headingTv.setText(movies.get(position).getPro_name());
            ((MovieHolder) holder).originalAmt.setText("₹ " + movies.get(position).getPro_mrp_ammount());
            ((MovieHolder) holder).description.setText(movies.get(position).getPro_sale());
//            ((ProductWithPagination.MovieHolder) holder).priceTv.setText("₹ " + movies.get(position).getP());

            imageList = Arrays.asList(movies.get(position).getPro_img().split("\\s*,\\s*"));

            if (imageList.size() > 0) {
                Glide.with(context).load(imageList.get(0)).into(((ProductWithPagination.MovieHolder) holder).profile_image);
            }

            ((ProductWithPagination.MovieHolder) holder).mainCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.click(position);
                }
            });

        }
        //No else part needed as load holder doesn't bind any data
    }
//
//    @Override
//    public int getItemViewType(int position) {
//        if (movies.get(position).type == null)
//            return TYPE_MOVIE;
//        else if (movies.get(position).type.equals("movie")) {
//            return TYPE_MOVIE;
//        } else {
//            return TYPE_LOAD;
//        }
//    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    /* VIEW HOLDERS */

    static class MovieHolder extends RecyclerView.ViewHolder {
        TextView headingTv, description, priceTv, originalAmt;
        LinearLayout mainLayout;
        ImageView profile_image, alphaThumb;
        CardView mainCard;


        public MovieHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            mainCard = itemView.findViewById(R.id.mainCard);
            headingTv = itemView.findViewById(R.id.headingTv);
            profile_image = itemView.findViewById(R.id.profile_image);
            alphaThumb = itemView.findViewById(R.id.alphaThumb);
            description = itemView.findViewById(R.id.description);
            priceTv = itemView.findViewById(R.id.priceTv);
            originalAmt = itemView.findViewById(R.id.originalAmt);
        }

    }

    static class LoadHolder extends RecyclerView.ViewHolder {
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}
