package com.technoarts.publix.paginationAdapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductWithPagination extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final int TYPE_MOVIE = 0;
    public final int TYPE_LOAD = 1;

    static Context context;
    ArrayList<ProductPojo> movies;
    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;
    List<String> imageList;

    onProductClick click;

    public ProductWithPagination(Context context, ArrayList<ProductPojo> data, onProductClick click) {
        this.context = context;
        this.movies = data;
        this.click = click;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == TYPE_MOVIE) {
            return new MovieHolder(inflater.inflate(R.layout.product_adapter, parent, false));
        } else {
            return new LoadHolder(inflater.inflate(R.layout.row_load, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if (getItemViewType(position) == TYPE_MOVIE) {

            new SizeUtil(context).setLinearLayout(((MovieHolder) holder).mainCard, 525, 600, 20, 10, 10, 0, Gravity.CENTER);
            new SizeUtil(context).setLinearLayout(((MovieHolder) holder).profile_image, 525, 350, 0, 0, 0, 0, Gravity.CENTER);

            System.out.println("PRODUCT : original amount : " + movies.get(position).getPublix_price1());
            System.out.println("PRODUCT : sale amount : " + movies.get(position).getPrice1());

            ((MovieHolder) holder).priceTv.setPaintFlags(((MovieHolder) holder).priceTv.getPaintFlags()
                    | Paint.STRIKE_THRU_TEXT_FLAG);

            ((MovieHolder) holder).headingTv.setText(movies.get(position).getPro_name());
            ((MovieHolder) holder).priceTv.setText(" ₹ " + movies.get(position).getPrice1());
//            ((MovieHolder) holder).description.setText(movies.get(position).getPro_sale());
            ((MovieHolder) holder).originalAmt.setText("₹ " + movies.get(position).getPublix_price1());

            imageList = Arrays.asList(movies.get(position).getPro_img().split("\\s*,\\s*"));

            if (TextUtils.isEmpty(movies.get(position).getPro_name())) {
                ((MovieHolder)holder).headingTv.setVisibility(View.GONE);
            } else {
                ((MovieHolder)holder).headingTv.setVisibility(View.VISIBLE);
            }
            if (TextUtils.isEmpty(movies.get(position).getPrice1())) {
                ((MovieHolder)holder).originalAmt.setVisibility(View.GONE);
            } else {
                ((MovieHolder)holder).originalAmt.setVisibility(View.VISIBLE);
            }
            if (TextUtils.isEmpty(movies.get(position).getPublix_price1())) {
                ((MovieHolder)holder).priceTv.setVisibility(View.GONE);
            } else {
                ((MovieHolder)holder).priceTv.setVisibility(View.VISIBLE);
            }
//            if (TextUtils.isEmpty(movies.get(position).getPro_sale())) {
//                ((MovieHolder)holder).description.setVisibility(View.GONE);
//            } else {
//                ((MovieHolder)holder).description.setVisibility(View.VISIBLE);
//            }

           try{
               if (imageList.size() > 0) {
                   Glide.with(context.getApplicationContext()).load(imageList.get(0))
                           .error(R.mipmap.ic_launcher).into(((MovieHolder) holder).profile_image);
               }
           }catch (Exception e){
               e.printStackTrace();
           }

            ((MovieHolder) holder).mainCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.click(position);
                }
            });

        }
        //No else part needed as load holder doesn't bind any data
    }

    @Override
    public int getItemViewType(int position) {
        if (movies.get(position).type == null)
            return TYPE_MOVIE;
        else if (movies.get(position).type.equals("movie")) {
            return TYPE_MOVIE;
        } else {
            return TYPE_LOAD;
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    /* VIEW HOLDERS */

    static class MovieHolder extends RecyclerView.ViewHolder {
        TextView headingTv, description, priceTv, originalAmt;
        LinearLayout mainLayout;
        ImageView profile_image, alphaThumb;
        CardView mainCard;


        public MovieHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            mainCard = itemView.findViewById(R.id.mainCard);
            headingTv = itemView.findViewById(R.id.headingTv);
            profile_image = itemView.findViewById(R.id.profile_image);
            alphaThumb = itemView.findViewById(R.id.alphaThumb);
//            description = itemView.findViewById(R.id.description);
            priceTv = itemView.findViewById(R.id.priceTv);
            originalAmt = itemView.findViewById(R.id.originalAmt);
        }

    }

    static class LoadHolder extends RecyclerView.ViewHolder {
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }


    public void filterList(ArrayList<ProductPojo> filterdNames) {
        this.movies = filterdNames;
        notifyDataSetChanged();
    }
}
