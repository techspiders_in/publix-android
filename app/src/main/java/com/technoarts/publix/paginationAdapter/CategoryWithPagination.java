package com.technoarts.publix.paginationAdapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.technoarts.publix.R;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.CategoryPojo;

import java.util.ArrayList;

public class CategoryWithPagination extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final int TYPE_MOVIE = 0;
    public final int TYPE_LOAD = 1;

    Context context;
    ArrayList<CategoryPojo> movies;
    onClicFilter click;

    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;

    public interface onClicFilter {
        void onClick(int pos);
    }


    public CategoryWithPagination(Context context, ArrayList<CategoryPojo> data, onClicFilter click) {
        this.context = context;
        this.movies = data;
        this.click = click;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == TYPE_MOVIE) {
            return new MovieHolder(inflater.inflate(R.layout.row_movie, parent, false));
        } else {
            return new LoadHolder(inflater.inflate(R.layout.row_load, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if (getItemViewType(position) == TYPE_MOVIE) {
            ((MovieHolder) holder).headingTv.setText(movies.get(position).getCat_name());
            new SizeUtil(context).setLinearLayout(((MovieHolder) holder).mainCard, 345, 345, 10, 0, 0, 0, Gravity.CENTER);
            new SizeUtil(context).setLinearLayout(((MovieHolder) holder).iconImage, 150, 150, 0, 0, 0, 0, Gravity.CENTER);


            Glide.with(context)
                    .applyDefaultRequestOptions(new RequestOptions()
                            .placeholder(R.drawable.launcher)
                            .error(R.drawable.launcher))
                    .load(movies.get(position).getIcon_img())
                    .into(((MovieHolder) holder).iconImage);


            ((MovieHolder) holder).mainCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.onClick(position);
                }
            });
        }
        //No else part needed as load holder doesn't bind any data
    }

    @Override
    public int getItemViewType(int position) {
        if (movies.get(position).type == null)
            return TYPE_MOVIE;
        else if (movies.get(position).type.equals("movie")) {
            return TYPE_MOVIE;
        } else {
            return TYPE_LOAD;
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    /* VIEW HOLDERS */

    static class MovieHolder extends RecyclerView.ViewHolder {
        TextView headingTv;
        ImageView iconImage;
        LinearLayout mainCard;

        public MovieHolder(View itemView) {
            super(itemView);
            headingTv = itemView.findViewById(R.id.headingTv);
            iconImage = itemView.findViewById(R.id.iconImage);
            mainCard = itemView.findViewById(R.id.mainCard);
        }

    }

    static class LoadHolder extends RecyclerView.ViewHolder {
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}
