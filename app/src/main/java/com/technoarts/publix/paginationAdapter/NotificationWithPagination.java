package com.technoarts.publix.paginationAdapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.NotificationVO;
import com.technoarts.publix.helpers.SizeUtil;

import java.util.ArrayList;

public class NotificationWithPagination extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final int TYPE_MOVIE = 0;
    public final int TYPE_LOAD = 1;

    static Context context;
    ArrayList<NotificationVO> movies;
    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;

    onProductClick click;

    public NotificationWithPagination(Context context, ArrayList<NotificationVO> data, onProductClick click) {
        this.context = context;
        this.movies = data;
        this.click = click;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == TYPE_MOVIE) {
            return new MovieHolder(inflater.inflate(R.layout.notification_adapter, parent, false));
        } else {
            return new LoadHolder(inflater.inflate(R.layout.row_load, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if (getItemViewType(position) == TYPE_MOVIE) {


            new SizeUtil(context).setLinearLayout(((MovieHolder) holder).mainCard, 1060, 220, 20, 0, 0, 0, Gravity.CENTER);
            new SizeUtil(context).setLinearLayout(((MovieHolder) holder).profile_image, 120, 120, 0, 10, 10, 0, Gravity.CENTER);


            ((MovieHolder) holder).headingTv.setText(movies.get(position).getTitle());
            ((MovieHolder) holder).description.setText(movies.get(position).getMessage());


            Glide.with(context)
                    .applyDefaultRequestOptions(new RequestOptions()
                            .placeholder(R.drawable.launcher)
                            .error(R.drawable.launcher))
                    .load(movies.get(position).getIconUrl())
                    .into(((MovieHolder) holder).profile_image);
//            Glide.with(context).load(movies.get(position).getIconUrl()).into(((MovieHolder) holder).profile_image);
            ((MovieHolder) holder).mainCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.click(position);
                }
            });
        }
        //No else part needed as load holder doesn't bind any data
    }

    @Override
    public int getItemViewType(int position) {
        if (movies.get(position).type == null)
            return TYPE_MOVIE;
        else if (movies.get(position).type.equals("movie")) {
            return TYPE_MOVIE;
        } else {
            return TYPE_LOAD;
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    /* VIEW HOLDERS */

    static class MovieHolder extends RecyclerView.ViewHolder {
        TextView headingTv, description;
        ImageView profile_image;
        CardView mainCard;

        public MovieHolder(View itemView) {
            super(itemView);
            mainCard = itemView.findViewById(R.id.mainCard);
            headingTv = itemView.findViewById(R.id.headingTv);
            profile_image = itemView.findViewById(R.id.profile_image);
            description = itemView.findViewById(R.id.description);

        }
    }

    static class LoadHolder extends RecyclerView.ViewHolder {
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}
