package com.technoarts.publix.asyncs;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.technoarts.publix.R;
import com.technoarts.publix.callback.onLoadImages;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoadMultiImages extends AsyncTask<Void, Void, Void> {

    Context mContext;
    onLoadImages call;
    String productId;
    ArrayList<String> productList;

    public LoadMultiImages(Context mContext, String productId, onLoadImages call) {
        this.mContext = mContext;
        this.call = call;
        this.productId = productId;
        productList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        loadPro();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    private void loadPro() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, mContext.getResources().getString(R.string.prefix_domain)+"ashokasta/askadmin/Products/multiple_image?pro_id=" + productId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("images");
                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject itemObj = (JSONObject) jsonArray.get(i);
                                String imaegs = jsonArray.getString(i);
                                productList.add(imaegs);
                            }

                            call.onClickImages(productList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("PRODUCT LIST : " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("PRODUCT LIST : " + error.getMessage());
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);
    }
}
