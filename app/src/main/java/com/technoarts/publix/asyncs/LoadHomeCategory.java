package com.technoarts.publix.asyncs;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.technoarts.publix.R;
import com.technoarts.publix.callback.onCategoryLoadComplete;
import com.technoarts.publix.pojo.CategoryPojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoadHomeCategory extends AsyncTask<Void, Void, Void> {

    Context mContext;
    onCategoryLoadComplete call;
    ArrayList<CategoryPojo> productList;

    public LoadHomeCategory(Context mContext, onCategoryLoadComplete call) {
        this.mContext = mContext;
        this.call = call;
        productList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        loadPro();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    private void loadPro() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                mContext.getResources().getString(R.string.prefix_domain) +
                "Maincategory/cat_json",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonObject = new JSONArray(response);
                            System.out.println("PRODUCT LIST : " + response);
//                            JSONArray jsonArray = jsonObject.getJSONArray("category");
                            for (int i = 0; i < jsonObject.length(); i++) {
                                JSONObject itemObj = (JSONObject) jsonObject.get(i);
                                CategoryPojo mProductPojo = new CategoryPojo(
                                        itemObj.getString("cat_id")
                                        , itemObj.getString("cat_name")
                                        , itemObj.getString("flag")
                                        , itemObj.getString("icon_img")
                                        , itemObj.getString("img")
                                        , itemObj.getString("deliverycharge")
                                        , false
                                );
                                productList.add(mProductPojo);
                            }
                            call.onComplete(productList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("PRODUCT LIST : " + e.getMessage());
                            call.onComplete(productList);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("PRODUCT LIST : " + error.getMessage());
                        call.onComplete(productList);
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);
    }
}
