package com.technoarts.publix.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.technoarts.publix.R;
import com.technoarts.publix.callback.onCouponLoad;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.pojo.CouponPojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class LoadCoupon extends AsyncTask<Void, Void, Void> {

    Context mContext;
    onCouponLoad call;
    String minPrice;
    ArrayList<CouponPojo> productList;

    public LoadCoupon(Context mContext, String minPrice, onCouponLoad call) {
        this.mContext = mContext;
        this.call = call;
        this.minPrice = minPrice;
        productList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        loadPro();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    private void loadPro() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, mContext.getResources().getString(R.string.prefix_domain)
                + "Coupon/json",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonObject = new JSONArray(response);
//                            System.out.println("PRODUCT LIST : " + response);
//                            JSONArray jsonArray = jsonObject.getJSONArray("coupon");
                            for (int i = 0; i < jsonObject.length(); i++) {
                                JSONObject itemObj = (JSONObject) jsonObject.get(i);
                                int minP = Integer.parseInt(minPrice);
//                                if (minP >= Integer.parseInt(itemObj.getString("cp_min_price"))) {
                                if (!postJson(
                                        new PrefHelper(mContext).
                                                get_STRING(Constant.checkout_user_id, ""),
                                        itemObj.getString("cp_id"))) {
                                    productList.add(new CouponPojo(
                                            itemObj.getString("cp_id"),
                                            itemObj.getString("cp_code"),
                                            itemObj.getString("pro_id"),
                                            itemObj.getString("cp_date"),
                                            itemObj.getString("cp_price"),
                                            itemObj.getString("cp_min_price")
                                    ));
                                }
//                                }
                            }
                            call.onLoadComplete(productList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            call.onLoadComplete(productList);
                            System.out.println("PRODUCT LIST : " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("PRODUCT LIST : " + error.getMessage());
                        call.onLoadComplete(productList);
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);
    }

    boolean is = false;

    public boolean postJson(String userid, String cpId) {

        RequestBody formBody = new FormBody.Builder()
                .add("user_id", userid /*"Jurassic@Park.com"*/)
                .add("coupon_id", cpId/*"90301171XX"*/)
                .build();

        final OkHttpClient client = new OkHttpClient();
        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url(mContext.getResources().getString(R.string.prefix_domain)
                        + "Coupon/check_coupon")
                .post(formBody)
                .build();
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    okhttp3.Response response = null;
                    try {
                        //getresponse();
                        response = client.newCall(request).execute();
                        String getresponse = response.body().string();
                        Log.e("response", getresponse);
                        JSONObject jsonObj = new JSONObject(getresponse);
                        int result = Integer.parseInt(jsonObj.getString("result"));
                        System.out.println("LOGIN : " + result);
                        if (result == 100) {
                            is = true;
                        } else {
                            is = false;
                        }

                    } catch (IOException e) {

                    }
                } catch (Exception e) {

                }
            }
        });

        thread.start();

        return is;
    }

}
