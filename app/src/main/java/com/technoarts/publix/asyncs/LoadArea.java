package com.technoarts.publix.asyncs;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.technoarts.publix.R;
import com.technoarts.publix.pojo.DeliveryAreaPojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoadArea extends AsyncTask<Void, Void, Void> {

    Context mContext;
    ArrayList<DeliveryAreaPojo> productList;
    onLoadComplete call;

    public interface onLoadComplete {
        void onComplete(ArrayList<DeliveryAreaPojo> data);
    }

    public LoadArea(Context mContext, onLoadComplete call) {
        this.mContext = mContext;
        this.call = call;
        productList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        loadPro();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    private void loadPro() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, mContext.getResources().getString(R.string.prefix_domain)
                + "Deliveryarea/json",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            System.out.println("PRODUCT LIST : " + response);
                            JSONArray jsonArray = new JSONArray(response);
                            productList.add(new DeliveryAreaPojo("Select Area...", "-1"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject itemObj = (JSONObject) jsonArray.get(i);
                                DeliveryAreaPojo mProductPojo = new DeliveryAreaPojo(
                                        itemObj.getString("areaname")
                                        , itemObj.getString("id")
                                );
                                productList.add(mProductPojo);
                            }
                            call.onComplete(productList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("PRODUCT LIST : " + e.getMessage());
                            call.onComplete(productList);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("PRODUCT LIST : " + error.getMessage());
                        call.onComplete(productList);
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);
    }
}
