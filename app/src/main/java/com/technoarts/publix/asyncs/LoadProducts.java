package com.technoarts.publix.asyncs;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.onProductLoadComplete;
import com.technoarts.publix.pojo.ProductPojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoadProducts extends AsyncTask<Void, Void, Void> {

    Context mContext;

    onProductLoadComplete call;
    ArrayList<ProductPojo> productList;

    public LoadProducts(Context mContext, onProductLoadComplete call) {
        this.mContext = mContext;
        this.call = call;
        productList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        loadPro();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    private void loadPro() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, mContext.getResources().getString(R.string.prefix_domain)+"ashokasta/askadmin/Products/json",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("PRODUCT LIST : " + response);
                            JSONArray jsonArray = jsonObject.getJSONArray("product");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject itemObj = (JSONObject) jsonArray.get(i);
                                ProductPojo mProductPojo = new ProductPojo(
                                        itemObj.getString("pro_id")
                                        , itemObj.getString("pro_name")
//                                        , itemObj.getString("pro_cat_id")
//                                        , itemObj.getString("cat_name")
                                        , itemObj.getString("car_id")
                                        , itemObj.getString("pro_desc")
                                        , itemObj.getString("pro_sale")
                                        , itemObj.getString("pro_amount")
                                        , itemObj.getString("pro_mrp_ammount")
                                        , itemObj.getString("pro_status")
                                        , itemObj.getString("date")
                                        , itemObj.getString("pro_img")
                                        , itemObj.getString("pro_size")
                                        , itemObj.getString("related_product")
                                        , itemObj.getString("name_price")
                                        , itemObj.getString("publix_price")
                                        , itemObj.getString("publix_price1")
                                        , itemObj.getString("publix_price2")
                                        , itemObj.getString("publix_price3")
                                        , itemObj.getString("name1")
                                        , itemObj.getString("name2")
                                        , itemObj.getString("name3")
                                        , itemObj.getString("price1")
                                        , itemObj.getString("price2")
                                        , itemObj.getString("price3")
                                        , itemObj.getString("price3")

                                );
                                productList.add(mProductPojo);
                            }
                            call.onLoadComplete(productList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("PRODUCT LIST : " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("PRODUCT LIST : " + error.getMessage());
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);
    }
}
