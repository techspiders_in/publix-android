package com.technoarts.publix.helpers;

import android.content.Context;

public class Constant {
    public static String USERNAME = "uName";
    public static String EMAIL = "email";
    public static String getcoupon = "coup", checkout_user_id = "userId", getgrandtotal = "totalAmt",
            getname = "aName", getpincode = "piniyo", getdropmsg = "msg",rate="rate",area="area",price="price",expcharge="expcharge";
    public static String isSocial = "social";
    public final static String BASE_URL = "https://thepublix.com/tpxadmin/";

    public static void logOut(Context mContext) {
        new PrefHelper(mContext).save_STRING(USERNAME, "");
        new PrefHelper(mContext).save_STRING(EMAIL, "");
        new PrefHelper(mContext).save_STRING(getcoupon, "0");
        new PrefHelper(mContext).save_STRING(checkout_user_id, "");
        new PrefHelper(mContext).save_STRING(getgrandtotal, "");
        new PrefHelper(mContext).save_STRING(getname, "");
        new PrefHelper(mContext).save_STRING(getpincode, "");
        new PrefHelper(mContext).save_STRING(getdropmsg, "");
        new PrefHelper(mContext).save_STRING(rate, "");
        new PrefHelper(mContext).save_BOOLEAN(getdropmsg, false);
    }


}
