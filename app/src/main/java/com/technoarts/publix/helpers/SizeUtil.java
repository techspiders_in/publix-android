package com.technoarts.publix.helpers;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;


public class SizeUtil {
    Context mContext;

    public SizeUtil(Context mContext) {
        this.mContext = mContext;
    }

    public void setLinearLayout(View v, int w, int h, int top, int left, int right, int bottom, int gravity) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                mContext.getResources().getDisplayMetrics().widthPixels * w / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * h / 1920);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.gravity = gravity;

        v.setLayoutParams(lp);
    }

    public void setLinearLayoutWrapHeight(View v, int w, int top, int left, int right, int bottom, int gravity) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                mContext.getResources().getDisplayMetrics().widthPixels * w / 1080,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.gravity = gravity;

        v.setLayoutParams(lp);
    }

    public void setRelativeLayoutWrapHeight(View v, int w, int top, int left, int right, int bottom, int gravity) {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                mContext.getResources().getDisplayMetrics().widthPixels * w / 1080,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.addRule(gravity);

        v.setLayoutParams(lp);
    }

    public void setLinearLayoutMatchHieght(View v, int w, int top, int left, int right, int bottom, int gravity) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                mContext.getResources().getDisplayMetrics().widthPixels * w / 1080,
                ViewGroup.LayoutParams.MATCH_PARENT);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.gravity = gravity;

        v.setLayoutParams(lp);
    }

    public void setLinearLayoutMatchWidth(View v, int h, int top, int left, int right, int bottom, int gravity) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                mContext.getResources().getDisplayMetrics().heightPixels * h / 1920);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.gravity = gravity;

        v.setLayoutParams(lp);
    }

    public void setLinearLayoutMargin(View v, int top, int left, int right, int bottom, int gravity) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.gravity = gravity;

        v.setLayoutParams(lp);
    }

    public void setLinearLayoutMarginM(View v, int top, int left, int right, int bottom, int gravity) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.gravity = gravity;

        v.setLayoutParams(lp);
    }

    public void setRelative(View v, int w, int h, int top, int left, int right, int bottom, int gravity) {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                mContext.getResources().getDisplayMetrics().widthPixels * w / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * h / 1920);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.addRule(gravity);

        v.setLayoutParams(lp);
    }

    public void setRelative(View v, int w, int h, int top, int left, int right, int bottom, int gravity, int g2) {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                mContext.getResources().getDisplayMetrics().widthPixels * w / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * h / 1920);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.addRule(gravity);
        lp.addRule(g2);

        v.setLayoutParams(lp);
    }

    public void setRelativeMatchWidth(View v, int h, int top, int left, int right, int bottom, int gravity) {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                mContext.getResources().getDisplayMetrics().heightPixels * h / 1920);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.addRule(gravity);

        v.setLayoutParams(lp);
    }

    public void setRelativeMatchHeight(View v, int w, int top, int left, int right, int bottom, int gravity) {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                mContext.getResources().getDisplayMetrics().heightPixels * w / 1920,
                ViewGroup.LayoutParams.MATCH_PARENT);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.addRule(gravity);

        v.setLayoutParams(lp);
    }

    public void setRelative(View v, int w, int top, int left, int right, int bottom, int gravity) {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                mContext.getResources().getDisplayMetrics().widthPixels * w / 1080,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.addRule(gravity);

        v.setLayoutParams(lp);
    }

    public void seScrollView(View v, int w, int top, int left, int right, int bottom, int gravity) {
        ScrollView.LayoutParams lp = new ScrollView.LayoutParams(
                mContext.getResources().getDisplayMetrics().widthPixels * w / 1080,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.gravity = gravity;

        v.setLayoutParams(lp);
    }


    public void setLinearLayoutWeighted(View v, int w, int h, int top, int left, int right, int bottom, int gravity) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                mContext.getResources().getDisplayMetrics().widthPixels * w / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * h / 1920);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.gravity = gravity;
        lp.weight = 1;

        v.setLayoutParams(lp);
    }


    public void setLinearLayoutWeightedWrapHieght(View v, int w, int h, int top, int left, int right, int bottom, int gravity) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                mContext.getResources().getDisplayMetrics().widthPixels * w / 1080,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);

        lp.gravity = gravity;
        lp.weight = 1;

        v.setLayoutParams(lp);
    }

    public void setRelativeLayoutWeightedWrapHieght(View v, int top, int left, int right, int bottom) {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        lp.setMargins(mContext.getResources().getDisplayMetrics().widthPixels * left / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * top / 1920,
                mContext.getResources().getDisplayMetrics().widthPixels * right / 1080,
                mContext.getResources().getDisplayMetrics().heightPixels * bottom / 1920);


        v.setLayoutParams(lp);
    }

}
