package com.technoarts.publix.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.ArrayMap;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.technoarts.publix.BaseFrag;
import com.technoarts.publix.R;
import com.technoarts.publix.activity.ProductDetailsAct;
import com.technoarts.publix.adapters.CAtegoryAdapter;
import com.technoarts.publix.callback.ProductApi;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.paginationAdapter.ProductWithPagination;
import com.technoarts.publix.pojo.ProductPojo;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragProduct extends BaseFrag {

    Context mContext;
    RecyclerView productListView;
    RecyclerView filterListView;
    CAtegoryAdapter mCAtegoryAdapter;
    ProductWithPagination mProductWithPagination;
    ProductApi api;
    ArrayList<ProductPojo> productList;
    ArrayList<String> duplicate;
    TextView emptyLogo;
    ImageView searchIcon;
    ProgressBar loadingBar;
    EditText searchEdt;
    LinearLayout searchBarLayout;

    @Override
    public int getLayoutResourceId() {
        return R.layout.frag_product;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        productList = new ArrayList<>();
        duplicate = new ArrayList<>();
        api = ServiceGenerator.createService(ProductApi.class);
    }

    @Override
    public void bindViews(View view) {
        productListView = view.findViewById(R.id.productListView);
        filterListView = view.findViewById(R.id.filterListView);
        loadingBar = view.findViewById(R.id.loadingBar);
        emptyLogo = view.findViewById(R.id.emptyLogo);
        searchIcon = view.findViewById(R.id.searchIcon);
        searchEdt = view.findViewById(R.id.searchEdt);
        searchBarLayout = view.findViewById(R.id.searchBarLayout);
    }

    @Override
    public void bindPixel() {
        new SizeUtil(mContext).setLinearLayout(searchIcon, 80, 80, 0, 30, 30, 0, Gravity.CENTER);
        new SizeUtil(mContext).setLinearLayoutMatchWidth(searchBarLayout, 160, 0, 0, 0, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        searchEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filterList(s.toString());
                System.out.println("PRODUCT ITEMS : product list size " + productList.size());
                if (mProductWithPagination != null)
                    mProductWithPagination.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void bindMethod() {
        productListView.setLayoutManager(new GridLayoutManager(mContext, 2));
        filterListView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        mProductWithPagination = null;
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                System.out.println("PRODUCT ITEMS : adapter object " + mProductWithPagination);
                mProductWithPagination = new ProductWithPagination(mContext, productList, new onProductClick() {
                    @Override
                    public void click(int position) {
                        Intent ip = new Intent(mContext, ProductDetailsAct.class);
                        ip.putExtra("SelectedProduct", (productList.get(position)));
                        startActivity(ip);
                    }
                });
                mProductWithPagination.setLoadMoreListener(new ProductWithPagination.OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {
                        productListView.post(new Runnable() {
                            @Override
                            public void run() {
                                int index = productList.size() - 1;
//                                loadMore(index);
                            }
                        });
                    }
                });
        productListView.setHasFixedSize(true);
                productListView.setAdapter(mProductWithPagination);
                load(0);
            }
        }, 2000);

    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void load(int index) {

        Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("cat_id", "-1");
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                (new JSONObject(jsonParams)).toString());

        System.out.println("VERIFY :  " + new JSONObject(jsonParams));



        Call<List<ProductPojo>> call = api.getProducts();
        call.enqueue(new Callback<List<ProductPojo>>() {
            @Override
            public void onResponse(Call<List<ProductPojo>> call, Response<List<ProductPojo>> response) {
                loadingBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        if (!duplicate.contains(response.body().get(i).getPro_id())) {
                            duplicate.add(response.body().get(i).getPro_id());
                            productList.add(response.body().get(i));
                            System.out.println("PRODUCT ITEMS : Response " + response.body().get(i).getPro_sale());
                        }
                    }

//                    Collections.sort(productList, new Comparator<ProductPojo>() {
//                        public int compare(ProductPojo v1, ProductPojo v2) {
//                            return v1.getPro_name().compareTo(v2.getPro_name());
//                        }
//                    });
                    mProductWithPagination.notifyDataChanged();
                } else {
//                    Log.e(TAG," Response Error "+String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
                if (isNetworkAvailable()) {
                    emptyLogo.setText("product not available yet!");
                } else {
                    emptyLogo.setText("You are offline, please try again letter");
                    openSupportContactDialog();
                }
                emptyLogo.setVisibility(View.VISIBLE);
                System.out.println("ERROR >>>> " + t.getMessage());
//                Log.e(TAG," Response Error "+t.getMessage());
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void loadMore(int index) {

        //add loading progress view
        productList.add(new ProductPojo("load"));
        mProductWithPagination.notifyItemInserted(productList.size() - 1);


        Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("cat_id", "-1");
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                (new JSONObject(jsonParams)).toString());

        System.out.println("VERIFY :  " + new JSONObject(jsonParams));



        Call<List<ProductPojo>> call = api.getProducts();
        call.enqueue(new Callback<List<ProductPojo>>() {
            @Override
            public void onResponse(Call<List<ProductPojo>> call, Response<List<ProductPojo>> response) {
                loadingBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    //remove loading view
                    productList.remove(productList.size() - 1);

                    List<ProductPojo> result = response.body();
                    if (result.size() > 0) {
                        //add loaded data
                        for (int i = 0; i < response.body().size(); i++) {
                            if (!duplicate.contains(response.body().get(i).getPro_id())) {
                                duplicate.add(response.body().get(i).getPro_id());
                                productList.add(response.body().get(i));
                            }
                        }
//                        Collections.sort(productList, new Comparator<ProductPojo>() {
//                            public int compare(ProductPojo v1, ProductPojo v2) {
//                                return v1.getPro_name().compareTo(v2.getPro_name());
//                            }
//                        });
                    } else {//result size 0 means there is no more data available at server
                        mProductWithPagination.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
                        Toast.makeText(mContext, "No More Data Available", Toast.LENGTH_LONG).show();
                    }
                    mProductWithPagination.notifyDataChanged();
                    if (productList.size() < 9) {
                        mProductWithPagination.setMoreDataAvailable(false);
                    }
                    //should call the custom method adapter.notifyDataChanged here to get the correct loading status
                } else {
//                    Log.e(TAG," Load More Response Error "+String.valueOf(response.code()));
                }

            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
//                Log.e(TAG," Load More Response Error "+t.getMessage());
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
                if (isNetworkAvailable()) {
                    emptyLogo.setText("product not available yet!");
                } else {
                    emptyLogo.setText("You are offline, please try again letter");
                    openSupportContactDialog();
                }
                emptyLogo.setVisibility(View.VISIBLE);
                System.out.println("ERROR >>>> " + t.getMessage());
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    void filterList(String text) {
        ArrayList<ProductPojo> temp = new ArrayList();
        try {
            for (ProductPojo d : productList) {
                if (d.getPro_name().toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d);
                }
            }
            mProductWithPagination.filterList(temp);

        } catch (Exception e) {
            if (temp != null && mProductWithPagination != null)
                mProductWithPagination.filterList(temp);
            System.out.println("PRODUCT ITEMS : filter error " + e.getMessage());
        }
    }
}
