package com.technoarts.publix.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.technoarts.publix.BaseFrag;
import com.technoarts.publix.R;
import com.technoarts.publix.activity.BakeryActivity;
import com.technoarts.publix.activity.CatItemAct;
import com.technoarts.publix.activity.DeliActivity;
import com.technoarts.publix.activity.FastFoodActivity;
import com.technoarts.publix.activity.ProductDetailsAct;
import com.technoarts.publix.adapters.BakeryAdapter;
import com.technoarts.publix.adapters.DashBoarDailyAdapter;
import com.technoarts.publix.adapters.FruitListAdapter;
import com.technoarts.publix.adapters.Slideradapter;
import com.technoarts.publix.callback.BakeryApi;
import com.technoarts.publix.callback.MoviesApi;
import com.technoarts.publix.callback.getDaily;
import com.technoarts.publix.callback.getFastFood;
import com.technoarts.publix.callback.getTopSlider;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.paginationAdapter.CategoryWithPagination;
import com.technoarts.publix.pojo.BannerModel;
import com.technoarts.publix.pojo.CategoryPojo;
import com.technoarts.publix.pojo.ProductPojo;
import com.technoarts.publix.pojo.Slidermodel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class FragCategory extends BaseFrag implements BaseSliderView.OnSliderClickListener {

    Context mContext;
    RecyclerView productListView;
    ArrayList<ProductPojo> productList = new ArrayList<>();
    ArrayList<CategoryPojo> categoryList = new ArrayList<>();
    ArrayList<String> duplicate;
    ProgressBar loadingBar;
    SliderLayout sliderLayout;
    private ArrayList<Slidermodel> catList1;
    //    private ArrayList<PojoDaily> dailyList;
//            , fastfoodList;
    private ArrayList<ProductPojo> dailyList, fastfoodList, bakerylist = new ArrayList<>();
    //        , ;
    HashMap<String, String> Hash_file_maps;
    private Slideradapter catadapter1;

    CategoryWithPagination mCategoryWithPagination;
    DashBoarDailyAdapter mDashBoarDailyAdapter;

    MoviesApi api;
    TextView emptyLogo;
    SizeUtil mSizeUtil;
    LinearLayout publixSpecialLayout;

    LinearLayout categoryLayout, specialviewAllLayout, specialViewAllButton, trendingLayout,
            trendingAllLayout, trendingAllButton, latestLayout, latestviewAllLayout, latestViewAllButton;
    getTopSlider apiTopBanner;
    ArrayList<String> topBanner;

    getDaily apiDaily;
    getFastFood apiFood;
    BakeryApi apiBakery;

    ArrayList<ProductPojo> fuitList;

    CardView spCard1, spCard2, spCard3;

    ImageView specialProductImage1, specialProductImage2, specialProductImage3;
    RecyclerView trendingListView;

    FruitListAdapter mFruitListAdapter;
    ProgressBar trendingloadingBar, loadingBarDaily, loadingBarBakery;

    LinearLayout retryDaily, retryFoodLayout, retrybakery,
            linearprice1, linearprice2, linearprice3, linearmrp1, linearmrp2, linearmrp3;
    RecyclerView dailyListView, bakeryListView;
    TextView tvproductname1, tvproductname2, tvproductname3,
            tvpublixPrice1, tvmrp1,
            tvpublixPrice2, tvmrp2,
            tvpublixPrice3, tvmrp3;
    BakeryAdapter bakeryAdapter;
    String pro1, pro2, pro3;
    String name1, name2, name3;

    @Override
    public int getLayoutResourceId() {
        return R.layout.frag_category;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
        productList = new ArrayList<>();
        catList1 = new ArrayList<>();
        categoryList = new ArrayList<>();
        duplicate = new ArrayList<>();
        dailyList = new ArrayList<>();
        fastfoodList = new ArrayList<>();
        bakerylist = new ArrayList<>();

        api = ServiceGenerator.createService(MoviesApi.class);
        apiDaily = ServiceGenerator.createService(getDaily.class);
        apiFood = ServiceGenerator.createService(getFastFood.class);
        apiTopBanner = ServiceGenerator.createService(getTopSlider.class);
        apiBakery = ServiceGenerator.createService(BakeryApi.class);

        fuitList = new ArrayList<>();
        topBanner = new ArrayList<>();
    }

    @Override
    public void bindViews(View view) {
        productListView = view.findViewById(R.id.productListView);
        loadingBar = view.findViewById(R.id.loadingBar);
        sliderLayout = view.findViewById(R.id.sliderLayout);
        emptyLogo = view.findViewById(R.id.emptyLogo);
        publixSpecialLayout = view.findViewById(R.id.publixSpecialLayout);
        categoryLayout = view.findViewById(R.id.categoryLayout);

        specialviewAllLayout = view.findViewById(R.id.specialviewAllLayout);
        trendingAllLayout = view.findViewById(R.id.trendingAllLayout);
        latestviewAllLayout = view.findViewById(R.id.latestviewAllLayout);


        specialViewAllButton = view.findViewById(R.id.specialViewAllButton);
        trendingAllButton = view.findViewById(R.id.trendingAllButton);
        latestViewAllButton = view.findViewById(R.id.latestViewAllButton);
        trendingLayout = view.findViewById(R.id.trendingLayout);
        bakeryListView = view.findViewById(R.id.bakeryListView);
        loadingBarBakery = view.findViewById(R.id.loadingBarBakery);
        retrybakery = view.findViewById(R.id.retrybakery);

        spCard1 = view.findViewById(R.id.spCard1);
        spCard2 = view.findViewById(R.id.spCard2);
        spCard3 = view.findViewById(R.id.spCard3);


        linearmrp1 = view.findViewById(R.id.linearmrp1);
        linearmrp2 = view.findViewById(R.id.linearmrp2);
        linearmrp3 = view.findViewById(R.id.linearmrp3);

        tvmrp1 = view.findViewById(R.id.tvMrp1);
        tvmrp2 = view.findViewById(R.id.tvMrp2);
        tvmrp3 = view.findViewById(R.id.tvMrp3);

        tvpublixPrice1 = view.findViewById(R.id.tvpublixPrice1);
        tvpublixPrice2 = view.findViewById(R.id.tvpublixPrice2);
        tvpublixPrice3 = view.findViewById(R.id.tvpublixPrice3);

        linearmrp1 = view.findViewById(R.id.linearmrp1);
        linearmrp2 = view.findViewById(R.id.linearmrp2);
        linearmrp3 = view.findViewById(R.id.linearmrp3);

        linearprice1 = view.findViewById(R.id.linearprice1);
        linearprice2 = view.findViewById(R.id.linearprice2);
        linearprice3 = view.findViewById(R.id.linearprice3);

        specialProductImage1 = view.findViewById(R.id.specialProductImage1);
        specialProductImage2 = view.findViewById(R.id.specialProductImage2);
        specialProductImage3 = view.findViewById(R.id.specialProductImage3);

        tvproductname1 = view.findViewById(R.id.tvproductname1);
        tvproductname2 = view.findViewById(R.id.tvproductname2);
        tvproductname3 = view.findViewById(R.id.tvproductname3);

        trendingListView = view.findViewById(R.id.trendingListView);
        trendingloadingBar = view.findViewById(R.id.trendingloadingBar);
        retryFoodLayout = view.findViewById(R.id.retryFoodLayout);
        latestLayout = view.findViewById(R.id.latestLayout);


        retryDaily = view.findViewById(R.id.retryDaily);
        dailyListView = view.findViewById(R.id.dailyListView);
        loadingBarDaily = view.findViewById(R.id.loadingBarDaily);

        latestViewAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), DeliActivity.class);
                mContext.startActivity(intent);
            }
        });
        trendingAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), FastFoodActivity.class);
                mContext.startActivity(intent);
            }
        });
        specialViewAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), BakeryActivity.class);
                mContext.startActivity(intent);
            }
        });
        loadBakery();


    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayout(sliderLayout, 1060, 620, 10, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(publixSpecialLayout, 1060, 1000, 30, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(latestLayout, 1060, 1300, 30, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(categoryLayout, 1060, 900, 20, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(trendingLayout, 1060, 620, 20, 0, 0, 0, Gravity.CENTER);

        mSizeUtil.setLinearLayout(specialViewAllButton, 190, 110, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(trendingAllButton, 190, 110, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(latestViewAllButton, 190, 110, 0, 30, 30, 0, Gravity.CENTER);

        mSizeUtil.setLinearLayout(spCard1, 600, 720, 0, 10, 10, 0, 0);
        mSizeUtil.setLinearLayout(spCard2, 400, 360, 0, 10, 10, 0, 0);
        mSizeUtil.setLinearLayout(spCard3, 400, 350, 10, 10, 10, 0, 0);


        mSizeUtil.setLinearLayout(specialProductImage1, 420, 520, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(specialProductImage2, 380, 190, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(specialProductImage3, 380, 190, 0, 0, 0, 0, Gravity.CENTER);

        mSizeUtil.setLinearLayoutMatchWidth(specialviewAllLayout, 160, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayoutMatchWidth(latestviewAllLayout, 160, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayoutMatchWidth(trendingAllLayout, 160, 0, 0, 0, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        retryDaily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadDaily();
            }
        });

        retryFoodLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFastFood();
            }
        });
        retrybakery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadBakery();
            }
        });

    }

    @Override
    public void bindMethod() {
        productListView.setLayoutManager(new GridLayoutManager(mContext, 3));
        dailyListView.setLayoutManager(new GridLayoutManager(mContext, 2));
        bakeryListView.setLayoutManager(new LinearLayoutManager(getContext()));

        mCategoryWithPagination = new CategoryWithPagination(mContext, categoryList, new CategoryWithPagination.onClicFilter() {
            @Override
            public void onClick(int pos) {
                Intent ip = new Intent(mContext, CatItemAct.class);
                ip.putExtra("CatName", categoryList.get(pos));
                startActivity(ip);
            }
        });
//        Collections.reverse(dailyList);
        mDashBoarDailyAdapter = new DashBoarDailyAdapter(mContext, dailyList, new DashBoarDailyAdapter.onClicFilter() {
            @Override
            public void onClick(int pos) {
                Intent ip = new Intent(mContext, ProductDetailsAct.class);
                ip.putExtra("SelectedProduct", (dailyList.get(pos)));
                startActivity(ip);
            }
        });

        dailyListView.setAdapter(mDashBoarDailyAdapter);

        loadDaily();

//         bakeryAdapter = new BakeryAdapter(mContext, bakerylist, new BakeryAdapter.onClicFilter() {
//             @Override
//             public void onClick(int pos) {
//                 Intent ip = new Intent(mContext, ProductDetailsAct.class);
//                 ip.putExtra("SelectedProduct", (bakerylist.get(pos)));
//                 startActivity(ip);
//             }
//        });
//
//        bakeryListView.setAdapter(bakeryAdapter);
//
//        loadBakery();

        mCategoryWithPagination.setLoadMoreListener(new CategoryWithPagination.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                productListView.post(new Runnable() {
                    @Override
                    public void run() {
                        int index = categoryList.size() - 1;
                        loadMore(index);
                    }
                });
            }
        });
        productListView.setHasFixedSize(true);
        productListView.setAdapter(mCategoryWithPagination);
        load(0);

        getSliderTop();

        trendingListView.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.HORIZONTAL, false));
//        Collections.reverse(fastfoodList);
        mFruitListAdapter = new FruitListAdapter(mContext, fastfoodList, new onProductClick() {
            @Override
            public void click(int position) {
                Intent ip = new Intent(mContext, ProductDetailsAct.class);
                ip.putExtra("SelectedProduct", fastfoodList.get(position));
                startActivity(ip);
            }
        });

        trendingListView.setAdapter(mFruitListAdapter);

        loadFastFood();

    }

    private void Slider(ArrayList<String> image) {
        HashMap<String, String> Hash_file_maps = new HashMap<String, String>();

        for (int i = 0; i < image.size(); i++) {
            Hash_file_maps.put("" + i, image.get(i));
        }

        for (String name : Hash_file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(mContext);
            textSliderView
                    .description("")
                    .image(Hash_file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(3000);
    }

    private void getSliderTop() {
        Call<List<BannerModel>> call = apiTopBanner.getBanners();
        call.enqueue(new Callback<List<BannerModel>>() {

            @Override
            public void onResponse(Call<List<BannerModel>> call, retrofit2.Response<List<BannerModel>> response) {
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        try {
                            topBanner.add(response.body().get(i).getImage());
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("MAIN BANNER : " + e.getMessage());
                        }

                    }

                    Slider(topBanner);
                }
            }

            @Override
            public void onFailure(Call<List<BannerModel>> call, Throwable t) {
                System.out.println("MAIN BANNER : " + t.getMessage());
            }
        });
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    private void load(int index) {
        Call<List<CategoryPojo>> call = api.getMovies();
        call.enqueue(new Callback<List<CategoryPojo>>() {


            @Override
            public void onResponse(Call<List<CategoryPojo>> call, retrofit2.Response<List<CategoryPojo>> response) {
                emptyLogo.setVisibility(View.GONE);
                loadingBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        if (!duplicate.contains(response.body().get(i).getCat_id())) {
                            duplicate.add(response.body().get(i).getCat_id());
                            categoryList.add(response.body().get(i));
                        }
                    }
//                    data.addAll(response.body());
                    mCategoryWithPagination.notifyDataChanged();
                } else {
                }

                Collections.sort(categoryList, new Comparator<CategoryPojo>() {
                    public int compare(CategoryPojo v1, CategoryPojo v2) {
                        return v1.getCat_name().compareTo(v2.getCat_name());
                    }
                });

                if (productList.size() < 3) {
                    mCategoryWithPagination.setMoreDataAvailable(false);
                }
            }

            @Override
            public void onFailure(Call<List<CategoryPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
                if (isNetworkAvailable()) {
                    emptyLogo.setText("product not available yet!");
                } else {
                    emptyLogo.setText("You are offline, please try again letter");
                    openSupportContactDialog();

                }
                emptyLogo.setVisibility(View.VISIBLE);
                System.out.println("ERROR >>>> " + t.getMessage());
            }
        });
    }


    private void loadDaily() {

        retryDaily.setVisibility(View.GONE);
        loadingBarDaily.setVisibility(View.VISIBLE);

        Call<List<ProductPojo>> call = apiDaily.getBannerss();
        call.enqueue(new Callback<List<ProductPojo>>() {

            @Override
            public void onResponse(Call<List<ProductPojo>> call, retrofit2.Response<List<ProductPojo>> response) {
                retryDaily.setVisibility(View.GONE);
                loadingBarDaily.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
//                        if (!duplicate.contains(response.body().get(i).getCat_id())) {
//                            duplicate.add(response.body().get(i).getCat_id());
                        dailyList.add(response.body().get(i));
//                        }

                    }
                    mDashBoarDailyAdapter.notifyDataSetChanged();
                } else {
                }

//                Collections.sort(dailyList, new Comparator<ProductPojo>() {
//                    public int compare(ProductPojo v1, ProductPojo v2) {
//                        return v1.getPro_id().compareTo(v2.getPro_id());
//                    }
//                });
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                System.out.println("ERROR >>>> " + t.getMessage());
                retryDaily.setVisibility(View.VISIBLE);
                loadingBarDaily.setVisibility(View.GONE);
            }
        });
    }

    private void loadBakery() {

        retrybakery.setVisibility(View.GONE);
        loadingBarBakery.setVisibility(View.VISIBLE);

        Call<List<ProductPojo>> call = apiBakery.getBannerss();
        call.enqueue(new Callback<List<ProductPojo>>() {

            @Override
            public void onResponse(Call<List<ProductPojo>> call, retrofit2.Response<List<ProductPojo>> response) {
                retryDaily.setVisibility(View.GONE);
                loadingBarDaily.setVisibility(View.GONE);
                int i = 0;
                final int finalI = i;

                if (response.isSuccessful()) {
                    for (i = 0; i < response.body().size(); i++) {
//                        if (!duplicate.contains(response.body().get(i).getCat_id())) {
//                            duplicate.add(response.body().get(i).getCat_id());
                        bakerylist.add(response.body().get(i));

                        spCard1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent ip = new Intent(mContext, ProductDetailsAct.class);
                                ip.putExtra("SelectedProduct", bakerylist.get(finalI));
                                startActivity(ip);
                            }
                        });
                        specialProductImage2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent ip = new Intent(mContext, ProductDetailsAct.class);
                                ip.putExtra("SelectedProduct", bakerylist.get(finalI + 1));
                                startActivity(ip);
                            }
                        });
                        specialProductImage3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent ip = new Intent(mContext, ProductDetailsAct.class);
                                ip.putExtra("SelectedProduct", bakerylist.get(finalI + 2));
                                startActivity(ip);
                            }
                        });

//                        }
                    }
                    if (bakerylist.size() == 0) {
                        loadingBarBakery.setVisibility(View.GONE);
                    } else if (bakerylist.size() == 1) {
                        pro1 = bakerylist.get(finalI).getPro_img();
                        Glide.with(getContext()).load(pro1).placeholder(R.drawable.launcher).into(specialProductImage1);
                        name1 = bakerylist.get(finalI).getPro_name();
                        tvproductname1.setText(name1);
                        tvmrp1.setText("₹ " + bakerylist.get(finalI).getPublix_price1());
                        tvpublixPrice1.setPaintFlags(tvpublixPrice1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        spCard3.setVisibility(View.GONE);
                        spCard1.setVisibility(View.VISIBLE);
                        spCard2.setVisibility(View.GONE);
                        if (TextUtils.isEmpty(bakerylist.get(finalI).getPro_name())) {
                            tvproductname1.setVisibility(View.GONE);
                        } else {
                            tvproductname1.setVisibility(View.VISIBLE);
                        }
                        if (TextUtils.isEmpty(bakerylist.get(finalI).getPrice1())) {
                            linearmrp1.setVisibility(View.GONE);
                        } else {
                            linearmrp1.setVisibility(View.VISIBLE);
                        }if (TextUtils.isEmpty(bakerylist.get(finalI).getPublix_price1())) {
                            linearprice1.setVisibility(View.GONE);
                        } else {
                            linearprice1.setVisibility(View.VISIBLE);
                        }
                    } else if (bakerylist.size() == 2) {
                        pro1 = bakerylist.get(finalI).getPro_img();
                        pro2 = bakerylist.get(finalI + 1).getPro_img();
                        Glide.with(getContext()).load(pro1).placeholder(R.drawable.launcher).into(specialProductImage1);
                        Glide.with(getContext()).load(pro2).placeholder(R.drawable.launcher).into(specialProductImage2);
                        name1 = bakerylist.get(finalI).getPro_name();
                        name2 = bakerylist.get(finalI + 1).getPro_name();
                        tvproductname1.setText(name1);
                        tvproductname2.setText(name2);
                        tvmrp1.setText("₹ " + bakerylist.get(finalI).getPublix_price1());
                        tvmrp2.setText("₹ " + bakerylist.get(finalI + 1).getPublix_price1());
                        tvpublixPrice1.setText("₹ " + bakerylist.get(finalI).getPrice1());
                        tvpublixPrice2.setText("₹ " + bakerylist.get(finalI + 1).getPrice1());
                        tvpublixPrice1.setPaintFlags(tvpublixPrice1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        tvpublixPrice2.setPaintFlags(tvpublixPrice2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        spCard3.setVisibility(View.GONE);
                        spCard1.setVisibility(View.VISIBLE);
                        spCard2.setVisibility(View.VISIBLE);
                        if (TextUtils.isEmpty(bakerylist.get(finalI).getPro_name())) {
                        tvproductname1.setVisibility(View.GONE);
                    } else {
                        tvproductname1.setVisibility(View.VISIBLE);
                    }
                    if (TextUtils.isEmpty(bakerylist.get(finalI + 1).getPro_name())) {
                        tvproductname2.setVisibility(View.GONE);
                    } else {
                        tvproductname2.setVisibility(View.VISIBLE);
                    }  if (TextUtils.isEmpty(bakerylist.get(finalI).getPrice1())) {
                        linearmrp1.setVisibility(View.GONE);
                    } else {
                        linearmrp1.setVisibility(View.VISIBLE);
                    }
                    if (TextUtils.isEmpty(bakerylist.get(finalI + 1).getPrice1())) {
                        linearmrp2.setVisibility(View.GONE);
                    } else {
                        linearmrp2.setVisibility(View.VISIBLE);
                    }if (TextUtils.isEmpty(bakerylist.get(finalI).getPublix_price1())) {
                        linearprice1.setVisibility(View.GONE);
                    } else {
                        linearprice1.setVisibility(View.VISIBLE);
                    }
                    if (TextUtils.isEmpty(bakerylist.get(finalI + 1).getPublix_price1())) {
                        linearprice2.setVisibility(View.GONE);
                    } else {
                        linearprice2.setVisibility(View.VISIBLE);
                    }
                    } else if (bakerylist.size() == 3) {
                        pro1 = bakerylist.get(finalI).getPro_img().split(",")[0];
                        pro2 = bakerylist.get(finalI + 1).getPro_img().split(",")[0];
                        pro3 = bakerylist.get(finalI + 2).getPro_img().split(",")[0];

                        Glide.with(getContext()).load(pro1).placeholder(R.drawable.launcher).into(specialProductImage1);
                        Glide.with(getContext()).load(pro2).placeholder(R.drawable.launcher).into(specialProductImage2);
                        Glide.with(getContext()).load(pro3).placeholder(R.drawable.launcher).into(specialProductImage3);

                        name1 = bakerylist.get(finalI).getPro_name();
                        name2 = bakerylist.get(finalI + 1).getPro_name();
                        name3 = bakerylist.get(finalI + 2).getPro_name();
                        tvproductname1.setText(name1);
                        tvproductname2.setText(name2);
                        tvproductname3.setText(name3);
                        spCard3.setVisibility(View.VISIBLE);
                        spCard1.setVisibility(View.VISIBLE);
                        spCard2.setVisibility(View.VISIBLE);
                        tvmrp1.setText("₹ " + bakerylist.get(finalI).getPublix_price1());
                        tvmrp2.setText("₹ " + bakerylist.get(finalI + 1).getPublix_price1());
                        tvmrp3.setText("₹ " + bakerylist.get(finalI + 2).getPublix_price1());

                        tvpublixPrice1.setText("₹ " + bakerylist.get(finalI).getPrice1());
                        tvpublixPrice2.setText("₹ " + bakerylist.get(finalI + 1).getPrice1());
                        tvpublixPrice3.setText("₹ " + bakerylist.get(finalI + 2).getPrice1());

                        tvpublixPrice1.setPaintFlags(tvpublixPrice1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        tvpublixPrice2.setPaintFlags(tvpublixPrice2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        tvpublixPrice3.setPaintFlags(tvpublixPrice3.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        if (TextUtils.isEmpty(bakerylist.get(finalI).getPro_name())) {
                            tvproductname1.setVisibility(View.GONE);
                        } else {
                            tvproductname1.setVisibility(View.VISIBLE);
                        }
                        if (TextUtils.isEmpty(bakerylist.get(finalI + 1).getPro_name())) {
                            tvproductname2.setVisibility(View.GONE);
                        } else {
                            tvproductname2.setVisibility(View.VISIBLE);
                        }
                        if (TextUtils.isEmpty(bakerylist.get(finalI + 2).getPro_name())) {
                            tvproductname3.setVisibility(View.GONE);
                        } else {
                            tvproductname3.setVisibility(View.VISIBLE);
                        }

                        //Mrp amount
                        if (TextUtils.isEmpty(bakerylist.get(finalI).getPrice1())) {
                            linearmrp1.setVisibility(View.GONE);
                        } else {
                            linearmrp1.setVisibility(View.VISIBLE);
                        }
                        if (TextUtils.isEmpty(bakerylist.get(finalI + 1).getPrice1())) {
                            linearmrp2.setVisibility(View.GONE);
                        } else {
                            linearmrp2.setVisibility(View.VISIBLE);
                        }
                        if (TextUtils.isEmpty(bakerylist.get(finalI + 2).getPrice1())) {
                            linearmrp3.setVisibility(View.GONE);
                        } else {
                            linearmrp3.setVisibility(View.VISIBLE);
                        }
                        //publix price
                        if (TextUtils.isEmpty(bakerylist.get(finalI).getPublix_price1())) {
                            linearprice1.setVisibility(View.GONE);
                        } else {
                            linearprice1.setVisibility(View.VISIBLE);
                        }
                        if (TextUtils.isEmpty(bakerylist.get(finalI + 1).getPublix_price1())) {
                            linearprice2.setVisibility(View.GONE);
                        } else {
                            linearprice2.setVisibility(View.VISIBLE);
                        }
                        if (TextUtils.isEmpty(bakerylist.get(finalI + 2).getPublix_price1())) {
                            linearprice3.setVisibility(View.GONE);
                        } else {
                            linearprice3.setVisibility(View.VISIBLE);
                        }
                    }
                }
//                Collections.sort(bakerylist, new Comparator<ProductPojo>() {
//                    public int compare(ProductPojo v1, ProductPojo v2) {
//                        return v1.getPro_id().compareTo(v2.getPro_id());
//                    }
//                });
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                System.out.println("ERROR >>>> " + t.getMessage());
                retrybakery.setVisibility(View.VISIBLE);
                loadingBarBakery.setVisibility(View.GONE);
            }
        });
    }


    private void loadFastFood() {

        retryFoodLayout.setVisibility(View.GONE);
        trendingloadingBar.setVisibility(View.VISIBLE);

        Call<List<ProductPojo>> call = apiFood.getBannerss();
        call.enqueue(new Callback<List<ProductPojo>>() {
            @Override
            public void onResponse(Call<List<ProductPojo>> call, retrofit2.Response<List<ProductPojo>> response) {
                retryFoodLayout.setVisibility(View.GONE);
                trendingloadingBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                        for (int i = 0; i < response.body().size(); i++) {
//                        if (!duplicate.contains(response.body().get(i).getCat_id())) {
//                            duplicate.add(response.body().get(i).getCat_id());
                            fastfoodList.add(response.body().get(i));
//                        }
                    }

                    mFruitListAdapter.notifyDataSetChanged();
                } else {
                }

//                Collections.sort(fastfoodList, new Comparator<ProductPojo>() {
//                    public int compare(ProductPojo v1, ProductPojo v2) {
//                        return v1.getPro_id().compareTo(v2.getPro_id());
//                    }
//                });
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                System.out.println("ERROR >>>> " + t.getMessage());
                retryFoodLayout.setVisibility(View.VISIBLE);
                trendingloadingBar.setVisibility(View.GONE);
            }
        });
    }

    class loadMultipleImages extends AsyncTask<Void, Void, Void> {

        String multipleImages;
        ArrayList<String> imageList;

        public loadMultipleImages(String multipleImages) {
            this.multipleImages = multipleImages;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            imageList = (ArrayList<String>) Arrays.asList(multipleImages.split("\\s*,\\s*"));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void loadMore(int index) {
        //add loading progress view
        categoryList.add(new CategoryPojo("load"));
        mCategoryWithPagination.notifyItemInserted(categoryList.size() - 1);

        Call<List<CategoryPojo>> call = api.getMovies();
        call.enqueue(new Callback<List<CategoryPojo>>() {
            @Override
            public void onResponse(Call<List<CategoryPojo>> call, retrofit2.Response<List<CategoryPojo>> response) {
                loadingBar.setVisibility(View.GONE);
                emptyLogo.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    //remove loading view
                    categoryList.remove(categoryList.size() - 1);

                    List<CategoryPojo> result = response.body();
                    if (result.size() > 0) {
                        //add loaded data
                        for (int i = 0; i < result.size(); i++) {
                            if (!duplicate.contains(result.get(i).getCat_id())) {
                                duplicate.add(result.get(i).getCat_id());
                                categoryList.add(result.get(i));
                            }
                        }
//                        data.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        mCategoryWithPagination.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
                        Toast.makeText(mContext, "No More Data Available", Toast.LENGTH_LONG).show();
                    }
                    Collections.sort(categoryList, new Comparator<CategoryPojo>() {
                        public int compare(CategoryPojo v1, CategoryPojo v2) {
                            return v1.getCat_name().compareTo(v2.getCat_name());
                        }
                    });
                    mCategoryWithPagination.notifyDataChanged();
                    //should call the custom method adapter.notifyDataChanged here to get the correct loading status
                } else {
                }
            }

            @Override
            public void onFailure(Call<List<CategoryPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
                if (isNetworkAvailable()) {
                    emptyLogo.setText("product not available yet!");
                } else {
                    emptyLogo.setText("You are offline, please try again letter");
                    openSupportContactDialog();

                }
                emptyLogo.setVisibility(View.VISIBLE);
                System.out.println("ERROR >>>> " + t.getMessage());
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("fruit.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
