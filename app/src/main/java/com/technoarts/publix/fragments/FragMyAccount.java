package com.technoarts.publix.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.technoarts.publix.BaseFrag;

import com.technoarts.publix.R;
import com.technoarts.publix.activity.ChangePassAct;
import com.technoarts.publix.activity.OrderAct;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;


public class FragMyAccount extends BaseFrag {

    String user_id, user_name = null;
    TextView username;
    click mClick;

    public static int notificationCountCart = 0;
    public FragMyAccount(click mClick) {
        this.mClick = mClick;
    }

    public FragMyAccount() {

    }

    public interface click {
        void onClick();
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.frag_my_account;
    }

    @Override
    public void bindObjects(Context mContext) {

    }

    @Override
    public void bindViews(View view) {
        username = view.findViewById(R.id.user_name);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        // SharedPreferences.Editor editor = sharedPreferences.edit();
        user_id = settings.getString("user_id", "");
        user_name = settings.getString("user_name", "");

        username.setText("welcome "+new PrefHelper(getActivity()).get_STRING(Constant.USERNAME, ""));

        LinearLayout linearLayout1 = view.findViewById(R.id.l1);
        linearLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OrderAct.class);
                startActivity(intent);
            }
        });
        LinearLayout linearLayout2 = view.findViewById(R.id.l2);
        linearLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ChangePassAct.class);
                startActivity(intent);
            }
        });
        if (new PrefHelper(getActivity()).get_BOOLEAN(Constant.isSocial, false)) {
            linearLayout2.setVisibility(View.GONE);
        } else {
            linearLayout2.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void bindPixel() {

    }

    @Override
    public void bindListener() {

    }

    @Override
    public void bindMethod() {

    }
}
