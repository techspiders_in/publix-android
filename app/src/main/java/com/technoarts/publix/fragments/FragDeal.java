package com.technoarts.publix.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.technoarts.publix.BaseFrag;
import com.technoarts.publix.R;
import com.technoarts.publix.activity.ProductDetailsAct;
import com.technoarts.publix.callback.DealApi;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.paginationAdapter.ProductWithPagination;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class FragDeal extends BaseFrag {

    Context mContext;
    RecyclerView productListView;
    ArrayList<ProductPojo> productList;
    ProgressBar loadingBar;

    DealApi api;
    ArrayList<String> duplicate;
   ProductWithPagination mProductWithPagination;

    @Override
    public int getLayoutResourceId() {
        return R.layout.frag_deal;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        productList = new ArrayList<>();

        productList = new ArrayList<>();
        duplicate = new ArrayList<>();
        api = ServiceGenerator.createService(DealApi.class);
    }

    @Override
    public void bindViews(View view) {
        productListView = view.findViewById(R.id.productListView);
        loadingBar = view.findViewById(R.id.loadingBar);
    }

    @Override
    public void bindPixel() {

    }

    @Override
    public void bindListener() {

    }

    @Override
    public void bindMethod() {
        productListView.setLayoutManager(new GridLayoutManager(mContext, 2));

//        new LoadDeal(mContext, new onProductLoadComplete() {
//            @Override
//            public void onLoadComplete(final ArrayList<ProductPojo> productList) {
//                loadingBar.setVisibility(View.GONE);
//                productListView.setAdapter(new ProductListAdapter(mContext, productList, new onProductClick() {
//                    @Override
//                    public void click(int position) {
//                        Intent ip = new Intent(mContext, ProductDetailsAct.class);
//                        ip.putExtra("SelectedProduct", productList.get(position));
//                        startActivity(ip);
//                    }
//                }));
//            }
//        }).execute();


        mProductWithPagination = new ProductWithPagination(mContext, productList, new onProductClick() {
            @Override
            public void click(int position) {
                Intent ip = new Intent(mContext, ProductDetailsAct.class);
                ip.putExtra("SelectedProduct", productList.get(position));
                startActivity(ip);
            }
        });
        mProductWithPagination.setLoadMoreListener(new ProductWithPagination.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                productListView.post(new Runnable() {
                    @Override
                    public void run() {
                        int index = productList.size() - 1;
                        loadMore(index);
                    }
                });
            }
        });
        productListView.setHasFixedSize(true);
        productListView.setAdapter(mProductWithPagination);
        load(0);
    }

    private void load(int index) {
        Call<List<ProductPojo>> call = api.getMovies(index);
        call.enqueue(new Callback<List<ProductPojo>>() {


            @Override
            public void onResponse(Call<List<ProductPojo>> call, retrofit2.Response<List<ProductPojo>> response) {
                loadingBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        if (!duplicate.contains(response.body().get(i).getPro_id())) {
                            duplicate.add(response.body().get(i).getPro_id());
                            productList.add(response.body().get(i));
                        }
                    }
//                    data.addAll(response.body());
                    mProductWithPagination.notifyDataChanged();
                } else {
                }

                Collections.sort(productList, new Comparator<ProductPojo>() {
                    public int compare(ProductPojo v1, ProductPojo v2) {
                        return v1.getPro_name().compareTo(v2.getPro_name());
                    }
                });

                if (productList.size() < 10) {
                    mProductWithPagination.setMoreDataAvailable(false);
                    System.out.println("ERROR >>>> " + productList.size());
                }
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
                openSupportContactDialog();
            }
        });
    }

    private void loadMore(int index) {
        //add loading progress view
        productList.add(new ProductPojo("load"));
        mProductWithPagination.notifyItemInserted(productList.size() - 1);

        Call<List<ProductPojo>> call = api.getMovies(index);
        call.enqueue(new Callback<List<ProductPojo>>() {
            @Override
            public void onResponse(Call<List<ProductPojo>> call, retrofit2.Response<List<ProductPojo>> response) {
                loadingBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    //remove loading view
                    productList.remove(productList.size() - 1);

                    List<ProductPojo> result = response.body();
                    if (result.size() > 0) {
                        //add loaded data
                        for (int i = 0; i < result.size(); i++) {
                            if (!duplicate.contains(result.get(i).getPro_id())) {
                                duplicate.add(result.get(i).getPro_id());
                                productList.add(result.get(i));
                            }
                        }
//                        data.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        mProductWithPagination.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
                        Toast.makeText(mContext, "No More Data Available", Toast.LENGTH_LONG).show();
                    }
                    Collections.sort(productList, new Comparator<ProductPojo>() {
                        public int compare(ProductPojo v1, ProductPojo v2) {
                            return v1.getPro_name().compareTo(v2.getPro_name());
                        }
                    });
                    mProductWithPagination.notifyDataChanged();
                    //should call the custom method adapter.notifyDataChanged here to get the correct loading status
                } else {
                }
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
