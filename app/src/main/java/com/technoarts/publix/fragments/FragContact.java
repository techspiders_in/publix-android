package com.technoarts.publix.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.technoarts.publix.BaseFrag;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.ContactUsApi;
import com.technoarts.publix.helpers.BasicHelper;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.helpers.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

public class FragContact extends BaseFrag {

    Context mContext;
    EditText name, email, message;
    TextView submit;
    String getname, getemail, getmsg;
    int result = 0;
    String response_message;
    click mClick;
    private ContactUsApi api;
    private TextView txt_phone;

    public FragContact(click mClick) {
        this.mClick = mClick;
    }

    public interface click {
        void onClick();
    }


    @Override
    public int getLayoutResourceId() {
        return R.layout.frag_contact;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void bindViews(View view) {
        name = view.findViewById(R.id.input_name);
        email = view.findViewById(R.id.input_email);
        message = view.findViewById(R.id.input_message);
        submit = view.findViewById(R.id.btn_submit);
        txt_phone = view.findViewById(R.id.txt_phone);

        api = ServiceGenerator.createService(ContactUsApi.class);

    }

    @Override
    public void bindPixel() {

    }

    @Override
    public void bindListener() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getname = name.getText().toString();
                Log.e("name", getname);
                getemail = email.getText().toString();
                Log.e("name", getemail);
                Pattern p = Pattern.compile(Utils.regEx);
                Matcher m = p.matcher(getemail);
                getmsg = message.getText().toString();
                Log.e("name", getmsg);

                if (getname.equals("") || getname.length() == 0
                        || getemail.equals("") || getemail.length() == 0) {
                    new BasicHelper().Show_Toast(mContext, v,
                            "All fields are required.");
                } else if (!m.find()) {
                    new BasicHelper().Show_Toast(mContext, v,
                            "Your email Id is invalid.");
                } else if (getmsg.equals("")) {
                    new BasicHelper().Show_Toast(mContext, v,
                            "Enter your message ");
                } else {
                    Postdata(getname, getemail, getmsg);
                }


            }
        });

        txt_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+ +91 8717000050"));
                startActivity(intent);
            }
        });
    }

    @Override
    public void bindMethod() {

    }

    private void Postdata(String getname, String getemail, String getmsg) {



        Map<String, String> jsonParams = new HashMap<>();
        jsonParams.put("name", getname);
        jsonParams.put("email", getemail);
        jsonParams.put("message", getmsg);



        api.shareContactUsDetail(jsonParams).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()){
                    clearValues();
                    Toast.makeText(mContext, "Thank you!!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void clearValues() {
        name.setText("");
        email.setText("");
        message.setText("");
        name.setFocusableInTouchMode(true);
        name.setFocusable(true);
        name.requestFocus();
    }

    private void showpopup() {
        ProgressDialog pdialog = new ProgressDialog(mContext);
        pdialog.setMessage("Loading...");
        pdialog.setCancelable(false);
        pdialog.setInverseBackgroundForced(false);
        pdialog.show();

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogsuccess);
        LinearLayout lay1;
        lay1 = (LinearLayout) dialog.findViewById(R.id.contact_success);
        lay1.setVisibility(View.VISIBLE);
        TextView ok = (TextView) dialog.findViewById(R.id.txt_ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        pdialog.dismiss();
    }
}
