package com.technoarts.publix.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.technoarts.publix.BaseFrag;
import com.technoarts.publix.R;
import com.technoarts.publix.activity.ProductDetailsAct;
import com.technoarts.publix.adapters.Slideradapter;
import com.technoarts.publix.asyncs.LoadHomeCategory;
import com.technoarts.publix.callback.onCategoryLoadComplete;
import com.technoarts.publix.paginationAdapter.CategoryWithPagination;
import com.technoarts.publix.pojo.CategoryPojo;
import com.technoarts.publix.pojo.ProductPojo;
import com.technoarts.publix.pojo.Slidermodel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FragHome extends BaseFrag implements BaseSliderView.OnSliderClickListener {

    Context mContext;
    RecyclerView productListView;
    ArrayList<ProductPojo> productList;
    ProgressBar loadingBar;
    SliderLayout sliderLayout;
    private ArrayList<Slidermodel> catList1;
    HashMap<String, String> Hash_file_maps;
    private Slideradapter catadapter1;

    @Override
    public int getLayoutResourceId() {
        return R.layout.frag_home;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        productList = new ArrayList<>();
        catList1 = new ArrayList<>();
    }

    @Override
    public void bindViews(View view) {
        productListView = view.findViewById(R.id.productListView);
        loadingBar = view.findViewById(R.id.loadingBar);
        sliderLayout = view.findViewById(R.id.sliderLayout);
    }

    @Override
    public void bindPixel() {

    }

    @Override
    public void bindListener() {

    }

    @Override
    public void bindMethod() {
        productListView.setLayoutManager(new GridLayoutManager(mContext, 2));

        new LoadHomeCategory(mContext, new onCategoryLoadComplete() {
            @Override
            public void onComplete(ArrayList<CategoryPojo> categoryList) {
                loadingBar.setVisibility(View.GONE);
                productListView.setAdapter(new CategoryWithPagination(mContext, categoryList, new CategoryWithPagination.onClicFilter() {
                    @Override
                    public void onClick(int pos) {
                        Intent ip = new Intent(mContext, ProductDetailsAct.class);
                        ip.putExtra("SelectedProduct", (productList.get(pos)));
                        startActivity(ip);
                    }

//                    @Override
//                    public void click(int position) {
//                        Intent ip = new Intent(mContext, ProductDetailsAct.class);
//                        ip.putExtra("SelectedProduct", productList.get(position));
//                        startActivity(ip);
//                    }
                }));
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        slider();
    }

    private void Slider(String img) {
        Hash_file_maps = new HashMap<String, String>();
//
        Hash_file_maps.put("", img);

        System.out.println("SLIDER : " + "http://monalisastores.com/ashokasta/askadmin/Slider/json" + img);
//        Hash_file_maps.put("Android Donut", "http://androidblog.esy.es/images/donut-2.png");
//        Hash_file_maps.put("Android Eclair", "http://androidblog.esy.es/images/eclair-3.png");
//        Hash_file_maps.put("Android Froyo", "http://androidblog.esy.es/images/froyo-4.png");
//        Hash_file_maps.put("Android GingerBread", "http://androidblog.esy.es/images/gingerbread-5.png");

        for (String name : Hash_file_maps.keySet()) {

            TextSliderView textSliderView = new TextSliderView(mContext);
            textSliderView
                    .description(name)
                    .image(Hash_file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(3000);
//        sliderLayout.addOnPageChangeListener(Fra);
    }

    private void slider() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://monalisastores.com/ashokasta/askadmin/Slider/json",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonObject = new JSONArray(response);
//                            JSONArray jsonArray = jsonObject.getJSONArray();
                            Log.d("-------service------", String.valueOf(jsonObject));
                            for (int i = 0; i < jsonObject.length(); i++) {
                                JSONObject itemObj = (JSONObject) jsonObject.get(i);
                                Slidermodel cat = new Slidermodel(itemObj.getString("id")
                                        , itemObj.getString("name"),
                                        itemObj.getString("image"));

                                catList1.add(cat);
                                Log.d("-------catlist------", String.valueOf(catList1));
                                catadapter1 = new Slideradapter(catList1, getLayoutInflater(), mContext);
                                dispslider();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("SLIDER : >>>" + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("SLIDER : ************** " + error.getMessage());
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);
    }

    private void dispslider() {
//        grid1.setAdapter(catadapter1);
        String image = null;
        for (int i = 0; i < catList1.size(); i++) {
            image = catList1.get(i).getImg();
            Log.e("getimage", image);
        }
        Slider(image);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
