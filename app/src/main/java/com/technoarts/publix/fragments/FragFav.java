package com.technoarts.publix.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.technoarts.publix.BaseFrag;
import com.technoarts.publix.R;
import com.technoarts.publix.activity.ProductDetailsAct;
import com.technoarts.publix.callback.RelatedApi;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.paginationAdapter.ProductWithPagination;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class FragFav extends BaseFrag {

    Context mContext;
    RecyclerView productListView;
    ArrayList<ProductPojo> productList;
    ProgressBar loadingBar;

    RelatedApi api;
    ArrayList<String> duplicate;
   ProductWithPagination mProductWithPagination;

    @Override
    public int getLayoutResourceId() {
        return R.layout.frag_deal;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        productList = new ArrayList<>();

        productList = new ArrayList<>();
        duplicate = new ArrayList<>();
        api = ServiceGenerator.createService(RelatedApi.class);
    }

    @Override
    public void bindViews(View view) {
        productListView = view.findViewById(R.id.productListView);
        loadingBar = view.findViewById(R.id.loadingBar);
    }

    @Override
    public void bindPixel() {

    }

    @Override
    public void bindListener() {

    }

    @Override
    public void bindMethod() {
        productListView.setLayoutManager(new GridLayoutManager(mContext, 2));

        mProductWithPagination = new ProductWithPagination(mContext, productList, new onProductClick() {
            @Override
            public void click(int position) {
                Intent ip = new Intent(mContext, ProductDetailsAct.class);
                ip.putExtra("SelectedProduct", productList.get(position));
                startActivity(ip);
            }
        });

        productListView.setHasFixedSize(true);
        productListView.setAdapter(mProductWithPagination);
        load();
    }

    private void load() {
        String userId  = new PrefHelper(mContext).get_STRING(Constant.checkout_user_id, "");
        HashMap<String,Object> hmParam = new HashMap<>();
        hmParam.put("user_id",userId);
        Call<List<ProductPojo>> call = api.getFavList(hmParam);
        call.enqueue(new Callback<List<ProductPojo>>() {


            @Override
            public void onResponse(Call<List<ProductPojo>> call, retrofit2.Response<List<ProductPojo>> response) {
                loadingBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        if (!duplicate.contains(response.body().get(i).getPro_id())) {
                            duplicate.add(response.body().get(i).getPro_id());
                            productList.add(response.body().get(i));
                        }
                    }
                    mProductWithPagination.notifyDataChanged();
                } else {
                }

                Collections.sort(productList, new Comparator<ProductPojo>() {
                    public int compare(ProductPojo v1, ProductPojo v2) {
                        return v1.getPro_name().compareTo(v2.getPro_name());
                    }
                });

                if (productList.size() < 10) {
                    mProductWithPagination.setMoreDataAvailable(false);
                    System.out.println("ERROR >>>> " + productList.size());
                }
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
