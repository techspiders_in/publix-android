package com.technoarts.publix;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public abstract class BaseFrag extends Fragment {
    View rootView;
    Context mContext;
    private AlertDialog dialog;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(getLayoutResourceId(), container,
                false);
        mContext = getActivity();
        bindObjects(mContext);
        bindViews(rootView);
        bindPixel();
        bindListener();
        bindMethod();
        return rootView;
    }

    public abstract int getLayoutResourceId();

    public abstract void bindObjects(Context mContext);

    public abstract void bindViews(View view);

    public abstract void bindPixel();

    public abstract void bindListener();

    public abstract void bindMethod();

    public void openSupportContactDialog() {
            if (dialog == null) {
                createDialog();
            }
            dialog.show();
     }


    private void createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setTitle(getString(R.string.no_internet));
        builder.setMessage(getString(R.string.no_worries));
        builder.setPositiveButton("Call", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+getString(R.string.place_order_number)));
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog = builder.create();
    }

}
