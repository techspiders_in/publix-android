package com.technoarts.ashokasta.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.technoarts.publix.helpers.NotificationVO;

import java.util.ArrayList;

public class OfflineNotification extends SQLiteOpenHelper {

    Context mContext;

    public static final String dbName = "OfflineDataArea";
    String TABLENAME = "AreaDb";
    String COL_ID = "id";
    String COL_TITLE = "title";
    String COL_MESSAGE = "message";
    String COL_IMAGE = "imagePath";


    String CREATE = "CREATE TABLE '" + TABLENAME + "' ('" + COL_ID + "' text ," + " '" + COL_TITLE + "' text,'" + COL_MESSAGE + "' text,'" + COL_IMAGE + "')";

    String DROP = "DROP TABLE IF EXISTS " + TABLENAME;

    public OfflineNotification(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL(CREATE);
        } catch (Exception e) {
            System.out.println("OFFLINE : Top Story : " + e.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        try {
            sqLiteDatabase.execSQL(DROP);
            onCreate(sqLiteDatabase);
        } catch (Exception e) {
            System.out.println("OFFLINE : Top Story : " + e.getMessage());
        }

    }

    public long addBookmark(String id, String title, String message, String image) {
        long i = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(COL_ID, id);
            contentValues.put(COL_TITLE, title);
            contentValues.put(COL_MESSAGE, message);
            contentValues.put(COL_IMAGE, image);
            i = db.insert(TABLENAME, null, contentValues);
        } catch (SQLException e) {
            System.out.println("OFFLINE : Top Story : " + e.getMessage());
        }
        return i;
    }

    public void deleteBookmark(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQury = "delete from " + TABLENAME + " where " + COL_ID + " = '" + id + "' ";
        db.execSQL(deleteQury);
    }


    public ArrayList<NotificationVO> getAllBookmark() {
        ArrayList<NotificationVO> array_list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLENAME, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            NotificationVO notificationVO = new NotificationVO("");
            notificationVO.setTitle(res.getString(res.getColumnIndex(COL_TITLE)));
            notificationVO.setMessage(res.getString(res.getColumnIndex(COL_MESSAGE)));
            notificationVO.setIconUrl(res.getString(res.getColumnIndex(COL_IMAGE)));
            notificationVO.setAction(res.getString(res.getColumnIndex(COL_ID)));
            array_list.add(notificationVO);
            res.moveToNext();
        }
        return array_list;
    }


    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQury = "delete from " + TABLENAME;
        db.execSQL(deleteQury);
    }

    public void delete(String title) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQury = "delete from "+TABLENAME+" where" + COL_TITLE + "=" + title;
        db.execSQL(deleteQury);
    }

    public long getNumberOfItems() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLENAME);
        db.close();
        return count;
    }
}
