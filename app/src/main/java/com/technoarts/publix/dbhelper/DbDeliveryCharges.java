package com.technoarts.publix.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.technoarts.publix.pojo.DeliveryChargesPojo;

import java.util.ArrayList;

public class DbDeliveryCharges extends SQLiteOpenHelper {

    Context mContext;

    public static final String dbName = "dCharges";
    String TABLENAME = "DeliveryChargesTb";
    String COL_ID = "id";
    String COL_TITLE = "deliveryCharge";


    String CREATE = "CREATE TABLE '" + TABLENAME + "' ('" + COL_ID + "' text ,"
            + " '" + COL_TITLE
            + "' text)";

    String DROP = "DROP TABLE IF EXISTS " + TABLENAME;

    public DbDeliveryCharges(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL(CREATE);
        } catch (Exception e) {
            System.out.println("OFFLINE : Top Story : " + e.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        try {
            sqLiteDatabase.execSQL(DROP);
            onCreate(sqLiteDatabase);
        } catch (Exception e) {
            System.out.println("OFFLINE : Top Story : " + e.getMessage());
        }

    }

    public long addCharge(String id, String charge) {
        long i = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(COL_ID, id);
            contentValues.put(COL_TITLE, charge);
            i = db.insert(TABLENAME, null, contentValues);
        } catch (SQLException e) {
            System.out.println("OFFLINE : Top Story : " + e.getMessage());
        }
        return i;
    }

    public boolean isBookmark(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLENAME + " where " + COL_ID + " = '"
                + id + "'", null);
        if (res.getCount() > 0)
            return true;
        else
            return false;
    }

    public void deleteCharges(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQury = "delete from " + TABLENAME + " where " + COL_ID + " = '" + id + "' ";
        db.execSQL(deleteQury);
    }


    public ArrayList<DeliveryChargesPojo> getAllCharges() {
        ArrayList<DeliveryChargesPojo> array_list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLENAME, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(new DeliveryChargesPojo(res.getString(res.getColumnIndex(COL_ID)),
                    res.getString(res.getColumnIndex(COL_TITLE))));
            res.moveToNext();
        }
        return array_list;
    }

    public int getDeliveryCharges() {
        int tot = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLENAME, null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            if (!res.getString(res.getColumnIndex(COL_ID)).equals(""))
                tot += Integer.parseInt(res.getString(res.getColumnIndex(COL_ID)));
            res.moveToNext();
        }
//        if (res.getCount() > 0) {
//            tot += Integer.parseInt(res.getString(res.getColumnIndex(COL_TITLE)));
//            res.moveToNext();
//        } else
//            tot += 0;
        return tot;
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQury = "delete from " + TABLENAME;
        db.execSQL(deleteQury);
    }

    public void delete(String title) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQury = "delete from " + TABLENAME + " where" + COL_TITLE + "=" + title;
        db.execSQL(deleteQury);
    }

    public long getNumberOfItems() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLENAME);
        db.close();
        return count;
    }
}
