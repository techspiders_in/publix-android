package com.technoarts.publix.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;

public class DbCart extends SQLiteOpenHelper {

    Context mContext;

    public static final String dbName = "cartDb";
    String TABLENAME = "cartTable";
    String COL_ID = "itemid";
    String COL_NAME = "itemName";
    String COL_IMAGES = "itemImages";
    String COL_PRICE = "itemPrice";
    String COL_DES = "itemDes";
    String COL_SALE = "itemseller";
    String COL_MRP = "itemserMrp";
    String COL_DATE = "itemserDate";
    String COL_STATUS = "itemserStatus";
    String COL_CAT_ID = "catId";
    String COL_QTY = "itemserQty";
    String COL_REL = "rel";
    String COL_NAME_PRICE = "name_price";
    String COL_PUBLIX_PRICE = "publix_price";
    String COL_PUBLIX_PRICE1 = "publix_price1";
    String COL_PUBLIX_PRICE2 = "publix_price2";
    String COL_PUBLIX_PRICE3 = "publix_price3";
    String COL_name1 = "name1";
    String COL_name2 = "name2";
    String COL_name3 = "name3";
    String COL_price1 = "price1";
    String COL_price2 = "price2";
    String COL_price3 = "price3";


//    String TABLENAME = "cartTable";
//    String COL_ID = "itemid";
//    String COL_PRO_ID = "pro_id";
////    String COL_NAME = "cat_name";
////    String COL_PRO_NAME = "pro_name";
//    String COL_NAME = "name";
//    String COL_CAT_ID="catId";
//    String COL_PRO_CAT_ID = "pro_cat_id";
//    String COL_DES = "pro_desc";
//    String COL_SALE = "itemseller";
//    String COL_PRICE = "itemPrice";
//    String COL_MRP = "itemserMrp";
//    String COL_STATUS = "itemserStatus";
//    String COL_DATE = "itemserDate";
////    String COL_SIZE = "pro_size";
//    String COL_IMAGES = "itemImages";
//    String COL_QTY = "itemserQty";
//    String COL_REL = "rel";
//    String COL_NAME_PRICE = "name_price";
//    String COL_PUBLIX_PRICE = "publix_price";
//    String COL_PUBLIX_PRICE1 = "publix_price1";
//    String COL_PUBLIX_PRICE2 = "publix_price2";
//    String COL_PUBLIX_PRICE3 = "publix_price3";
//    String COL_name1 = "name1";
//    String COL_name2 = "name2";
//    String COL_name3 = "name3";
//    String COL_price1 = "price1";
//    String COL_price2 = "price2";
//    String COL_price3 = "price3";


    String CREATE = "CREATE TABLE '" + TABLENAME + "' ('"
            + COL_ID + "' text ," +
            " '" + COL_NAME + "' text ," +
            "'" + COL_IMAGES + "' text ," +
            "'" + COL_PRICE + "' text ," +
            "'" + COL_DES + "' text ," +
            "'" + COL_SALE + "' text ," +
            "'" + COL_MRP + "' text ," +
            "'" + COL_DATE + "' text ," +
            "'" + COL_CAT_ID + "' text ," +
            "'" + COL_STATUS + "' text ," +
            "'" + COL_REL + "' text ," +
            "'" + COL_QTY + "' text, " +
            "'" + COL_NAME_PRICE + "'text," +
            "'" + COL_PUBLIX_PRICE + "'text," +

            "'" + COL_name1 + "'text," +
            "'" + COL_price1 + "'text," +
            "'" + COL_PUBLIX_PRICE1 + "'text," +

            "'" + COL_name2 + "'text," +
            "'" + COL_price2 + "'text," +
            "'" + COL_PUBLIX_PRICE2 + "'text," +

            "'" + COL_name3 + "'text," +
            "'" + COL_price3 + "'text," +
            "'" + COL_PUBLIX_PRICE3 + "'text" +
            ")";

    String DROP = "DROP TABLE IF EXISTS " + TABLENAME;

    public DbCart(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL(CREATE);
        } catch (Exception e) {
            System.out.println("OFFLINE : Top Story : " + e.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        try {
            sqLiteDatabase.execSQL(DROP);
            onCreate(sqLiteDatabase);
        } catch (Exception e) {
            System.out.println("OFFLINE : Top Story : " + e.getMessage());
        }

    }


//    public long addToCart(String cat_id, String cat_name,String pro_cat_id,
//             String pro_desc, String pro_sale, String pro_mrp_ammount,String pro_name,
//                           String pro_status, String date, String pro_img, String name1, String qty,
//                          String related_product, String name_price, String deliverycharge, String price1, String publix_price1,
//                          String name2, String price2, String publix_price2, String name3, String price3,
//                          String publix_price3, String publix_price) {
//        long i = -1;
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            ContentValues contentValues = new ContentValues();
//            contentValues.put(COL_ID, cat_id);
//            contentValues.put(COL_PRO_NAME, pro_name);
////            contentValues.put(COL_PRO_ID, cat_id);
////            contentValues.put(COL_PRO_NAME, cat_name);
//
//            contentValues.put(COL_PRO_CAT_ID, pro_cat_id);
////            contentValues.put(COL_SIZE, pro_size);
//            contentValues.put(COL_IMAGES, pro_img);
//            contentValues.put(COL_DES, pro_desc);
//            contentValues.put(COL_MRP, pro_mrp_ammount);
//            contentValues.put(COL_DATE, date);
//            contentValues.put(COL_SALE, pro_sale);
//            contentValues.put(COL_STATUS, pro_status);
//            contentValues.put(COL_REL, related_product);
//            contentValues.put(COL_QTY, qty);
//            contentValues.put(COL_NAME_PRICE, name_price);
//            contentValues.put(COL_PUBLIX_PRICE, publix_price);
//            contentValues.put(COL_PUBLIX_PRICE1, publix_price1);
//            contentValues.put(COL_PUBLIX_PRICE2, publix_price2);
//            contentValues.put(COL_PUBLIX_PRICE3, publix_price3);
//            contentValues.put(COL_name1, name1);
//            contentValues.put(COL_name2, name2);
//            contentValues.put(COL_name3, name3);
//            contentValues.put(COL_price1, price1);
//            contentValues.put(COL_price2, price2);
//            contentValues.put(COL_price3, price3);
//            i = db.insert(TABLENAME, null, contentValues);
//        } catch (SQLException e) {
//            System.out.println("OFFLINE : Top Story : " + e.getMessage());
//        }
//        return i;
//    }


    public long addToCart(String id, String name, String image, String price, String description,
                          String mrp, String status, String date, String proSale, String catId, String qty,
                          String rel, String name_price, String publix_price,
                          String publix_price1,
                          String publix_price2, String publix_price3,
                          String name1, String name2, String name3,
                          String price1, String price2, String price3
    ) {
        long i = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(COL_ID, id);
            contentValues.put(COL_NAME, name);
            contentValues.put(COL_IMAGES, image);
            contentValues.put(COL_PRICE, price);
            contentValues.put(COL_DES, description);
            contentValues.put(COL_MRP, mrp);
            contentValues.put(COL_DATE, date);
            contentValues.put(COL_SALE, proSale);
            contentValues.put(COL_CAT_ID, catId);
            contentValues.put(COL_STATUS, status);
            contentValues.put(COL_REL, rel);
            contentValues.put(COL_QTY, qty);
            contentValues.put(COL_NAME_PRICE, name_price);
            contentValues.put(COL_PUBLIX_PRICE, publix_price);
            contentValues.put(COL_name1, name1);
            contentValues.put(COL_price1, price1);
            contentValues.put(COL_PUBLIX_PRICE1, publix_price1);
            contentValues.put(COL_name2, name2);
            contentValues.put(COL_price2, price2);
            contentValues.put(COL_PUBLIX_PRICE2, publix_price2);
            contentValues.put(COL_name3, name3);
            contentValues.put(COL_price3, price3);
            contentValues.put(COL_PUBLIX_PRICE3, publix_price3);
            i = db.insert(TABLENAME, null, contentValues);
        } catch (SQLException e) {
            System.out.println("OFFLINE : Top Story : " + e.getMessage());
        }
        return i;
    }

    public void deleteBookmark(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQury = "delete from " + TABLENAME + " where " + COL_ID + " = '" + id + "' ";
        db.execSQL(deleteQury);
    }


    public ArrayList<ProductPojo> getAllBookmark() {
        ArrayList<ProductPojo> array_list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLENAME, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(new ProductPojo(
                            res.getString(res.getColumnIndex(COL_ID)),
                            res.getString(res.getColumnIndex(COL_NAME)),
                            res.getString(res.getColumnIndex(COL_CAT_ID)),
                            "",
                            res.getString(res.getColumnIndex(COL_DES)),
                            res.getString(res.getColumnIndex(COL_SALE)),
                            res.getString(res.getColumnIndex(COL_PRICE)),
                            res.getString(res.getColumnIndex(COL_MRP)),
                            res.getString(res.getColumnIndex(COL_STATUS)),
                            res.getString(res.getColumnIndex(COL_DATE)),
                            res.getString(res.getColumnIndex(COL_IMAGES)),
                            res.getString(res.getColumnIndex(COL_QTY)),
                            res.getString(res.getColumnIndex(COL_REL)),
                            res.getString(res.getColumnIndex(COL_NAME_PRICE)),
                            res.getString(res.getColumnIndex(COL_PUBLIX_PRICE)),
//                            "",
//                            "", "", "", "", "", "", "", ""
                            res.getString(res.getColumnIndex(COL_name1)),
                            res.getString(res.getColumnIndex(COL_price1)),
                            res.getString(res.getColumnIndex(COL_PUBLIX_PRICE1)),
                            res.getString(res.getColumnIndex(COL_name2)),
                            res.getString(res.getColumnIndex(COL_price2)),
                            res.getString(res.getColumnIndex(COL_PUBLIX_PRICE2)),
                            res.getString(res.getColumnIndex(COL_name3)),
                            res.getString(res.getColumnIndex(COL_price3)),
                            res.getString(res.getColumnIndex(COL_PUBLIX_PRICE3))
                    )
            );
            res.moveToNext();
        }
        return array_list;
    }

    public int updateOffline(String id, ProductPojo data) {
        int updatedData = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
//            cv.put(COL_TITLE, data.getItemName());
            updatedData = db.update(TABLENAME, cv, COL_ID + "=" + id, null);
        } catch (Exception r) {

        }
        return updatedData;
    }

    public boolean isBookmark(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLENAME + " where " + COL_ID + " = '" + id + "'", null);
        if (res.getCount() > 0)
            return true;
        else
            return false;
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQury = "delete from " + TABLENAME;
        db.execSQL(deleteQury);
    }

    public long getNumberOfItems() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLENAME);
        db.close();
        return count;
    }

    public long updateQuantity(String productId, String quantity) {
        int updatedData = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COL_QTY, quantity);
            updatedData = db.update(TABLENAME, cv, COL_ID + "=" + productId, null);
        } catch (Exception r) {

        }
        return updatedData;
    }


}
