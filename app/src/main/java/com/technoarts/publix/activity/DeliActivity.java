package com.technoarts.publix.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.technoarts.publix.R;
import com.technoarts.publix.adapters.MainCategoryRecyclerAdapter;
import com.technoarts.publix.callback.getDaily;
import com.technoarts.publix.paginationAdapter.CategoryWithPagination;
import com.technoarts.publix.pojo.PojoDaily;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeliActivity extends AppCompatActivity {
    ArrayList<PojoDaily> myListData=new ArrayList<>();
    ArrayList<String> duplicate=new ArrayList<>();
    RecyclerView recyclerView;
    List<ProductPojo>  categoryList=new ArrayList<>();
    MainCategoryRecyclerAdapter adapter;
    ProgressBar loadingBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deli2);
        recyclerView =  findViewById(R.id.categotyrecycler);
        loadingBar =  findViewById(R.id.loadingBar);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        adapter = new MainCategoryRecyclerAdapter( categoryList,DeliActivity.this, new CategoryWithPagination.onClicFilter() {
            @Override
            public void onClick(int pos) {
                Intent ip = new Intent(DeliActivity.this, ProductDetailsAct.class);
                ip.putExtra("SelectedProduct", categoryList.get(pos));
                startActivity(ip);
            }
        });
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getDaily.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        getDaily api = retrofit.create(getDaily.class);
        Call<List<ProductPojo>> call = api.getBannerss();
        call.enqueue(new Callback<List<ProductPojo>>() {
            @Override
            public void onResponse(Call<List<ProductPojo>> call, Response<List<ProductPojo>> response) {
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        if (!duplicate.contains(response.body().get(i).getPro_id())) {
                            duplicate.add(response.body().get(i).getPro_id());
                            categoryList.add(response.body().get(i));
                        }
                    }

                }
//                MainCategoryRecyclerAdapter adapter = new MainCategoryRecyclerAdapter(categoryList,MainCategoryActivity.this);
                recyclerView.setAdapter(adapter);
                loadingBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {

            }
        });
    }
    public void onBackBtnClick(View view) {
        onBackPressed();
    }
}
