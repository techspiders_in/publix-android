package com.technoarts.publix.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;


public class ThankAct extends BaseAct {

    LinearLayout backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_thank;
    }

    @Override
    public void bindObjects(Context mContext) {

    }

    @Override
    public void bindViews() {
        backButton = findViewById(R.id.backButton);
    }

    @Override
    public void bindPixel() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ThankAct.this, MainAct.class));
                finish();
            }
        });
    }

    @Override
    public void bindListener() {

    }

    @Override
    public void bindMethod() {

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ThankAct.this,MainAct.class));
        finish();
    }
}
