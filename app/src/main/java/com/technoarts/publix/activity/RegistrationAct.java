package com.technoarts.publix.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.loginApi;
import com.technoarts.publix.helpers.BasicHelper;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegistrationAct extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout;
    private static EditText full_name, user_name, emailId, mobileNumber, location,
            password_, confirmPassword;
    private static Button signUpButton;
    CheckBox terms_conditions;
    String fullname, user_id, email, mobile, password, confirm_pwd,
            device_type="Android";
    int result;
    String message;
    PrefHelper mPrefHelper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_registration;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
        mPrefHelper = new PrefHelper(mContext);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        full_name = findViewById(R.id.fullName);
        user_name = findViewById(R.id.user_name);
        emailId = findViewById(R.id.userEmailId);
        mobileNumber = findViewById(R.id.mobileNumber);
        password_ = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirmPassword);
        signUpButton = findViewById(R.id.signUpBtn);
        terms_conditions = findViewById(R.id.terms_conditions);
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation(v);
            }
        });

        terms_conditions.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    terms();
                }
            }
        });
    }


    @Override
    public void bindMethod() {
        setUpStatusBar();
    }

    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }

    private void checkValidation(View view) {

        String getFullName = full_name.getText().toString();
        String getEmailId = emailId.getText().toString();
        String getMobileNumber = mobileNumber.getText().toString();
        String getPassword = password_.getText().toString();
        String getConfirmPassword = confirmPassword.getText().toString();

        Pattern p = Pattern.compile(Utils.regEx);
        Matcher m = p.matcher(getEmailId);

        if (getFullName.equals("") || getFullName.length() == 0
                || getEmailId.equals("") || getEmailId.length() == 0
                || getPassword.equals("") || getPassword.length() == 0
                || getConfirmPassword.equals("")
                || getConfirmPassword.length() == 0)

            new BasicHelper().Show_Toast(mContext, view,
                    "All fields are required.");

        else if (!m.find())
            new BasicHelper().Show_Toast(mContext, view,
                    "Your Email Id is Invalid.");

        else if (!getConfirmPassword.equals(getPassword))
            new BasicHelper().Show_Toast(mContext, view,
                    "Both password doesn't match.");

        else if (!terms_conditions.isChecked())
            new BasicHelper().Show_Toast(mContext, view,
                    "Please select Terms and Conditions.");
        else {
            register();
        }
    }

    private ProgressDialog placingDialog;

    private void register() {
        placingDialog = new ProgressDialog(mContext);
        placingDialog.setMessage("Register account, please wait.");
        placingDialog.show();
        placingDialog.setCancelable(false);
        fullname = full_name.getText().toString();
        user_id = user_name.getText().toString();
        email = emailId.getText().toString();
        mobile = mobileNumber.getText().toString();
        password = password_.getText().toString();
        confirm_pwd = confirmPassword.getText().toString();

        Log.e("fullname",full_name.getText().toString());
        Log.e("email",emailId.getText().toString());
        Log.e("password",password_.getText().toString());

        postCode("Android",fullname,email,password);

//        postJson(device_type,fullname, email, p_password);
//        postJson(fullname, user_id, email, mobile, password);

    }
//    private void postJson(final String devicetype , final String fullname, final String email,
//             String password) {
//        RequestBody formBody = new FormBody.Builder()
//                .add("device_type",devicetype)
//                .add("fullname", fullname)
//                .add("email", email)
//                .add("password ", password)
//                .build();
//        Log.e("payment_id", fullname);
//        Log.e("payment", email);
//        final OkHttpClient client = new OkHttpClient();
//        final okhttp3.Request request = new okhttp3.Request.Builder()
//                .url(getResources().getString(R.string.prefix_domain) + "tpxadmin/Users/publix_register")
//                .post(formBody)
//                .build();
//        Thread thread = new Thread(new Runnable() {
//
//            @Override
//            public void run() {
//                try {
//                    okhttp3.Response response = null;
//                    try {
//                        //getresponse();
//                        response = client.newCall(request).execute();
//                        String getresponse = response.body().string();
//                        Log.e("response", getresponse);
//                        JSONArray arr = new JSONArray(getresponse);
//                        JSONObject jsonObj = arr.getJSONObject(0);
//                        result = Integer.parseInt(jsonObj.getString("result"));
//                        Log.e("getresponse", String.valueOf(result));
//                        message = jsonObj.getString("message");
//                        System.out.println("REGISTER : " + message);
//                        Log.e("message", message);
//
//                        if (placingDialog.isShowing()) {
//                            placingDialog.dismiss();
//                        }
//
//                        if (result == 300) {
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    showpopup();
//                                    mPrefHelper.save_STRING(Constant.USERNAME, fullname);
//                                }
//                            });
//                        } else if (result == 200) {
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    duplicate();
//                                }
//                            });
//                        } else {
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    failpopup();
//                                }
//                            });
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                        System.out.println("REGISTER : EROR 1 : " + e.getMessage());
//                        if (placingDialog.isShowing()) {
//                            placingDialog.dismiss();
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    System.out.println("REGISTER : EROR 2 : " + e.getMessage());
//                    if (placingDialog.isShowing()) {
//                        placingDialog.dismiss();
//                    }
//                }
//            }
//        });
//
//        thread.start();
//    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void postCode(final String device_type,final String fullname, final String email, String password) {
        Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("device_type", device_type);
        jsonParams.put("fullname", fullname);
        jsonParams.put("email", email);
        jsonParams.put("password", password);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                (new JSONObject(jsonParams)).toString());

        System.out.println("REGI : response : " + new JSONObject(jsonParams).toString());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(loginApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        loginApi api = retrofit.create(loginApi.class);
        Call<JsonObject> call = api.getUser(jsonParams);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (placingDialog.isShowing()) {
                    placingDialog.dismiss();
                }
                System.out.println("REGI : response : " + response.isSuccessful());
                if (response.isSuccessful()) {
                    String result = response.body().get("result").toString().replaceAll("\"", "");
                    final String msg = response.body().get("message").toString();
                    System.out.println("REGI : response : " + response.body().toString());
                    System.out.println("REGI : response : " + Integer.parseInt(result) + " ****");
                    if (result.equals("200")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                duplicate();
                            }
                        });
                    }else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                failpopup();
                            }
                        });
                    }
                    if (result.equals("300")) {
                        showpopup();
                        mPrefHelper.save_STRING(Constant.USERNAME, fullname);
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new BasicHelper().Show_Toast(mContext, signUpButton, msg);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (placingDialog.isShowing()) {
                    placingDialog.dismiss();
                }

                System.out.println("REGI : 1*error*1 : " + t.getMessage());
            }
        });
    }

//    private void postJson(String device_type,final String fullname, final String email,
//                           String password) {
//        RequestBody formBody = new FormBody.Builder()
//                .add("device_type", device_type)
//                .add("fullname", fullname)
//                .add("email", email)
//                .add("password", password)
//                .build();
//        Log.e("payment_id", fullname);
//        Log.e("payment", email);
        private void postJson (String device_type,final String fullname, final String email
                     ,String password){
            RequestBody formBody = new FormBody.Builder()
                    .add("device_type", device_type)
                    .add("fullname", fullname)
                    .add("email", email)
                    .add("password", password)
                    .build();
            Log.e("payment_id", fullname);
            Log.e("payment", email);
            final OkHttpClient client = new OkHttpClient();
            final okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("https://thepublix.com/tpxadmin/Users/publix_register")
                    .post(formBody)
                    .build();
            Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        okhttp3.Response response = null;

                        try {
                            //getresponse();
                            response = client.newCall(request).execute();
                            String getresponse = response.body().string();
                            Log.e("response", getresponse);
                            JSONArray arr = new JSONArray(getresponse);
                            JSONObject jsonObj = arr.getJSONObject(0);
                            result = Integer.parseInt(jsonObj.getString("result"));
                            Log.e("getresponse", String.valueOf(result));
                            message = jsonObj.getString("message");
                            System.out.println("REGISTER : " + message);
                            Log.e("message", message);

                            if (placingDialog.isShowing()) {
                                placingDialog.dismiss();
                            }
                            if (result == 300) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        showpopup();
                                        mPrefHelper.save_STRING(Constant.USERNAME, fullname);
                                    }
                                });
                            } else if (result == 200) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        duplicate();
                                    }
                                });
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        failpopup();
                                    }
                                });
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            System.out.println("REGISTER : EROR 1 : " + e.getMessage());
                            if (placingDialog.isShowing()) {
                                placingDialog.dismiss();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("REGISTER : EROR 2 : " + e.getMessage());
                        if (placingDialog.isShowing()) {
                            placingDialog.dismiss();
                        }
                    }
                }
            });

            thread.start();
        }


    private void failpopup() {
        ProgressDialog pdialog = new ProgressDialog(mContext);
        pdialog.setMessage("Loading...");
        pdialog.setCancelable(false);
        pdialog.setInverseBackgroundForced(false);
        pdialog.show();
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogfail);
        TextView ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView fail = (TextView) dialog.findViewById(R.id.fail);
        fail.setVisibility(View.VISIBLE);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, LoginAct.class);
                startActivity(i);
            }
        });
        dialog.show();
    }

    private void duplicate() {
        final ProgressDialog pdialog = new ProgressDialog(mContext);
        pdialog.setMessage("Loading...");
        pdialog.setCancelable(false);
        pdialog.setInverseBackgroundForced(false);
        pdialog.show();
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.duplicate);
        TextView ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView fail = (TextView) dialog.findViewById(R.id.fail);
        fail.setVisibility(View.VISIBLE);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                pdialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showpopup() {
        ProgressDialog pdialog = new ProgressDialog(mContext);
        pdialog.setMessage("Loading...");
        pdialog.setCancelable(false);
        pdialog.setInverseBackgroundForced(false);
        pdialog.show();

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogsuccess);
        LinearLayout lay1;
        lay1 = (LinearLayout) dialog.findViewById(R.id.contact_success);
        lay1.setVisibility(View.VISIBLE);
        TextView ok = (TextView) dialog.findViewById(R.id.txt_ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, MainAct.class);
                startActivity(i);
            }
        });
        dialog.show();
    }

    private void terms() {
        final ProgressDialog pdialog = new ProgressDialog(mContext);
        pdialog.setMessage("Loading...");
        pdialog.setCancelable(false);
        pdialog.setInverseBackgroundForced(false);
        pdialog.show();

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.terms_conditions);

        TextView ok = (TextView) dialog.findViewById(R.id.txt_ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                pdialog.dismiss();
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialog.dismiss();
                pdialog.dismiss();
            }
        });

        dialog.show();
    }
}
