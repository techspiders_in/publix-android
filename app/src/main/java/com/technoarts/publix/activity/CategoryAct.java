package com.technoarts.publix.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.MoviesApi;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.paginationAdapter.CategoryWithPagination;
import com.technoarts.publix.pojo.CategoryPojo;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class CategoryAct extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout;
    TextView titleBarTv;
    RecyclerView notificationView;

    MoviesApi api;
    ArrayList<CategoryPojo> categoryList;
    ArrayList<String> duplicate;
    CategoryWithPagination mCategoryWithPagination;
    TextView emptyLogo;
    ProgressBar loadingBar;
    ArrayList<ProductPojo> productList;
    RecyclerView productListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return (R.layout.activity_category);
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
        productList = new ArrayList<>();
        categoryList = new ArrayList<>();
        duplicate = new ArrayList<>();
        api = ServiceGenerator.createService(MoviesApi.class);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        titleBarTv = findViewById(R.id.titleBarTv);
        notificationView = findViewById(R.id.notificationView);

        loadingBar = findViewById(R.id.loadingBar);
        emptyLogo = findViewById(R.id.emptyLogo);
        productListView = findViewById(R.id.productListView);
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void bindMethod() {
        productListView.setLayoutManager(new GridLayoutManager(mContext, 3));

        mCategoryWithPagination = new CategoryWithPagination(mContext, categoryList, new CategoryWithPagination.onClicFilter() {
            @Override
            public void onClick(int pos) {
                Intent ip = new Intent(mContext, CatItemAct.class);
                ip.putExtra("CatName", categoryList.get(pos));
                startActivity(ip);
            }
        });

        mCategoryWithPagination.setLoadMoreListener(new CategoryWithPagination.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                productListView.post(new Runnable() {
                    @Override
                    public void run() {
                        int index = categoryList.size() - 1;
                        loadMore(index);
                    }
                });
            }
        });

        productListView.setHasFixedSize(true);
        productListView.setAdapter(mCategoryWithPagination);
        load(0);

    }

    private void load(int index) {
        Call<List<CategoryPojo>> call = api.getMovies();
        call.enqueue(new Callback<List<CategoryPojo>>() {


            @Override
            public void onResponse(Call<List<CategoryPojo>> call, retrofit2.Response<List<CategoryPojo>> response) {
                emptyLogo.setVisibility(View.GONE);
                loadingBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
//                        if (!duplicate.contains(response.body().get(i).getCat_id())) {
//                            duplicate.add(response.body().get(i).getCat_id());
                            categoryList.add(response.body().get(i));
//                        }
                    }
//                    data.addAll(response.body());
                    mCategoryWithPagination.notifyDataChanged();
                } else {
                }

                Collections.sort(categoryList, new Comparator<CategoryPojo>() {
                    public int compare(CategoryPojo v1, CategoryPojo v2) {
                        return v1.getCat_id().compareTo(v2.getCat_id());
                    }
                });

                if (productList.size() < 3) {
                    mCategoryWithPagination.setMoreDataAvailable(false);
                }
            }

            @Override
            public void onFailure(Call<List<CategoryPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
                if (isNetworkAvailable()) {
                    emptyLogo.setText("product not available yet!");
                } else {
                    emptyLogo.setText("You are offline, please try again letter");
                    openSupportContactDialog();
                }
                emptyLogo.setVisibility(View.VISIBLE);
                System.out.println("ERROR >>>> " + t.getMessage());
            }
        });
    }

    private void loadMore(int index) {
        //add loading progress view
        categoryList.add(new CategoryPojo("load"));
        mCategoryWithPagination.notifyItemInserted(categoryList.size() - 1);

        Call<List<CategoryPojo>> call = api.getMovies();
        call.enqueue(new Callback<List<CategoryPojo>>() {
            @Override
            public void onResponse(Call<List<CategoryPojo>> call, retrofit2.Response<List<CategoryPojo>> response) {
                loadingBar.setVisibility(View.GONE);
                emptyLogo.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    //remove loading view
                    categoryList.remove(categoryList.size() - 1);

                    List<CategoryPojo> result = response.body();
                    if (result.size() > 0) {
                        //add loaded data
                        for (int i = 0; i < result.size(); i++) {
//                            if (!duplicate.contains(result.get(i).getCat_id())) {
//                                duplicate.add(result.get(i).getCat_id());
                                categoryList.add(result.get(i));
//                            }
                        }
//                        data.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        mCategoryWithPagination.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
                        Toast.makeText(mContext, "No More Data Available", Toast.LENGTH_LONG).show();
                    }
                    Collections.sort(categoryList, new Comparator<CategoryPojo>() {
                        public int compare(CategoryPojo v1, CategoryPojo v2) {
                            return v1.getCat_name().compareTo(v2.getCat_name());
                        }
                    });
                    mCategoryWithPagination.notifyDataChanged();
                    //should call the custom method adapter.notifyDataChanged here to get the correct loading status
                } else {
                }
            }

            @Override
            public void onFailure(Call<List<CategoryPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
                if (isNetworkAvailable()) {
                    emptyLogo.setText("product not available yet!");
                } else {
                    emptyLogo.setText("You are offline, please try again letter");
                    openSupportContactDialog();

                }
                emptyLogo.setVisibility(View.VISIBLE);
                System.out.println("ERROR >>>> " + t.getMessage());
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}