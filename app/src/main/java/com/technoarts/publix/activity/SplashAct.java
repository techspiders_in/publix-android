package com.technoarts.publix.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.FCMTokenApi;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.pojo.ResponseModel;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashAct extends BaseAct {
    Context mContext;
    private long SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_splash;

    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void bindViews() {
        ImageView image = findViewById(R.id.image);
        Glide.with(this)
                .load(R.drawable.publix)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(new DrawableImageViewTarget(image));

    }

    @Override
    public void bindPixel() {

    }

    @Override
    public void bindListener() {

    }

    @Override
    public void bindMethod() {

        final String[] androidId = {Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)};

        final String name = androidId[0];

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String deviceToken = instanceIdResult.getToken();
                tokenpost(deviceToken, name);

                System.out.println("NOTIFICATION : device token : " + deviceToken);
                System.out.println("NOTIFICATION : device id : " + name);
            }
        });

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(mContext, MainAct.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);*/
    }

    private void tokenpost(String newToken, String androidId) {
        Map<String, String> jsonParams = new HashMap<>();
        jsonParams.put("tokenid", newToken);
        jsonParams.put("deviceid", androidId);


        Call<ResponseModel> api = ServiceGenerator.createService(FCMTokenApi.class).doRegistrationToken(jsonParams);
        api.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                Log.e("FCM", "Response" + response.isSuccessful());
                if (response.isSuccessful()) {
                    Intent i = new Intent(mContext, MainAct.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(mContext, "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e("FCM", "Response" + t.getLocalizedMessage());

                Intent i = new Intent(mContext, MainAct.class);
                startActivity(i);
                finish();

            }
        });
    }
}
