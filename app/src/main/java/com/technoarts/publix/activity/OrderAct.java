package com.technoarts.publix.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.adapters.Order_Adapter;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.Order_Model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class OrderAct extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout;

    TextView fromdate, todate, txt_totalprice;
    String sdate, edate, month, day, data;


    ListView listView;
    private ArrayList<Order_Model> catList;
    private Order_Adapter LAdapter;
    String user_id;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_order;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        listView = (ListView) findViewById(R.id.list);
        catList = new ArrayList<>();
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void bindMethod() {
        setUpStatusBar();
//        order();
        user_id = new PrefHelper(mContext).get_STRING(Constant.checkout_user_id, "");
        System.out.println("LOGIN USER : " + user_id);
//        postJson();
        new bg().execute();
    }

    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }

    void temp() {
        RequestBody formBody = new FormBody.Builder()
                .add("login_id", user_id /*"90301171XX"*/)
                .build();
        Log.e("sdate", user_id);
        final OkHttpClient client = new OkHttpClient();
        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url(getResources().getString(R.string.prefix_domain) + "Orders/order_date")
                .post(formBody)
                .build();

        final okhttp3.Response[] response = {null};
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            try {
                response[0] = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                data = response[0].body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (data != null) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray jsonArray = jsonObject.getJSONArray("order_date");
                    Log.d("-------service------", String.valueOf(jsonArray));
//                    System.out.println("ORDER DETAILS : "+jsonArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject itemObj = (JSONObject) jsonArray.get(i);
                        String dt = itemObj.getString("order_date");
                        Order_Model cat = new Order_Model(
                                itemObj.getString("order_no"),
                                itemObj.getString("order_date"),
                                itemObj.getString("total_amount"),
                                itemObj.getString("order_id"),
                                itemObj.getString("order_status"),
                                itemObj.getString("mobile_no")
                        );
                        catList.add(cat);
                        Log.d("-------catlist------", String.valueOf(catList));

                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                    System.out.println("ORDER DETAILS : " + e.getMessage());
                }
            } else {

            }

        } catch (Exception e) {
            System.out.println("ORDER DETAILS : " + e.getMessage());
        }
    }

    private ProgressDialog placingDialog;

    class bg extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            placingDialog = new ProgressDialog(mContext);
            placingDialog.setMessage("Fetching data, please wait.");
            placingDialog.show();
            placingDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            temp();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (placingDialog.isShowing()) {
                placingDialog.dismiss();
            }
            LAdapter = new Order_Adapter(catList, /*getLayoutInflater(),*/getApplicationContext());
            listView.setAdapter(LAdapter);
        }
    }

}
