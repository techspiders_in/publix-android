package com.technoarts.publix.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.NotificationApi;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.NotificationVO;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.paginationAdapter.NotificationWithPagination;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class NotificationAct extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout;
    TextView titleBarTv;
    RecyclerView notificationView;

    NotificationApi api;
    ArrayList<String> duplicate;
    ArrayList<NotificationVO> notificationList;
    TextView emptyLogo;
    ProgressBar loadingBar;
    NotificationWithPagination mNotificationWithPagination;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return (R.layout.activity_notification);
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);

        duplicate = new ArrayList<>();
        notificationList = new ArrayList<>();
        api = ServiceGenerator.createService(NotificationApi.class);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        titleBarTv = findViewById(R.id.titleBarTv);
        notificationView = findViewById(R.id.notificationView);

        loadingBar = findViewById(R.id.loadingBar);
        emptyLogo = findViewById(R.id.emptyLogo);
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void bindMethod() {
        notificationView.setLayoutManager(new GridLayoutManager(mContext, 1));

        mNotificationWithPagination = new NotificationWithPagination(mContext, notificationList, new onProductClick() {
            @Override
            public void click(int position) {
                Intent ip = new Intent(mContext, ViewNotificationAct.class);
                ip.putExtra("selectedNotification",notificationList.get(position));
                startActivity(ip);
            }
        });

        mNotificationWithPagination.setLoadMoreListener(new NotificationWithPagination.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                notificationView.post(new Runnable() {
                    @Override
                    public void run() {
                        int index = notificationList.size() - 1;
                        loadMore(index);
                    }
                });
            }
        });

        notificationView.setHasFixedSize(true);
        notificationView.setAdapter(mNotificationWithPagination);
        load(0);

    }


    private void load(int index) {
        Call<List<NotificationVO>> call = api.getMovies(index);
        call.enqueue(new Callback<List<NotificationVO>>() {

            @Override
            public void onResponse(Call<List<NotificationVO>> call, retrofit2.Response<List<NotificationVO>> response) {
                loadingBar.setVisibility(View.GONE);
                emptyLogo.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        if (!duplicate.contains(response.body().get(i).getNoti_id())) {
                            duplicate.add(response.body().get(i).getNoti_id());
                            notificationList.add(response.body().get(i));
                        }
                    }
                    mNotificationWithPagination.notifyDataChanged();
                } else {
                }

//                Collections.sort(notificationList, new Comparator<NotificationVO>() {
//                    public int compare(NotificationVO v1, NotificationVO v2) {
//                        return v1.getTitle().compareTo(v2.getTitle());
//                    }
//                });

                if (notificationList.size() <9) {
                    mNotificationWithPagination.setMoreDataAvailable(false);
                }
            }

            @Override
            public void onFailure(Call<List<NotificationVO>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
                if (isNetworkAvailable()) {
                    emptyLogo.setText("product not available yet!");
                } else {
                    emptyLogo.setText("You are offline, please try again letter");
                    openSupportContactDialog();
                }
                emptyLogo.setVisibility(View.VISIBLE);
                System.out.println("ERROR >>>> " + t.getMessage());
            }
        });
    }

    private void loadMore(int index) {
        //add loading progress view
        notificationList.add(new NotificationVO("load"));
        mNotificationWithPagination.notifyItemInserted(notificationList.size() - 1);

        Call<List<NotificationVO>> call = api.getMovies(index);
        call.enqueue(new Callback<List<NotificationVO>>() {
            @Override
            public void onResponse(Call<List<NotificationVO>> call, retrofit2.Response<List<NotificationVO>> response) {
                loadingBar.setVisibility(View.GONE);
                emptyLogo.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    //remove loading view
                    notificationList.remove(notificationList.size() - 1);

                    List<NotificationVO> result = response.body();
                    if (result.size() > 0) {
                        //add loaded data
                        for (int i = 0; i < result.size(); i++) {
                            if (!duplicate.contains(result.get(i).getNoti_id())) {
                                duplicate.add(result.get(i).getNoti_id());
                                notificationList.add(result.get(i));
                            }
                        }
//                        data.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        mNotificationWithPagination.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
                        Toast.makeText(mContext, "No More Data Available", Toast.LENGTH_LONG).show();
                    }
//                    Collections.sort(notificationList, new Comparator<NotificationVO>() {
//                        public int compare(NotificationVO v1, NotificationVO v2) {
//                            return v1.getTitle().compareTo(v2.getTitle());
//                        }
//                    });
                    mNotificationWithPagination.notifyDataChanged();
                    //should call the custom method adapter.notifyDataChanged here to get the correct loading status
                } else {
                }
            }

            @Override
            public void onFailure(Call<List<NotificationVO>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
                if (isNetworkAvailable()) {
                    emptyLogo.setText("product not available yet!");
                } else {
                    emptyLogo.setText("You are offline, please try again letter");
                    openSupportContactDialog();

                }
                emptyLogo.setVisibility(View.VISIBLE);
                System.out.println("ERROR >>>> " + t.getMessage());
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }
}
