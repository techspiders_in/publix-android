package com.technoarts.publix.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.adapters.Order_Detail_Adapter;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.Orderlist;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class OrderDetails extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout;

    ListView listView;
    ArrayList<String> duplicate;
    Button cart;
    String product_name, product_id, product_image, getProduct_price, order_id, data;
    private ArrayList<Orderlist> catList;
    TextView proNumber, totalPay, deliveryCharges, maintotal;
    private Order_Detail_Adapter catadapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_order_details;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
        duplicate = new ArrayList<>();
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        proNumber = findViewById(R.id.proNumber);
        totalPay = findViewById(R.id.totalPay);
        deliveryCharges = findViewById(R.id.deliveryCharges);
        maintotal = findViewById(R.id.maintotal);

        listView = (ListView) findViewById(R.id.orderdetail);
        catList = new ArrayList<>();
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void bindMethod() {
        setUpStatusBar();
        Intent i = getIntent();
        order_id = i.getStringExtra("order_id");
        proNumber.setText("ORDER NO." + order_id);
        Log.e("order_id", order_id);
//        postJson();
        new bg().execute();
    }

    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }

    private String postJson() {
        RequestBody formBody = new FormBody.Builder()
                .add("order_id", order_id/*"Jurassic@Park.com"*/)
                .build();
        Log.e("order_id", order_id);

        final OkHttpClient client = new OkHttpClient();
        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url(getResources().getString(R.string.prefix_domain) + "Orders/order_details")
                .post(formBody)
                .build();

        final okhttp3.Response[] response = {null};
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            try {
                response[0] = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Log.e("response", response[0].body().string());
            try {
                data = response[0].body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e("detailresponsedata", data);

            if (data != "") {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray jsonArray = jsonObject.getJSONArray("order_details");
                    Log.d("-------service------", String.valueOf(jsonArray));
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject itemObj = (JSONObject) jsonArray.get(i);
                        if (!duplicate.contains(itemObj.getString("pro_name"))) {
                            duplicate.add(itemObj.getString("pro_name"));
                            Orderlist cat = new Orderlist(
                                    itemObj.getString("pro_name"),
                                    itemObj.getString("qty"),
                                    itemObj.getString("pro_amount"),
                                    itemObj.getString("pro_img"),
                                    itemObj.getString("deliverycharge")
                            );
                            catList.add(cat);
                            Log.d("-------catlist------", String.valueOf(catList));
                            System.out.println("ORDER DETAILS : " + itemObj.getString("deliverycharge"));
                        }
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                    System.out.println("ORDER DETAILS : " + e.getMessage());
                }
            } else {
                //      Toast.makeText(getApplicationContext(),"Data Not Found",Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            System.out.println("ORDER DETAILS : " + e.getMessage());
        }
        return null;
    }

    private ProgressDialog placingDialog;

    class bg extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            placingDialog = new ProgressDialog(mContext);
            placingDialog.setMessage("Fetching data, please wait.");
            placingDialog.show();
            placingDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            postJson();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (placingDialog.isShowing()) {
                placingDialog.dismiss();
            }
            int amount = 0;
            for (int i = 0; i < catList.size(); i++) {
                amount = amount + Integer.parseInt(catList.get(i).getPro_amount());
            }

            amount = amount * Integer.parseInt(catList.get(0).getQty());

            totalPay.setText("Amount : " + amount);
            deliveryCharges.setText("Delivery Charges : " + catList.get(0).getDeliverycharge());
            maintotal.setText("Total Amount : " + (amount + Integer.parseInt(catList.get(0).getDeliverycharge())));

            catadapter = new Order_Detail_Adapter(catList, /*getLayoutInflater(),*/getApplicationContext());

            listView.setAdapter(catadapter);
        }
    }
}
