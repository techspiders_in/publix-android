package com.technoarts.publix.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.adapters.SpinAdapter;
import com.technoarts.publix.asyncs.LoadArea;
import com.technoarts.publix.callback.AddressAPI;
import com.technoarts.publix.callback.BookOrderAPI;
import com.technoarts.publix.dbhelper.DbCart;
import com.technoarts.publix.dbhelper.DbDeliveryCharges;
import com.technoarts.publix.helpers.BasicHelper;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.DeliveryAreaPojo;
import com.technoarts.publix.pojo.GenericResponseModel;
import com.technoarts.publix.pojo.UserAddressModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CheckoutAct extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout;
    EditText edtAddress;
    EditText edtPhone;
    EditText edtMobile;
    EditText edtMessage;
    EditText edtGst;
    CheckBox chkterms;
    CheckBox chPayOnDelivery;
    TextView btnConfirm;
    String getcoupon = null, checkout_user_id, getgrandtotal, getname, getpincode,
            getdropmsg, getPhone;
    String getSelectedArea = "Select Area...";
    public static JSONObject finaldatalist;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    PrefHelper mPrefHelper;
    RadioGroup gstGroups;
    boolean isGst = false;
    TextView errorTv, tvarea;
    Spinner deliveryAreaSp;
    ArrayList<DeliveryAreaPojo> dArea;
    String mAreaId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_checkout;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
        finaldatalist = new JSONObject();
        mPrefHelper = new PrefHelper(mContext);
        dArea = new ArrayList<>();
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        edtAddress = findViewById(R.id.edtAddress);
        edtPhone = findViewById(R.id.edtPhone);
        edtMessage = findViewById(R.id.edtMessage);
        chkterms = findViewById(R.id.chkterms);
        btnConfirm = findViewById(R.id.btnConfirm);
        edtMobile = findViewById(R.id.edtMobile);
        gstGroups = findViewById(R.id.gstGroups);
        edtGst = findViewById(R.id.edtGst);
        errorTv = findViewById(R.id.errorTv);
        deliveryAreaSp = findViewById(R.id.deliveryAreaSp);
        tvarea = findViewById(R.id.tvarea);
        chPayOnDelivery = findViewById(R.id.chPayOnDelivery);
        String area = getIntent().getStringExtra("area");

        tvarea.setText(area);
        mAreaId = getIntent().getIntExtra("areaId", 0) + "";
        getAddressFromPreviousOrder();
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    String address, phone, message, userName;

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                address = edtAddress.getText().toString();
                phone = edtPhone.getText().toString();
                message = edtMessage.getText().toString();
                if (address.equals("") || phone.length() == 0
                        || phone.equals("")) {
                    new BasicHelper().Show_Toast(getApplicationContext(), v,
                            "All fields are required.");
                } else if (!chkterms.isChecked()) {
                    new BasicHelper().Show_Toast(getApplicationContext(), v,
                            "please accept our terms and conditions");
//                } else if (getSelectedArea.equals("Select Area...")) {
//                    new BasicHelper().Show_Toast(getApplicationContext(), v,
//                            "please select area");
                }else if (!chPayOnDelivery.isChecked()){
                    new BasicHelper().Show_Toast(getApplicationContext(), v,
                            "Please select Pay on Delivery.");
                }

                else if (!isValidMobile(edtMobile.getText().toString())) {
                    new BasicHelper().Show_Toast(getApplicationContext(), v,
                            "please enter valid mobile number");
                } else if (isGst) {
                    if (edtGst.getText().toString().length() != 15) {
                        new BasicHelper().Show_Toast(getApplicationContext(), v,
                                "please enter valid GST number");
                    } else {
                        btnConfirm.setClickable(false);
                        getdata();
                    }
                } else {
                    //orderCode
                    btnConfirm.setClickable(false);
                    getdata();
                }
            }
        });

        gstGroups.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                boolean isChecked = checkedRadioButton.isChecked();
                if (isChecked) {
                    if (checkedRadioButton.getText().equals("YES")) {
                        edtGst.setVisibility(View.VISIBLE);
                        isGst = true;
                    } else {
                        isGst = false;
                        edtGst.setVisibility(View.GONE);
                    }
                }
            }
        });

        chkterms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    showpopup();
                }
            }
        });

        deliveryAreaSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                getSelectedArea = dArea.get(position).getAreaname();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    @Override
    public void bindMethod() {
        setUpStatusBar();
        userName = new PrefHelper(mContext).get_STRING(Constant.USERNAME, "");
        for (int i = 0; i < new DbCart(mContext).getAllBookmark().size(); i++) {
            System.out.println("CART ITEM : " + new DbCart(mContext).getAllBookmark().get(i).getPro_name());
        }

        new LoadArea(mContext, new LoadArea.onLoadComplete() {
            @Override
            public void onComplete(ArrayList<DeliveryAreaPojo> data) {
                dArea = data;
                deliveryAreaSp.setAdapter(new SpinAdapter(mContext,
                        android.R.layout.simple_spinner_item,
                        data));
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }

    private ProgressDialog placingDialog;

    private void getdata() {
        placingDialog = new ProgressDialog(mContext);
        placingDialog.setMessage("placing order, please wait.");
        placingDialog.show();
        placingDialog.setCancelable(false);
        try {
            initCart();
            JSONObject jsonObject;
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < new DbCart(mContext).getAllBookmark().size(); i++) {
                jsonObject = new JSONObject();
                jsonObject.put("prodid", new DbCart(mContext).getAllBookmark().get(i).getPro_id());
                jsonObject.put("qty", new DbCart(mContext).getAllBookmark().get(i).getQty());
                jsonArray.put(jsonObject);
            }
            System.out.println("ORDER : json array : " + String.valueOf((jsonArray)));
            finaldatalist.put("productid", jsonArray);
            finaldatalist.put("grandtotal", mPrefHelper.get_STRING(Constant.getgrandtotal, "0"));
            finaldatalist.put("user_login_id", mPrefHelper.get_STRING(Constant.checkout_user_id, "00"));
            finaldatalist.put("address", address);
            finaldatalist.put("message", message);
            finaldatalist.put("gst_number", edtGst.getText().toString());
            finaldatalist.put("mobile_no", edtMobile.getText().toString());

            finaldatalist.put("landmark", phone);
            finaldatalist.put("selected_area_id", mAreaId);


        } catch (JSONException e) {
            e.printStackTrace();
            errorTv.setText(e.getMessage());
        }
        Log.e("finaldatalist123333", String.valueOf(finaldatalist));
        // Postdata();
        bookOrder();


    }

   /* private void Postdata() {
        String postUrl = mContext.getResources().getString(R.string.prefix_domain) + "Orders/checkout";
        String postBody = finaldatalist.toString();

        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        try {
            postRequest(postUrl, postBody);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("ORDER : " + e.getMessage());
        }
    }*/

    private void bookOrder() {
        CartAPIItem item = new CartAPIItem(mPrefHelper.get_STRING(Constant.checkout_user_id, "00"),
                address, mPrefHelper.get_STRING(Constant.getgrandtotal, "0"),
                edtMobile.getText().toString(),
                edtGst.getText().toString(),
                message,
                phone,
                mAreaId);

        ArrayList<ProductIem> productid = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < new DbCart(mContext).getAllBookmark().size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                productid.add(new ProductIem(new DbCart(mContext).getAllBookmark().get(i).getPro_id(), new DbCart(mContext).getAllBookmark().get(i).getQty(),
                        new DbCart(mContext).getAllBookmark().get(i).getPrice1()));
                jsonObject.put("prodid", new DbCart(mContext).getAllBookmark().get(i).getPro_id());
                jsonObject.put("qty", new DbCart(mContext).getAllBookmark().get(i).getQty());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        item.setProductid(productid);


        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_login_id", mPrefHelper.get_STRING(Constant.checkout_user_id, "00"));
        jsonParams.put("productid", jsonArray.toString().replaceAll("\\\\", ""));
        jsonParams.put("grandtotal", mPrefHelper.get_STRING(Constant.getgrandtotal, "0"));
        jsonParams.put("address", address);
        jsonParams.put("message", message);
        jsonParams.put("gst_number", edtGst.getText().toString());
        jsonParams.put("mobile_no", edtMobile.getText().toString());
        jsonParams.put("landmark", phone);
        jsonParams.put("selected_area_id", mAreaId);


        retrofit2.Call<GenericResponseModel> api = ServiceGenerator.createService(BookOrderAPI.class).bookOrder(item);
        api.enqueue(new retrofit2.Callback<GenericResponseModel>() {
            @Override
            public void onResponse(retrofit2.Call<GenericResponseModel> call, retrofit2.Response<GenericResponseModel> response) {

                String message = response.body().getMessage();
                if (response.body().getResult().equals("300")) {
                    if (placingDialog.isShowing()) {
                        placingDialog.dismiss();
                    }
                    System.out.println("ORDER : amount " + getgrandtotal);
                    System.out.println("ORDER : coupon " + getcoupon);
                    System.out.println("ORDER : user id " + checkout_user_id);
                    System.out.println("ORDER : name " + getname);
                    System.out.println("ORDER : pin " + getpincode);
                    System.out.println("ORDER : gst " + edtGst.getText().toString());
                    System.out.println("ORDER : message " + getdropmsg);
                    System.out.println("ORDER : email address " + mPrefHelper.get_STRING(Constant.EMAIL, ""));
                    mPrefHelper.removeAll(Constant.getcoupon);
                    mPrefHelper.removeAll(Constant.getdropmsg);
                    mPrefHelper.removeAll(Constant.getgrandtotal);
                    mPrefHelper.removeAll(Constant.getpincode);
                    new DbCart(mContext).deleteAll();
                    new DbDeliveryCharges(mContext).deleteAll();
                    btnConfirm.setClickable(true);
                    finish();
                    Intent ip = new Intent(mContext, ThankAct.class);
                    startActivity(ip);
                } else {
                    if (placingDialog.isShowing()) {
                        placingDialog.dismiss();
                    }
                    new BasicHelper().Show_Toast(getApplicationContext(), CheckoutAct.this.btnConfirm,
                            "something went wrong please try again letter..");
                }
                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(retrofit2.Call<GenericResponseModel> call, Throwable t) {
                Log.e("FCM", "Response" + t.getLocalizedMessage());
                if (placingDialog.isShowing()) {
                    placingDialog.dismiss();
                }
                new BasicHelper().Show_Toast(getApplicationContext(), CheckoutAct.this.btnConfirm,
                        "something went wrong please try again letter..");

            }
        });
    }

    void postRequest(String postUrl, String postBody) throws IOException {


        OkHttpClient client = new OkHttpClient();

        RequestBody body = RequestBody.create(JSON, postBody);

        final Request request = new Request.Builder()
                .url(postUrl)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                System.out.println("ORDER : " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                try {
                    String getresponse = response.body().string();
                    JSONObject oo = new JSONObject(getresponse);
                    int result = Integer.parseInt(oo.getString("result"));
                    System.out.println("ORDER : result : " + result);
                    if (result == 300) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (placingDialog.isShowing()) {
                                    placingDialog.dismiss();
                                }
                                System.out.println("ORDER : amount " + getgrandtotal);
                                System.out.println("ORDER : coupon " + getcoupon);
                                System.out.println("ORDER : user id " + checkout_user_id);
                                System.out.println("ORDER : name " + getname);
                                System.out.println("ORDER : pin " + getpincode);
                                System.out.println("ORDER : gst " + edtGst.getText().toString());
                                System.out.println("ORDER : message " + getdropmsg);
                                System.out.println("ORDER : email address " + mPrefHelper.get_STRING(Constant.EMAIL, ""));
                                mPrefHelper.removeAll(Constant.getcoupon);
                                mPrefHelper.removeAll(Constant.getdropmsg);
                                mPrefHelper.removeAll(Constant.getgrandtotal);
                                mPrefHelper.removeAll(Constant.getpincode);
                                new DbCart(mContext).deleteAll();
                                new DbDeliveryCharges(mContext).deleteAll();
                                btnConfirm.setClickable(true);
                                finish();
                                Intent ip = new Intent(mContext, ThankAct.class);
                                startActivity(ip);
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (placingDialog.isShowing()) {
                                    placingDialog.dismiss();
                                }
                                new BasicHelper().Show_Toast(getApplicationContext(), CheckoutAct.this.btnConfirm,
                                        "something went wrong please try again letter..");
                            }
                        });
                    }
                } catch (Exception e) {
                    if (placingDialog.isShowing()) {
                        placingDialog.dismiss();
                        System.out.println("ORDER : result : " + e.getMessage());
                    }
//                    errorTv.setText(e.getMessage());
//                    System.out.println("ORDER : error** : " + e.getMessage());
                }

            }
        });
    }

    void initCart() {
        getgrandtotal = mPrefHelper.get_STRING(Constant.getgrandtotal, "");
        getcoupon = mPrefHelper.get_STRING(Constant.getcoupon, "jig");
        checkout_user_id = mPrefHelper.get_STRING(Constant.checkout_user_id, "");
        getname = address;
        getpincode = phone;
        getdropmsg = message;
        getPhone = edtMobile.getText().toString();
    }

    private void showpopup() {
        final ProgressDialog pdialog = new ProgressDialog(mContext);
        pdialog.setMessage("Loading...");
        pdialog.setCancelable(false);
        pdialog.setInverseBackgroundForced(false);
        pdialog.show();

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.terms_conditions_order);

        TextView ok = (TextView) dialog.findViewById(R.id.txt_ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                pdialog.dismiss();
            }
        });
        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialog.dismiss();
                pdialog.dismiss();
            }
        });
    }

    private void getAddressFromPreviousOrder() {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", mPrefHelper.get_STRING(Constant.checkout_user_id, "00"));
        ServiceGenerator.createService(AddressAPI.class).getUserAddress(param).enqueue(new retrofit2.Callback<ArrayList<UserAddressModel>>() {
            @Override
            public void onResponse(retrofit2.Call<ArrayList<UserAddressModel>> call, retrofit2.Response<ArrayList<UserAddressModel>> response) {
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        UserAddressModel item = response.body().get(0);
                        edtAddress.setText(item.getAddress());
                        edtPhone.setText(item.getLandmark());
                        edtMobile.setText(item.getMobile_no());
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ArrayList<UserAddressModel>> call, Throwable t) {
                Log.e("GetAddress", t.getLocalizedMessage() + "");
            }
        });

    }


    public class CartAPIItem {
        private String user_login_id;
        private String address;
        private ArrayList<ProductIem> productid;
        private String grandtotal;
        private String mobile_no;
        private String gst_number;
        private String message;
        private String landmark;
        private String selected_area_id;

        public CartAPIItem(String user_login_id, String address, String grandtotal, String mobile_no, String gst_number, String message, String landmark, String selected_area_id) {
            this.user_login_id = user_login_id;
            this.address = address;
            this.grandtotal = grandtotal;
            this.mobile_no = mobile_no;
            this.gst_number = gst_number;
            this.message = message;
            this.landmark = landmark;
            this.selected_area_id = selected_area_id;
        }

        public String getUser_login_id() {
            return user_login_id;
        }

        public void setUser_login_id(String user_login_id) {
            this.user_login_id = user_login_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public ArrayList<ProductIem> getProductid() {
            return productid;
        }

        public void setProductid(ArrayList<ProductIem> productid) {
            this.productid = productid;
        }

        public String getGrandtotal() {
            return grandtotal;
        }

        public void setGrandtotal(String grandtotal) {
            this.grandtotal = grandtotal;
        }

        public String getMobile_no() {
            return mobile_no;
        }

        public void setMobile_no(String mobile_no) {
            this.mobile_no = mobile_no;
        }

        public String getGst_number() {
            return gst_number;
        }

        public void setGst_number(String gst_number) {
            this.gst_number = gst_number;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getSelected_area_id() {
            return selected_area_id;
        }

        public void setSelected_area_id(String selected_area_id) {
            this.selected_area_id = selected_area_id;
        }
    }

    public class ProductIem {
        private String prodid;
        private String qty;
        private String price;

        public ProductIem(String productid, String qty, String price) {
            this.prodid = productid;
            this.qty = qty;
            this.price = price;
        }

        public String getProductid() {
            return prodid;
        }

        public void setProductid(String productid) {
            this.prodid = productid;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice() {
            return price;
        }
    }

}
