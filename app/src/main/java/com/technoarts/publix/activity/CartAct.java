package com.technoarts.publix.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.adapters.CartListAdapter;
import com.technoarts.publix.callback.AreaApi;
import com.technoarts.publix.callback.OrderApi;
import com.technoarts.publix.dbhelper.DbCart;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.AreaModel;
import com.technoarts.publix.pojo.MiniOrderItem;
import com.technoarts.publix.pojo.ProductPojo;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CartAct extends BaseAct implements AdapterView.OnItemSelectedListener {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout, buyLayout, buyNowLayout, couponLayout, mainLayoutBottom;
    RecyclerView cartListView;
    DbCart mDbCart;
    TextView totalAmountTv, emptyTv, grandTotalTv, dcAmountTv, tvMinimumOrderCharge;
    int couponPrice, tempCoupon;
    PrefHelper mPrefHelper;
    Spinner spinner;
    List<AreaModel> areaModels = new ArrayList<>();
    List<ProductPojo> selecteditems = new ArrayList<>();
    ArrayList<ProductPojo> selecteditems1 = new ArrayList<>();
    String tvMrp;
    int isFlagVariant = 1;
    CartListAdapter cartListAdapter;
    //    private BroadcastReceiver mMessageReceiver;
    String ItemName, ItemName1, ItemName2;
    ArrayAdapter<String> spinnerArrayAdapter;
    String pp;
    int delivery, pricet, expcharge;
    private OrderApi api;

    RadioGroup rgDeliveryType;
    private List<AreaModel> CategoryList;
    private String minimumOrderCharge = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {

        return R.layout.activity_cart;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
        mDbCart = new DbCart(mContext);
        mPrefHelper = new PrefHelper(mContext);
        api = ServiceGenerator.createService(OrderApi.class);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        cartListView = findViewById(R.id.cartListView);
        buyLayout = findViewById(R.id.buyLayout);
        buyNowLayout = findViewById(R.id.buyNowLayout);
        totalAmountTv = findViewById(R.id.totalAmountTv);
//        couponLayout = findViewById(R.id.couponLayout);
        emptyTv = findViewById(R.id.emptyTv);
        grandTotalTv = findViewById(R.id.grandTotalTv);
        dcAmountTv = findViewById(R.id.dcAmountTv);
        tvMinimumOrderCharge = findViewById(R.id.tvMinimumOrderCharge);
        mainLayoutBottom = findViewById(R.id.mainLayoutBottom);
        spinner = findViewById(R.id.spinner);
        initSpinnerType();
        spinner.setOnItemSelectedListener(this);

        rgDeliveryType = findViewById(R.id.rgDeliveryType);
        rgDeliveryType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                String area = spinnerArrayAdapter.getItem(spinner.getSelectedItemPosition());
                postCode(area);
                if (checkedId == R.id.rbExpress)
                    Toast.makeText(mContext, "Get your order delivered in 90 minutes.", Toast.LENGTH_LONG).show();
            }
        });

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void postCode(final String area) {
        final Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("area", area);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                (new JSONObject(jsonParams)).toString());
        System.out.println("REGI : response : " + new JSONObject(jsonParams).toString());
        Log.e("area", new JSONObject(jsonParams).toString());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AreaApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AreaApi api = retrofit.create(AreaApi.class);
        Call<List<AreaModel>> call = api.getAreaList(jsonParams);
        call.enqueue(new Callback<List<AreaModel>>() {
            @Override
            public void onResponse(Call<List<AreaModel>> call, Response<List<AreaModel>> response) {
//                System.out.println("REGI : response : " + response.isSuccessful());
                if (response != null) {
                    if (response.isSuccessful()) {
                        List<AreaModel> area = response.body();
                        for (int i = 0; i < area.size(); i++) {
                            mPrefHelper.save_STRING(Constant.rate, area.get(i).getRate());
                            mPrefHelper.save_STRING(Constant.area, area.get(i).getArea());
                            mPrefHelper.save_STRING(Constant.expcharge, area.get(i).getExpcharge());
                            pp = mPrefHelper.get_STRING(Constant.rate, "");
                            delivery = Integer.parseInt(mPrefHelper.get_STRING(Constant.rate, ""));
                            expcharge = Integer.parseInt(mPrefHelper.get_STRING(Constant.expcharge, ""));
                            Log.e("arealist", pp);
                            if (rgDeliveryType.getCheckedRadioButtonId() == R.id.rbExpress) {
                                dcAmountTv.setText(expcharge + "");
                                grandTotalTv.setText("₹ " + (totalPaybleAount + expcharge + Integer.parseInt(minimumOrderCharge)));
                            } else {
                                dcAmountTv.setText(delivery + "");
                                grandTotalTv.setText("₹ " + (totalPaybleAount + delivery + Integer.parseInt(minimumOrderCharge)));
                            }
                        }
                    }
                } else {
                    Toast.makeText(CartAct.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<List<AreaModel>> call, Throwable t) {
                System.out.println("REGI : 1*error*1 : " + t.getMessage());
            }
        });
    }

    private void initSpinnerType() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AreaApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AreaApi api = retrofit.create(AreaApi.class);
        Call<List<AreaModel>> call = api.getAreaList1();
        call.enqueue(new Callback<List<AreaModel>>() {
            @Override
            public void onResponse(Call<List<AreaModel>> call, Response<List<AreaModel>> response) {
                CategoryList = response.body();

                if (CategoryList != null && CategoryList.size() > 0) {
                    String[] Categorys = new String[CategoryList.size()];

                    for (int i = 0; i < CategoryList.size(); i++) {
                        Categorys[i] = CategoryList.get(i).getArea();
                        String keyNameStr = (CategoryList.get(i).getArea());
                        spinnerArrayAdapter = new ArrayAdapter<>(CartAct.this, R.layout.spinner_item, Categorys);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        spinner.setAdapter(spinnerArrayAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<AreaModel>> call, Throwable t) {

            }
        });
    }

    @Override
    public void bindPixel() {
        //mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        //mSizeUtil.setLinearLayoutMatchWidth(buyLayout, 350, 0, 0, 0, 0, 0);
        //  mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
//        mSizeUtil.setLinearLayout(buyNowLayout, 520, 110, 10, 10, 10, 10, Gravity.CENTER);
//        mSizeUtil.setLinearLayout(couponLayout, 520, 110, 10, 10, 10, 10, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buyNowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPrefHelper.get_STRING(Constant.USERNAME, "").equals("")) {
                    Intent ip = new Intent(mContext, LoginAct.class);
                    startActivity(ip);
                } else {
                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
                            +
                            delivery)
                            + "");
                    Intent ip = new Intent(mContext, CheckoutAct.class);
                    ip.putExtra("area", spinnerArrayAdapter.getItem(spinner.getSelectedItemPosition()));
                    Integer selectedAreaId = Integer.valueOf(CategoryList.get(spinner.getSelectedItemPosition()).getId());
                    ip.putExtra("areaId", selectedAreaId);
                    startActivity(ip);
                }
            }
        });

//        couponLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent ip = new Intent(mContext, CouponAct.class);
//                ip.putExtra("MinPrice", totalPaybleAount);
//                ip.putExtra("isDrawer", false);
//                startActivity(ip);
//            }
//        });
    }

    int totalPaybleAount = 00;
    int price = 0;

    //    @Override
//    public void bindMethod() {
//        setUpStatusBar();
//        cartListView.setLayoutManager(new GridLayoutManager(mContext, 1));
////        try {
//        tvMrp = getIntent().getStringExtra("key");
//        System.out.println("tvprice " + tvMrp);
//
//        for (int i = 0; i < mDbCart.getAllBookmark().size(); i++) {
//            if (!(mDbCart.getAllBookmark().get(i).getPublix_price1().isEmpty())) {
//                totalPaybleAount = totalPaybleAount + Integer.parseInt((tvMrp));
//            }
//        }
//        mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
//                + new DbDeliveryCharges(mContext).getDeliveryCharges()) + "");
//        totalAmountTv.setText("₹ " + totalPaybleAount);
//
//        dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());
//
//        grandTotalTv.setText("₹ " + (totalPaybleAount));
//
//        if (mDbCart.getAllBookmark().size() == 0) {
//            emptyTv.setVisibility(View.VISIBLE);
//            buyLayout.setVisibility(View.GONE);
////            couponLayout.setVisibility(View.GONE);
//            mainLayoutBottom.setVisibility(View.GONE);
//        } else {
//            emptyTv.setVisibility(View.GONE);
//            buyLayout.setVisibility(View.VISIBLE);
////            couponLayout.setVisibility(View.VISIBLE);
//            mainLayoutBottom.setVisibility(View.VISIBLE);
//            System.out.println("CART : " + mDbCart.getAllBookmark());
//            cartListView.setAdapter(new CartListAdapter(mContext, mDbCart.getAllBookmark(), new com.technoarts.ashokasta.callback.onCartClick() {
//                @Override
//                public void click(int position) {
//
//                }
//
//                @Override
//                public void onClickPlus(int position) {
////
//                    totalPaybleAount = totalPaybleAount + Integer.parseInt(tvMrp);
////
//                    totalAmountTv.setText("₹ " + (totalPaybleAount));
//
//                    dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());
//
//                    grandTotalTv.setText("₹ " + (totalPaybleAount
//                            + new DbDeliveryCharges(mContext).getDeliveryCharges()));
//                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
//                            + new DbDeliveryCharges(mContext).getDeliveryCharges()) + "");
//                }
//
//                @Override
//                public void onClickMinus(final int position) {
////                    int amt = Integer.parseInt(mDbCart.getAllBookmark().get(position).getPublix_price1());
//                    System.out.println("CART PRICE: coupon " + couponPrice);
////
//                    totalPaybleAount = totalPaybleAount - Integer.parseInt(tvMrp);
////
//                    totalAmountTv.setText("₹ " + (totalPaybleAount));
//
//                    dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());
//
//                    grandTotalTv.setText("₹ " + (totalPaybleAount
//                            + new DbDeliveryCharges(mContext).getDeliveryCharges()));
//                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
//                            + new DbDeliveryCharges(mContext).getDeliveryCharges()) + "");
//
//                }
//
//                @Override
//                public void onClickDelete(int position) {
//                    onResume();
////
//                    totalPaybleAount = totalPaybleAount - Integer.parseInt(tvMrp);
////
//                    totalAmountTv.setText("₹ " + (totalPaybleAount));
//
//                    dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());
//
//                    grandTotalTv.setText("₹ " + (totalPaybleAount
//                            + new DbDeliveryCharges(mContext).getDeliveryCharges()));
//                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
//                            + new DbDeliveryCharges(mContext).getDeliveryCharges()) + "");
//                }
//
//                @Override
//                public void onCouponDelete(int position) {
//                    totalPaybleAount = tempCoupon + totalPaybleAount;
//                    tempCoupon = 0;
//                }
//            }));
//        }
//    }
    @Override
    public void bindMethod() {
        setUpStatusBar();

        cartListView.setLayoutManager(new GridLayoutManager(mContext, 1));
        tvMrp = (((getIntent().getStringExtra("key"))));
        isFlagVariant = (((getIntent().getIntExtra("isFlagVariant", 1))));
        cartListAdapter = new CartListAdapter(CartAct.this, isFlagVariant);
        // totalPaybleAount=  cartListAdapter.getTotalAmount();
        //        if (tvMrp != null) {
//        pricet = Integer.parseInt(tvMrp);
//        }
        for (int i = 0; i < mDbCart.getAllBookmark().size(); i++) {
            if (isFlagVariant == 1) {
                if (!mDbCart.getAllBookmark().get(i).getPublix_price1().equals(""))
                    totalPaybleAount += Integer.parseInt(mDbCart.getAllBookmark().get(i).getPublix_price1());
            } else if (isFlagVariant == 2) {
                if (!mDbCart.getAllBookmark().get(i).getPublix_price2().equals(""))
                    totalPaybleAount += Integer.parseInt(mDbCart.getAllBookmark().get(i).getPublix_price2());
            } else if (isFlagVariant == 3) {
                if (!mDbCart.getAllBookmark().get(i).getPublix_price3().equals(""))
                    totalPaybleAount += Integer.parseInt(mDbCart.getAllBookmark().get(i).getPublix_price3());
            }
        }


        mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
                + delivery) + "");
        totalAmountTv.setText("₹ " + totalPaybleAount);


//        dcAmountTv.setText(pp);

        grandTotalTv.setText("₹ " + (totalPaybleAount
                + delivery));

        if (mDbCart.getAllBookmark().size() == 0) {
            emptyTv.setVisibility(View.VISIBLE);
            buyLayout.setVisibility(View.GONE);
//            couponLayout.setVisibility(View.GONE);
            mainLayoutBottom.setVisibility(View.GONE);
        } else {
            emptyTv.setVisibility(View.GONE);
            buyLayout.setVisibility(View.VISIBLE);
//            couponLayout.setVisibility(View.VISIBLE);
            mainLayoutBottom.setVisibility(View.VISIBLE);
            System.out.println("CART : " + mDbCart.getAllBookmark());
            cartListView.setAdapter(new CartListAdapter(mContext, mDbCart.getAllBookmark(), isFlagVariant, new com.technoarts.publix.callback.onCartClick() {
                @Override
                public void click(int position) {

                }

                @Override
                public void onClickPlus(int position) {

                    totalPaybleAount = totalPaybleAount + Integer.parseInt(mDbCart.getAllBookmark().get(position).getPublix_price1());
                    totalAmountTv.setText("₹ " + (totalPaybleAount));

//                    dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());
//                    dcAmountTv.setText(pp);
                    grandTotalTv.setText("₹ " + (totalPaybleAount
                            + delivery));
                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
                            + delivery) + "");
                    updateMinimumOrderCharge();

                }

                @Override
                public void onClickMinus(final int position) {
//                    int amt = Integer.parseInt(mDbCart.getAllBookmark().get(position).getPro_amount());
//                    System.out.println("CART PRICE: coupon " + couponPrice);

                    totalPaybleAount = totalPaybleAount - Integer.parseInt(mDbCart.getAllBookmark().get(position).getPublix_price1());
                    ;
                    totalAmountTv.setText("₹ " + (totalPaybleAount));

//                    dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());
//                    dcAmountTv.setText(pp);
                    grandTotalTv.setText("₹ " + (totalPaybleAount
                            + delivery));
                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
                            + delivery) + "");
                    updateMinimumOrderCharge();


                }

                @Override
                public void onClickDelete(int position) {

                    totalPaybleAount = totalPaybleAount - Integer.parseInt(mDbCart.getAllBookmark().get(position).getPublix_price1());
                    ;
                    ;
                    totalAmountTv.setText("₹ " + (totalPaybleAount));

//                    dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());
//                    dcAmountTv.setText(pp);
                    grandTotalTv.setText("₹ " + (totalPaybleAount
                            + delivery));
                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
                            + delivery) + "");
                    updateMinimumOrderCharge();
                }

                @Override
                public void onCouponDelete(int position) {
                    totalPaybleAount = tempCoupon + totalPaybleAount;
                    tempCoupon = 0;
                }

                @Override
                public void onName1(int position) {
                    totalPaybleAount = 0;
//                    for (int i = 0; i < mDbCart.getAllBookmark().size(); i++) {
//                        totalPaybleAount = (totalPaybleAount + Integer.parseInt(mDbCart.getAllBookmark().get(i).getPublix_price1()));
//                    }
//                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
//                            + delivery) + "");
//                    totalAmountTv.setText("₹ " + totalPaybleAount);
////                    dcAmountTv.setText(pp);
////                    dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());
//                    grandTotalTv.setText("₹ " + (totalPaybleAount
//                            + delivery));
                    for (int i = 0; i < mDbCart.getAllBookmark().size(); i++) {

                        totalPaybleAount += Integer.parseInt(mDbCart.getAllBookmark().get(i).getPublix_price1());


                    }


                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
                            + delivery) + "");
                    totalAmountTv.setText("₹ " + totalPaybleAount);


//        dcAmountTv.setText(pp);

                    grandTotalTv.setText("₹ " + (totalPaybleAount
                            + delivery));
                }

                @Override
                public void onName2(int position) {
                    totalPaybleAount = 0;
//                    for (int i = 0; i < mDbCart.getAllBookmark().size(); i++) {
//
//                        totalPaybleAount = totalPaybleAount + Integer.parseInt(mDbCart.getAllBookmark().get(i).getPublix_price2());
//                    }
//                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
//                            + delivery) + "");
//                    totalAmountTv.setText("₹ " + totalPaybleAount);
//
//
////                    dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());
////                    dcAmountTv.setText(pp);
//                    grandTotalTv.setText("₹ " + (totalPaybleAount
//                            + delivery));
                    for (int i = 0; i < mDbCart.getAllBookmark().size(); i++) {


                        totalPaybleAount += Integer.parseInt(mDbCart.getAllBookmark().get(i).getPublix_price2());


                    }


                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
                            + delivery) + "");
                    totalAmountTv.setText("₹ " + totalPaybleAount);


//        dcAmountTv.setText(pp);

                    grandTotalTv.setText("₹ " + (totalPaybleAount
                            + delivery));
                }

                @Override
                public void onName3(int position) {
                    totalPaybleAount = 0;
//
//                    for (int i = 0; i < mDbCart.getAllBookmark().size(); i++) {
//                        totalPaybleAount = (totalPaybleAount + Integer.parseInt(mDbCart.getAllBookmark().get(i).getPublix_price3()));
//                    }
//                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
//                            + delivery) + "");
//                    totalAmountTv.setText("₹ " + totalPaybleAount);
////                    dcAmountTv.setText(pp);
//
////                    dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());
//
//                    grandTotalTv.setText("₹ " + (totalPaybleAount
//                            + delivery));
                    for (int i = 0; i < mDbCart.getAllBookmark().size(); i++) {
                        totalPaybleAount += Integer.parseInt(mDbCart.getAllBookmark().get(i).getPublix_price3());

                    }


                    mPrefHelper.save_STRING(Constant.getgrandtotal, (totalPaybleAount
                            + delivery) + "");
                    totalAmountTv.setText("₹ " + totalPaybleAount);


//        dcAmountTv.setText(pp);

                    grandTotalTv.setText("₹ " + (totalPaybleAount
                            + delivery));
                }
            }));
        }

        updateMinimumOrderCharge();


    }

    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        couponPrice = Integer.parseInt(mPrefHelper.get_STRING(Constant.getcoupon, "0"));
        tempCoupon = couponPrice;
        if (couponPrice > 0) {
            int temp = totalPaybleAount;
            totalPaybleAount = Integer.parseInt(mPrefHelper.get_STRING(Constant.getgrandtotal, "0"));
            totalAmountTv.setText(temp + " - (" + couponPrice + " discount on coupon)" + (totalPaybleAount - couponPrice));

            grandTotalTv.setText("₹ " + (totalPaybleAount
                    + delivery));

//            dcAmountTv.setText("" + new DbDeliveryCharges(mContext).getDeliveryCharges());

            totalPaybleAount = totalPaybleAount - couponPrice;
            couponPrice = 0;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mPrefHelper.save_STRING(Constant.getcoupon, "0");
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
        postCode(String.valueOf(parent.getItemAtPosition(i)));

//       postCode(areaModels.get(i).getRate());
//        int pos=parent.getSelectedItemPosition();
//            dcAmountTv.setText(areaModels.get(pos).getRate());
//        }
//        String area=areaModels.get(i).getArea();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void updateMinimumOrderCharge() {
        Map<String, String> param = new HashMap<>();
        param.put("amt", totalPaybleAount + "");

        api.getMiniOrderAmount(param).enqueue(new Callback<ArrayList<MiniOrderItem>>() {
            @Override
            public void onResponse(Call<ArrayList<MiniOrderItem>> call, Response<ArrayList<MiniOrderItem>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String amount = response.body().get(0).getCharge();
                        if (amount == null) amount = "0";
                        minimumOrderCharge = amount;

                        tvMinimumOrderCharge.setText(getString(R.string.ruppes) + amount);
                    } else {
                        tvMinimumOrderCharge.setText("");
                    }
                } else {
                    tvMinimumOrderCharge.setText("");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MiniOrderItem>> call, Throwable t) {
                tvMinimumOrderCharge.setText("");
            }
        });
    }
}
