package com.technoarts.publix.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.adapters.MenuListAdapter;
import com.technoarts.publix.callback.onClickMenuItem;
import com.technoarts.publix.dbhelper.DbCart;
import com.technoarts.publix.fragments.FragCategory;
import com.technoarts.publix.fragments.FragContact;
import com.technoarts.publix.fragments.FragDeal;
import com.technoarts.publix.fragments.FragFav;
import com.technoarts.publix.fragments.FragHome;
import com.technoarts.publix.fragments.FragMyAccount;
import com.technoarts.publix.fragments.FragProduct;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.DrawerPojo;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

public class MainAct extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ArrayList<DrawerPojo> menuData;
    DrawerLayout drawerLayout;
    RecyclerView navMenuListView;
    LinearLayout navigationProfileLayout, titleBarLayout, bottombarLayout,
            bottomProductLayout, bottomDealLayout, bottomCartLayout, homeLayout;
    ImageView drawerBtn, bottomProduct, bottomDeal, bottomCart, cartBtn, home;
    TextView titleBarTv, cartSizeTv;
    DbCart mDbCart;
    MenuListAdapter menuListAdapter;
    PrefHelper mPrefHelper;
    TextView userNameTv;
    ImageView likeFb, likeInsta, logoImage;

    static GoogleSignInClient mGoogleSignInClient;
    static GoogleSignInAccount account;
    public static final int MULTIPLE_PERMISSIONS = 10;
    String[] permissions = new String[]{
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
        menuData = new ArrayList<>();
        mDbCart = new DbCart(mContext);
        mPrefHelper = new PrefHelper(mContext);
    }

    @Override
    public void bindViews() {
        drawerLayout = findViewById(R.id.drawerLayout);
        navMenuListView = findViewById(R.id.navMenuListView);

        navigationProfileLayout = findViewById(R.id.navigationProfileLayout);
        titleBarLayout = findViewById(R.id.titleBarLayout);
        bottombarLayout = findViewById(R.id.bottombarLayout);

        drawerBtn = findViewById(R.id.drawerBtn);
        titleBarTv = findViewById(R.id.titleBarTv);
        cartSizeTv = findViewById(R.id.cartSizeTv);

        bottomProduct = findViewById(R.id.bottomProduct);
        bottomDeal = findViewById(R.id.bottomDeal);
        bottomCart = findViewById(R.id.bottomCart);

        cartBtn = findViewById(R.id.cartBtn);
        bottomProductLayout = findViewById(R.id.bottomProductLayout);
        homeLayout = findViewById(R.id.homeLayout);

        bottomDealLayout = findViewById(R.id.bottomDealLayout);
        bottomCartLayout = findViewById(R.id.bottomCartLayout);
        userNameTv = findViewById(R.id.userNameTv);

        likeFb = findViewById(R.id.likeFb);
        likeInsta = findViewById(R.id.likeInsta);
        logoImage = findViewById(R.id.logoImage);
        home = findViewById(R.id.home);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, MainAct.class));
            }
        });
//        if(account == null) {
//            startActivity(new Intent(this, LoginAct.class));
//        }
// else {

//            System.out.println("GOOGLE : " + account.getDisplayName());
//            System.out.println("GOOGLE : " + account.getEmail());
//            System.out.println("GOOGLE : " + account.getFamilyName());
////            mPrefHelper.save_STRING(Constant.checkout_user_id, account.getEmail());
//            mPrefHelper.save_STRING(Constant.EMAIL, account.getEmail());
//            mPrefHelper.save_STRING(Constant.USERNAME, account.getDisplayName());
//            mPrefHelper.save_BOOLEAN(Constant.isSocial, true);
////            new LoginAct.bgRegistration(account.getDisplayName(), account.getEmail(), account.getEmail(), "").execute();
////            TextView name = findViewById(R.id.name);
////            name.setText(account.getDisplayName());
//        }
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(navigationProfileLayout, 512, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayoutMatchWidth(bottombarLayout, 160, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(drawerBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(bottomProduct, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(bottomDeal, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(bottomCart, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(home, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(cartBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(cartSizeTv, 70, 70, 0, 10, 20, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(likeFb, 70, 70, 0, 10, 10, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(likeInsta, 70, 70, 0, 10, 10, 0, Gravity.CENTER);
        mSizeUtil.setRelative(logoImage, 400, 400, 0, 10, 10, 0, RelativeLayout.CENTER_IN_PARENT);
    }

    @Override
    public void bindListener() {
        drawerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        bottomProductLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent ip = new Intent(mContext, CategoryAct.class);
//                startActivity(ip);
            }
        });

        bottomDealLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ip = new Intent(mContext, BottomItemAct.class);
                ip.putExtra("ItemName", "Deal/Offer");
                startActivity(ip);
            }
        });

        bottomCartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ip = new Intent(mContext, CartAct.class);
                startActivity(ip);
            }
        });

        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ip = new Intent(mContext, CartAct.class);
                startActivity(ip);
            }
        });

        bottomDealLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contentContainer, new FragFav());
                transaction.addToBackStack(null);
                transaction.commit();


            }
        });

        bottomProductLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contentContainer, new FragProduct());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        likeFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.LEFT);
                String url = "https://www.facebook.com/Publix-Grocery-111038267411714";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        likeInsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.LEFT);
                String url = "https://www.instagram.com/publixgroceryjmu/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }

    @Override
    public void bindMethod() {


        menuData.add(new DrawerPojo("Home", R.drawable.home, true, new FragCategory()));
        menuData.add(new DrawerPojo("Products", R.drawable.product, false, new FragProduct()));
        menuData.add(new DrawerPojo("Deal/Offer", R.drawable.offer, false, new FragDeal()));
        menuData.add(new DrawerPojo("Cart", R.drawable.empty_cart, false, new FragHome()));
//        menuData.add(new DrawerPojo("Coupons", R.drawable.couponimg, false, new FragHome()));
        menuData.add(new DrawerPojo("My Account", R.drawable.myaccountr, false, new FragMyAccount()));

        menuData.add(new DrawerPojo("Share", R.drawable.share, false, null));
        menuData.add(new DrawerPojo("Notifications", R.drawable.noti, false, new FragMyAccount()));
        menuData.add(new DrawerPojo("Contact us", R.drawable.contact, false, new FragContact(null)));
        menuData.add(new DrawerPojo("Login", R.drawable.contact, false, new FragMyAccount()));
//        menuData.add(new DrawerPojo("Login", R.drawable.logout, false, null));

        navMenuListView.setLayoutManager(new GridLayoutManager(mContext, 1));

        loadThis(0, menuData.get(0).getmFragment());

        menuListAdapter = new MenuListAdapter(mContext, menuData, new onClickMenuItem() {
            @Override
            public void onClick(int position) {
                loadThis(position, menuData.get(position).getmFragment());
            }
        });

        navMenuListView.setAdapter(menuListAdapter);

        createHash();

        System.out.println("USER ID : " + mPrefHelper.get_STRING(Constant.checkout_user_id, ""));
    }


    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissionsList,
                                           int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                }

                return;
            }
        }
    }

    void createHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                System.out.println("" +
                        "." +
                        " " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            System.out.println("FB HASH : " + e.getMessage());
        }
    }

    public static void shareApp(Context mContext) {

        // change message as per app

        String link = "https://play.google.com/store/apps/details?id=" + mContext.getPackageName();
        String text = "Save everyday with Publix, download app now " +link;


        Intent i = new Intent("android.intent.action.SEND");
        i.putExtra("android.intent.extra.TEXT", text + link);
        i.setType("text/plain");
        mContext.startActivity(Intent.createChooser(i, "Share Via"));
    }

    void loadThis(int postion, Fragment newFragment) {
        drawerLayout.closeDrawer(Gravity.LEFT);
        if (postion == 4) {
            if (mPrefHelper.get_STRING(Constant.EMAIL, "").equals("") || mPrefHelper.get_STRING(Constant.USERNAME, "").equals("")) {
                startActivity(new Intent(mContext, LoginAct.class));
            } else {
                titleBarTv.setText(menuData.get(postion).getName());
                final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contentContainer, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        }
        else if (postion==8){
            if (mPrefHelper.get_STRING(Constant.EMAIL, "").equals("") || mPrefHelper.get_STRING(Constant.USERNAME, "").equals("")) {
                startActivity(new Intent(mContext, LoginAct.class));
            } else {
                mPrefHelper.clearPref();
                mPrefHelper.removeAll(Constant.checkout_user_id);
                // onResume();
                titleBarTv.setText("");
                try {
                    mGoogleSignInClient.signOut();
                    LoginManager.getInstance().logOut();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new DbCart(getApplicationContext()).deleteAll();

                onResume();
                final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contentContainer, new FragCategory());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        }
        else if (postion == 5) {
            shareApp(mContext);
        } else if (postion == 3) {
            startActivity(new Intent(mContext, CartAct.class));
        }
//        else if (postion == 4) {
//            Intent ip = new Intent(mContext, CouponAct.class);
//            ip.putExtra("isDrawer", true);
//            startActivity(ip);
//        }
        else if (postion == 6) {
            startActivity(new Intent(mContext, NotificationAct.class));
        } /*else if (postion == 7) {
            if (mPrefHelper.get_STRING(Constant.USERNAME, "").equals("")) {
                startActivity(new Intent(mContext, LoginAct.class));
            } else {
                Constant.logOut(mContext);
                mDbCart.deleteAll();
                menuData.get(7).setName("Login");
                try {
                    signOut();
                } catch (Exception e) {

                }
                finish();
                startActivity(new Intent(mContext, MainAct.class));
            }
        }*/ else {
            if (postion == 0) {
                titleBarTv.setText("Publix");
            } else {
                titleBarTv.setText(menuData.get(postion).getName());
            }
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.contentContainer, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    private void signOut() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(mContext, gso);
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });

        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkCart();
        if (mPrefHelper.get_STRING(Constant.USERNAME, "").equals("")) {
            menuData.get(8).setName("Login");
            menuData.get(8).setResourceIcon(R.drawable.logout);
            menuListAdapter.notifyDataSetChanged();
            userNameTv.setText("");
            try {
                LoginManager.getInstance().logOut();
            } catch (Exception e) {

            }
        } else {
            menuData.get(8).setName("Logout");
            menuData.get(8).setResourceIcon(R.drawable.logout);
            menuListAdapter.notifyDataSetChanged();
            userNameTv.setText(mPrefHelper.get_STRING(Constant.USERNAME, ""));
        }
    }

    void checkCart() {
        if (mDbCart.getNumberOfItems() == 0) {
            cartSizeTv.setVisibility(View.GONE);
            mSizeUtil.setLinearLayout(cartBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        } else {
            cartSizeTv.setVisibility(View.VISIBLE);
            cartSizeTv.setText(mDbCart.getNumberOfItems() + "");
            mSizeUtil.setLinearLayout(cartBtn, 70, 70, 0, 0, 0, 0, Gravity.CENTER);
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exit")
                .setMessage("Are you sure you want to Exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
//        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
//            drawerLayout.closeDrawer(Gravity.LEFT);
//        } else {
//            final Fragment fragmentInFrame = getSupportFragmentManager().findFragmentById(R.id.contentContainer);
//            if (fragmentInFrame instanceof FragCategory) {
//                finishAffinity();
//            } else {
//                titleBarTv.setText(menuData.get(0).getName());
//                loadThis(0, menuData.get(0).getmFragment());
//                menuListAdapter.setSelected(0);
//            }
//        }
    }

}
