package com.technoarts.publix.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.technoarts.publix.R;
import com.technoarts.publix.callback.loginApi;
import com.technoarts.publix.helpers.BasicHelper;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.helpers.Utils;
import com.technoarts.publix.pojo.ResponseModel;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegActivity extends AppCompatActivity {
    private static EditText full_name, user_name, emailId, mobileNumber, location,
            password_, confirmPassword;
    LinearLayout titleBarLayout;
    ImageView backBtn;
    private static Button signUpButton;
    SizeUtil mSizeUtil;
    CheckBox terms_conditions;
    Context mContext;
    loginApi api;
    List<ResponseModel> responseModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        full_name = findViewById(R.id.fullName);
        user_name = findViewById(R.id.user_name);
        emailId = findViewById(R.id.userEmailId);
        mobileNumber = findViewById(R.id.mobileNumber);
        password_ = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirmPassword);
        signUpButton = findViewById(R.id.signUpBtn);
        terms_conditions = findViewById(R.id.terms_conditions);
        mSizeUtil = new SizeUtil(mContext);
//        mPrefHelper = new PrefHelper(mContext)
//        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
//        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation(v);
            }
        });

        terms_conditions.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    terms();
                }
            }
        });
    }

    private void terms() {
        final ProgressDialog pdialog = new ProgressDialog(mContext);
        pdialog.setMessage("Loading...");
        pdialog.setCancelable(false);
        pdialog.setInverseBackgroundForced(false);
        pdialog.show();

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.terms_conditions);

        TextView ok = (TextView) dialog.findViewById(R.id.txt_ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                pdialog.dismiss();
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialog.dismiss();
                pdialog.dismiss();
            }
        });

        dialog.show();
    }

    private void checkValidation(View view) {

        String getFullName = full_name.getText().toString();
        String getEmailId = emailId.getText().toString();
        String getMobileNumber = mobileNumber.getText().toString();
        String getPassword = password_.getText().toString();
        String getConfirmPassword = confirmPassword.getText().toString();

        Pattern p = Pattern.compile(Utils.regEx);
        Matcher m = p.matcher(getEmailId);

        if (getFullName.equals("") || getFullName.length() == 0
                || getEmailId.equals("") || getEmailId.length() == 0
                || getPassword.equals("") || getPassword.length() == 0
                || getConfirmPassword.equals("")
                || getConfirmPassword.length() == 0)

            new BasicHelper().Show_Toast(mContext, view,
                    "All fields are required.");

        else if (!m.find())
            new BasicHelper().Show_Toast(mContext, view,
                    "Your Email Id is Invalid.");

        else if (!getConfirmPassword.equals(getPassword))
            new BasicHelper().Show_Toast(mContext, view,
                    "Both password doesn't match.");

//        else if (!terms_conditions.isChecked())
//            new BasicHelper().Show_Toast(mContext, view,
//                    "Please select Terms and Conditions.");
        else {
//            register();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            loginApi api=retrofit.create(loginApi.class);
            Call<ResponseModel> userCall = api.doRegistration("Android", getFullName, getEmailId, getPassword);
            userCall.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    if (response.body().getResult().equals("300")){
                        Log.e("response", response.body().getResult());
                        full_name.setText("");
                        emailId.setText("");
                        password_.setText("");
                        Toast.makeText(RegActivity.this, "Success", Toast.LENGTH_SHORT).show();
//                        MainActivity.appPreference.showToast("Registered Successfully");
                    } else if (response.body().getResult().equals("200")){
                        Toast.makeText(RegActivity.this, "Exixts", Toast.LENGTH_SHORT).show();
//                        MainActivity.appPreference.showToast("This email already exists");
                    } else if (response.body().getResult().equals("")){
                        Toast.makeText(RegActivity.this, "Error", Toast.LENGTH_SHORT).show();
//                        MainActivity.appPreference.showToast("Oops! something went wrong.");
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                }
            });
        }
    }

}
