package com.technoarts.publix.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.helpers.SizeUtil;

public class BottomItemAct extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout;
    String selectedItem;
    TextView titleBarTv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return (R.layout.activity_bottom_item);
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        titleBarTv = findViewById(R.id.titleBarTv);
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout,160,0,0,0,0,0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void bindMethod() {
        Bundle mBundle = getIntent().getExtras();
        if (mBundle != null) {
            selectedItem = mBundle.getString("ItemName");
            titleBarTv.setText(selectedItem);
        }
    }
}
