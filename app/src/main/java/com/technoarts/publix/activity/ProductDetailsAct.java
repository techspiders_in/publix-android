package com.technoarts.publix.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.adapters.MyCustomPagerAdapter;
import com.technoarts.publix.adapters.Slideradapter;
import com.technoarts.publix.callback.RelatedApi;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.dbhelper.DbCart;
import com.technoarts.publix.dbhelper.DbDeliveryCharges;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.paginationAdapter.ProductWithPagination;
import com.technoarts.publix.pojo.GenericResponseModel;
import com.technoarts.publix.pojo.PojoDaily;
import com.technoarts.publix.pojo.ProductPojo;
import com.technoarts.publix.pojo.Slidermodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsAct extends BaseAct implements BaseSliderView.OnSliderClickListener {
    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn, likeIcon, shareIcon;
    LinearLayout titleBarLayout, addCartLayout, buyNowLayout;
    RelativeLayout imageLayout;
    PojoDaily SelectedProduct1;
    ProductPojo SelectedProduct;
    TextView productNameTv, productPriceTv, descriptionTv, addCartTv,
            originalAmount, tvname2, tvname3, tvdec, pricetvr, originalr, titleBarTv;
    ViewPager itemImagePager;
    LinearLayout descriptionCard, name2, name3;
    Integer isFlagVariant = 1;
    RecyclerView relatedListView;
    LinearLayout panelRelatedProduct;
    ArrayList<ProductPojo> productList;
    DbCart mDbCart;
    ScrollView fullScrollView;
    ProgressBar loadingBar;
    LinearLayout callButton;
    ImageView callIcon, cartDirect;
    List<String> imageList;
    LinearLayout wishLayout;
    LinearLayout llShare;
    LinearLayout lbAddFav;
    String imgPath = "";
    ImageView wishShare, wishRel;
    CheckBox wishFav;
    String price;
    private ArrayList<ProductPojo> ImagesArray = new ArrayList<>();
    SliderLayout sliderLayout;
    private ArrayList<Slidermodel> catList1;
    HashMap<String, String> Hash_file_maps;
    private Slideradapter catadapter1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_product_details;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
        productList = new ArrayList<>();
        mDbCart = new DbCart(mContext);
        api = ServiceGenerator.createService(RelatedApi.class);
        relatedproductList = new ArrayList<>();
        duplicate = new ArrayList<>();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        sliderLayout = findViewById(R.id.sliderLayout);

        backBtn = findViewById(R.id.backBtn);
        imageLayout = findViewById(R.id.imageLayout);
        itemImagePager = findViewById(R.id.itemImagePager);
        descriptionCard = findViewById(R.id.descriptionCard);
        likeIcon = findViewById(R.id.likeIcon);
        shareIcon = findViewById(R.id.shareIcon);
        addCartLayout = findViewById(R.id.addCartLayout);
        buyNowLayout = findViewById(R.id.buyNowLayout);
        relatedListView = findViewById(R.id.relatedListView);
        callButton = findViewById(R.id.callButton);
        addCartTv = findViewById(R.id.addCartTv);
        loadingBar = findViewById(R.id.loadingBar);
        fullScrollView = findViewById(R.id.fullScrollView);
        callIcon = findViewById(R.id.callIcon);
        cartDirect = findViewById(R.id.cartDirect);
        originalAmount = findViewById(R.id.originalAmount);
        wishLayout = findViewById(R.id.wishLayout);
        llShare = findViewById(R.id.llShare);
        lbAddFav = findViewById(R.id.lbAddFav);
        wishShare = findViewById(R.id.wishShare);
        wishFav = findViewById(R.id.wishFav);
        wishRel = findViewById(R.id.wishRel);
        titleBarTv = findViewById(R.id.titleBarTv);

        descriptionTv = findViewById(R.id.descriptionTv);
        productNameTv = findViewById(R.id.productNameTv);
        productPriceTv = findViewById(R.id.productPriceTv);

        name2 = findViewById(R.id.name2);
        name3 = findViewById(R.id.name3);
        tvname2 = findViewById(R.id.tvname2);
        tvname3 = findViewById(R.id.tvname3);
        tvdec = findViewById(R.id.tvdec);
        pricetvr = findViewById(R.id.pricetvr);
        originalr = findViewById(R.id.originalr);
        panelRelatedProduct = findViewById(R.id.panelRelatedProduct);

    }


    @Override
    public void bindPixel() {
      /*  mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setRelativeMatchWidth(relatedListView, 360, 0, 0, 0, 0, 0);
        //mSizeUtil.setLinearLayoutMatchWidth(wishLayout, 100, 0, 20, 20, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);

        mSizeUtil.setLinearLayout(wishShare, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(wishFav, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(wishRel, 70, 70, 0, 30, 30, 0, Gravity.CENTER);

        mSizeUtil.setLinearLayout(cartDirect, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(likeIcon, 60, 60, 0, 10, 10, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(shareIcon, 60, 60, 0, 10, 10, 0, Gravity.CENTER);
//        mSizeUtil.setLinearLayout(imageLayout, 950, 670, 10, 0, 0, 0, Gravity.CENTER);
        //mSizeUtil.setLinearLayout(addCartLayout, 500, 190, 20, 20, 0, 20, Gravity.CENTER);
        //mSizeUtil.setLinearLayout(buyNowLayout, 500, 190, 20, 20, 20, 20, Gravity.CENTER);
        mSizeUtil.setLinearLayout(callButton, 400, 100, 20, 20, 20, 20, Gravity.CENTER);
        mSizeUtil.setLinearLayout(callIcon, 70, 70, 0, 20, 20, 0, Gravity.CENTER);*/
//        mSizeUtil.setLinearLayoutWrapHeight(descriptionCard, 250, 0, 0, 0, 0,);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addCartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDbCart.isBookmark(SelectedProduct.getPro_id())) {
                    addCartTv.setText("Add to cart");
                    mDbCart.deleteBookmark(SelectedProduct.getPro_id());
                    new DbDeliveryCharges(mContext).deleteCharges(SelectedProduct.getPro_id());
                } else {
                    addCartTv.setText("Remove from cart");
                    System.out.println("CART ITEM SIZE : " +
                            mDbCart.addToCart(
                                    SelectedProduct.getPro_id(),
                                    SelectedProduct.getPro_name(),
                                    imgPath,
                                    SelectedProduct.getPro_mrp_ammount(),
                                    SelectedProduct.getPro_desc(),
                                    SelectedProduct.getPro_mrp_ammount(),
                                    SelectedProduct.getPro_status(),
                                    SelectedProduct.getDate(),
                                    SelectedProduct.getPro_sale(),
                                    SelectedProduct.getPro_cat_id(),
                                    "1",
                                    "",
                                    SelectedProduct.getName_price(),
                                    SelectedProduct.getPublix_price(),
                                    SelectedProduct.getPublix_price1(),
                                    SelectedProduct.getPublix_price2(),
                                    SelectedProduct.getPublix_price3(),
                                    SelectedProduct.getName1(),
                                    SelectedProduct.getName2(),
                                    SelectedProduct.getName3(),
                                    SelectedProduct.getPrice1(),
                                    SelectedProduct.getPrice2(),
                                    SelectedProduct.getPrice3()
                            ));
                    if (!new DbDeliveryCharges(mContext).isBookmark(SelectedProduct.getPro_id())) {
                        new DbDeliveryCharges(mContext).addCharge(SelectedProduct.getPro_id(),
                                "SelectedProduct.getDeliveryCharges()");
                    }
//                    }
                }
                Intent myIntent = new Intent(ProductDetailsAct.this, CartAct.class);
                price = SelectedProduct.getPublix_price1();
                myIntent.putExtra("key", price);
                myIntent.putExtra("isFlagVariant", isFlagVariant);
                Log.e("tvprice", price);
                startActivity(myIntent);
            }

        });

        callButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                String phn = "9797683909";
                try {
                    Uri call = Uri.parse("tel:" + phn);
                    Intent surf = new Intent(Intent.ACTION_CALL, call);
                    startActivity(surf);
                } catch (Exception e) {

                }
            }
        });

        cartDirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ProductDetailsAct.this, CartAct.class);
                price = SelectedProduct.getPublix_price1();
                myIntent.putExtra("key", price);

                startActivity(myIntent);
            }
        });

        buyNowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCartLayout.performClick();
            }
                /*if (new PrefHelper(mContext).get_STRING(Constant.checkout_user_id, "").equals("")) {
                    Intent ip = new Intent(mContext, LoginAct.class);
                    startActivity(ip);
                } else {
                    mDbCart.addToCart(

                            SelectedProduct.getPro_id(),
                            SelectedProduct.getPro_name(),
                            SelectedProduct.getPro_img(),
                            SelectedProduct.getPro_mrp_ammount(),
                            SelectedProduct.getPro_desc(),
                            SelectedProduct.getPro_mrp_ammount(),
                            SelectedProduct.getPro_status(),
                            SelectedProduct.getDate(),
                            SelectedProduct.getPro_sale(),
                            SelectedProduct.getPro_cat_id(),
                            "1",
                            "SelectedProduct.getRelated_product()",
                            SelectedProduct.getName_price(),
                            SelectedProduct.getPublix_price(),
                            SelectedProduct.getName1(),
                            SelectedProduct.getPrice1(),
                            SelectedProduct.getPublix_price1(),
                            SelectedProduct.getName2(),
                            SelectedProduct.getPrice2(),
                            SelectedProduct.getPublix_price2(),
                            SelectedProduct.getName3(),
                            SelectedProduct.getPrice3(),
                            SelectedProduct.getPublix_price3()
                    );
                    new DbDeliveryCharges(mContext).addCharge(SelectedProduct.getPro_id(),
                            "  SelectedProduct.getDeliveryCharges()");

//                    new PrefHelper(mContext).save_STRING(Constant.getgrandtotal, "" + price);
//                    Intent ip = new Intent(mContext, CheckoutAct.class);
//                    startActivity(ip);
                    price = SelectedProduct.getPrice1();
                    if (price!=null){
                        new PrefHelper(mContext).save_STRING(Constant.getgrandtotal,
                                (Integer.parseInt(price)
                                        + new DbDeliveryCharges(mContext).getDeliveryCharges()) + "");
//                    new PrefHelper(mContext).save_STRING(Constant.getgrandtotal, "" + SelectedProduct.getPro_amount());
                        Intent myIntent = new Intent(ProductDetailsAct.this, CartAct.class);
                        price =SelectedProduct.getPublix_price1();
                        myIntent.putExtra("key", price);

                        startActivity(myIntent);
                    }

                }
            }*/
        });
        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String link = "https://play.google.com/store/apps/details?id=" + mContext.getPackageName();
                String text = "Save everyday with Publix, download app now " + link;


                Intent i = new Intent("android.intent.action.SEND");
                i.putExtra("android.intent.extra.TEXT", text + link);
                i.setType("text/plain");
                mContext.startActivity(Intent.createChooser(i, "Share Via"));
            }
        });

        lbAddFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PrefHelper mPrefHelper = new PrefHelper(mContext);
                if (mPrefHelper.get_STRING(Constant.EMAIL, "").equals("") || mPrefHelper.get_STRING(Constant.USERNAME, "").equals("")) {
                    Intent intent = new Intent(mContext, LoginAct.class);
                    intent.putExtra("open_from_fav", true);
                    startActivity(intent);
                } else {
                    if (wishFav.isChecked())
                    removeToFav();
                    else addToFav();
                }
            }
        });
    }

    private void getSingleProductInfo() {
        HashMap<String, Object> hmParam = new HashMap<>();
        hmParam.put("pro_id", SelectedProduct.getPro_id());
        hmParam.put("user_id", new PrefHelper(mContext).get_STRING(Constant.checkout_user_id, ""));

        api.getSingleProductInfo(hmParam).enqueue(new Callback<List<ProductPojo>>() {
            @Override
            public void onResponse(Call<List<ProductPojo>> call, Response<List<ProductPojo>> response) {
                if (response.body() != null) {
                    SelectedProduct = response.body().get(0);
                }
                loadProduct();
            }


            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {

            }
        });
    }

    private void addToFav() {
        HashMap<String, Object> hmParam = new HashMap<>();
        hmParam.put("pro_id", SelectedProduct.getPro_id());
        hmParam.put("user_id", new PrefHelper(mContext).get_STRING(Constant.checkout_user_id, ""));

        api.addToFav(hmParam).enqueue(new Callback<GenericResponseModel>() {
            @Override
            public void onResponse(Call<GenericResponseModel> call, Response<GenericResponseModel> response) {
                String message = response.body().getMessage();
                if (response.body().getResult().equals("300")) {
                    updateFavView(!wishFav.isChecked());
                }
                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GenericResponseModel> call, Throwable t) {

            }
        });
    } private void removeToFav() {
        HashMap<String, Object> hmParam = new HashMap<>();
        hmParam.put("pro_id", SelectedProduct.getPro_id());
        hmParam.put("user_id", new PrefHelper(mContext).get_STRING(Constant.checkout_user_id, ""));

        api.removeToFav(hmParam).enqueue(new Callback<GenericResponseModel>() {
            @Override
            public void onResponse(Call<GenericResponseModel> call, Response<GenericResponseModel> response) {
                String message = response.body().getMessage();
                if (response.body().getResult().equals("300")) {
                    updateFavView(!wishFav.isChecked());
                }
                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GenericResponseModel> call, Throwable t) {

            }
        });
    }


    @Override
    public void bindMethod() {

        setUpStatusBar();
        Bundle mBundle = getIntent().getExtras();
        if (mBundle != null) {
            SelectedProduct = (ProductPojo) mBundle.getSerializable("SelectedProduct");
            titleBarTv.setText(SelectedProduct.getPro_name());
            getSingleProductInfo();
//            System.out.println("JIGNESH : " + SelectedProduct.getDeliveryCharges());
            //loadProduct();
//            System.out.println("PRODUCR RELETED : first time : " + SelectedProduct.getRelated_product());
        }

        getRelatedProduct();
    }

    private void getRelatedProduct() {
        //api.getMovies()
    }

    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }

    private void updateFavView(boolean state) {
        wishFav.setChecked(state);
    }

    void loadProduct() {
        updateFavView(SelectedProduct.getIs_favorite());
        relatedListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        relatedproductList.clear();
        duplicate.clear();
        mProductWithPagination = null;

//        fullScrollView.scrollTo(5, 10);
        loadingBar.setVisibility(View.VISIBLE);
        relatedListView.setAdapter(null);
        descriptionTv.setTextColor(Color.parseColor("#fd1d53"));
        descriptionTv.setText(SelectedProduct.getName1());
        tvname2.setText(SelectedProduct.getName2());
        tvname3.setText(SelectedProduct.getName3());
        descriptionCard.setBackgroundResource(R.drawable.rounded_red_border);
        productNameTv.setText(SelectedProduct.getPro_name());
        productPriceTv.setText(getString(R.string.ruppes) + SelectedProduct.getPublix_price1());
        tvdec.setText(SelectedProduct.getPro_desc());
        //pricetvr.setText("Our Price : ₹ ");
        //originalr.setText("MRP : ₹ ");
//        originalr.setPaintFlags(originalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        originalAmount.setPaintFlags(originalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        originalAmount.setText(getString(R.string.ruppes) + SelectedProduct.getPrice1());

        imageList = Arrays.asList(SelectedProduct.getPro_img().split("\\s*,\\s*"));
        if (imageList.size() > 0) {
            imgPath = imageList.get(0);
            itemImagePager.setAdapter(new MyCustomPagerAdapter(mContext, imageList));
        }
        if (TextUtils.isEmpty(SelectedProduct.getName1())) {
            descriptionCard.setVisibility(View.GONE);
        } else {
            descriptionCard.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(SelectedProduct.getName2())) {
            name2.setVisibility(View.GONE);
        } else {
            name2.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(SelectedProduct.getName3())) {
            name3.setVisibility(View.GONE);
        } else {
            name3.setVisibility(View.VISIBLE);
        }


        descriptionCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDbCart.isBookmark(SelectedProduct.getName2()) && mDbCart.isBookmark(SelectedProduct.getName3())) {
                    addCartTv.setText("Remove from cart");
                } else {
                    addCartTv.setText("Add to cart");
                }
                descriptionTv.setTextColor(Color.parseColor("#fd1d53"));
                tvname2.setTextColor(Color.parseColor("#111111"));
                tvname3.setTextColor(Color.parseColor("#111111"));
                descriptionCard.setBackgroundResource(R.drawable.rounded_red_border);
                name2.setBackgroundResource(R.drawable.rounded_white_border);
                name3.setBackgroundResource(R.drawable.rounded_white_border);
                productPriceTv.setText(getString(R.string.ruppes) + SelectedProduct.getPublix_price1());
                isFlagVariant = 1;

                originalAmount.setPaintFlags(originalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                originalAmount.setText(getString(R.string.ruppes) + SelectedProduct.getPrice1());
            }
        });
        name2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDbCart.isBookmark(SelectedProduct.getName1()) && mDbCart.isBookmark(SelectedProduct.getName3())) {
                    addCartTv.setText("Remove from cart");
                } else {
                    addCartTv.setText("Add to cart");
                }
                tvname2.setTextColor(Color.parseColor("#fd1d53"));
                tvname3.setTextColor(Color.parseColor("#111111"));
                descriptionTv.setTextColor(Color.parseColor("#111111"));
                productPriceTv.setText(getString(R.string.ruppes) + SelectedProduct.getPublix_price2());
                descriptionCard.setBackgroundResource(R.drawable.rounded_white_border);
                name2.setBackgroundResource(R.drawable.rounded_red_border);
                name3.setBackgroundResource(R.drawable.rounded_white_border);
                originalAmount.setPaintFlags(originalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                originalAmount.setText(getString(R.string.ruppes) + SelectedProduct.getPrice2());
                isFlagVariant = 2;
            }
        });
        name3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDbCart.isBookmark(SelectedProduct.getName2()) && mDbCart.isBookmark(SelectedProduct.getName1())) {
                    addCartTv.setText("Remove from cart");
                } else {
                    addCartTv.setText("Add to cart");
                }
                tvname3.setTextColor(Color.parseColor("#fd1d53"));
                tvname2.setTextColor(Color.parseColor("#111111"));
                descriptionTv.setTextColor(Color.parseColor("#111111"));
                productPriceTv.setText(getString(R.string.ruppes) + SelectedProduct.getPublix_price3());
                descriptionCard.setBackgroundResource(R.drawable.rounded_white_border);
                name2.setBackgroundResource(R.drawable.rounded_white_border);
                name3.setBackgroundResource(R.drawable.rounded_red_border);
                originalAmount.setPaintFlags(originalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                originalAmount.setText(getString(R.string.ruppes) + SelectedProduct.getPrice3());
                isFlagVariant = 3;
            }
        });

//        relatedListView.setLayoutManager(new GridLayoutManager(mContext, 1));

        mProductWithPagination = new ProductWithPagination(mContext, relatedproductList, new onProductClick() {
            @Override
            public void click(int position) {
                relatedListView.setAdapter(null);
//                SelectedProduct = relatedproductList.get(position);
                System.out.println("PRODUCR RELETED : call click : " + "SelectedProduct.getRelated_product()");
                loadProduct();
            }
        });

        mProductWithPagination.setLoadMoreListener(new ProductWithPagination.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                relatedListView.post(new Runnable() {
                    @Override
                    public void run() {
                        int index = relatedproductList.size() - 1;
                        loadMore(index);
                    }
                });
            }
        });

        relatedListView.setHasFixedSize(true);
        relatedListView.setAdapter(mProductWithPagination);
        load(SelectedProduct.getPro_id());

        if (mDbCart.isBookmark(SelectedProduct.getPro_id())) {
            addCartTv.setText("Remove from cart");
        } else {
            addCartTv.setText("Add to cart");
        }

    }

    private void load(String catid) {
        HashMap<String, String> param = new HashMap<>();
        param.put("pro_id", catid);
        Call<List<ProductPojo>> call = api.getMovies(param);
        call.enqueue(new Callback<List<ProductPojo>>() {


            @Override
            public void onResponse(Call<List<ProductPojo>> call, retrofit2.Response<List<ProductPojo>> response) {
                loadingBar.setVisibility(View.GONE);
//                emptyLogo.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        if (!duplicate.contains(response.body().get(i).getPro_id())) {
                            duplicate.add(response.body().get(i).getPro_id());
                            relatedproductList.add(response.body().get(i));
                        }
                    }
//                    data.addAll(response.body());
                    mProductWithPagination.notifyDataChanged();
                } else {
                    System.out.println("PRODUCR RELETED : fsil " + response.body());
                }

//                Collections.sort(relatedproductList, new Comparator<ProductPojo>() {
//                    public int compare(ProductPojo v1, ProductPojo v2) {
//                        return v1.getPro_name().compareTo(v2.getPro_name());
//                    }
//                });

                System.out.println("PRODUCR RELETED : size " + relatedproductList.size());

                if (relatedproductList.size() == 0) {
                    mProductWithPagination.setMoreDataAvailable(false);
                    panelRelatedProduct.setVisibility(View.GONE);
                }

                if (relatedproductList.size() < 9) {
                    mProductWithPagination.setMoreDataAvailable(false);
                }
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("PRODUCR RELETED : error " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
                panelRelatedProduct.setVisibility(View.GONE);
            }
        });
    }

    private void loadMore(int index) {
        //add loading progress view
        relatedproductList.add(new ProductPojo("load"));
        mProductWithPagination.notifyItemInserted(relatedproductList.size() - 1);
        HashMap<String, String> param = new HashMap<>();
        param.put("pro_id", SelectedProduct.getPro_id());
        Call<List<ProductPojo>> call = api.getMovies(param);
        call.enqueue(new Callback<List<ProductPojo>>() {
            @Override
            public void onResponse(Call<List<ProductPojo>> call, retrofit2.Response<List<ProductPojo>> response) {
                loadingBar.setVisibility(View.GONE);
//                emptyLogo.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    //remove loading view
                    relatedproductList.remove(relatedproductList.size() - 1);

                    List<ProductPojo> result = response.body();
                    if (result.size() > 0) {
                        //add loaded data
                        for (int i = 0; i < result.size(); i++) {
                            if (!duplicate.contains(result.get(i).getPro_id())) {
                                duplicate.add(result.get(i).getPro_id());
                                relatedproductList.add(result.get(i));
                            }
                        }
                        panelRelatedProduct.setVisibility(View.VISIBLE);
//                        data.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        mProductWithPagination.setMoreDataAvailable(false);
                        panelRelatedProduct.setVisibility(View.GONE);
                        //telling adapter to stop calling load more as no more server data available
                        Toast.makeText(mContext, "No More Data Available", Toast.LENGTH_LONG).show();
                    }
//                    Collections.sort(relatedproductList, new Comparator<ProductPojo>() {
//                        public int compare(ProductPojo v1, ProductPojo v2) {
//                            return v1.getPro_name().compareTo(v2.getPro_name());
//                        }
//                    });
                    mProductWithPagination.notifyDataChanged();
                    //should call the custom method adapter.notifyDataChanged here to get the correct loading status
                } else {
                }
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("ERROR >>>> " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
            }
        });
    }

    ProductWithPagination mProductWithPagination;
    RelatedApi api;
    ArrayList<ProductPojo> relatedproductList;
    ArrayList<String> duplicate;

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

}
