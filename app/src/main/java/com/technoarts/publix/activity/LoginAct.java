package com.technoarts.publix.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonObject;
import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.loginApi;
import com.technoarts.publix.helpers.BasicHelper;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.helpers.Utils;
import com.technoarts.publix.pojo.ResponseModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginAct extends BaseAct implements GoogleApiClient.OnConnectionFailedListener {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn, logoImg, loginIcon;
    LinearLayout titleBarLayout;
    CardView loginCard;
    TextView registrationTv;
    EditText userNameEdt, passwordEdt;
    PrefHelper mPrefHelper;
    SignInButton loginWithGoogle;
    LoginButton loginWithFb;
    GoogleSignInClient mGoogleSignInClient;
    GoogleSignInOptions gso;
    CallbackManager callbackManager;
    int result;
    String message;
    static int RC_SIGN_IN = 1;
    ResponseModel model;

 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
        mPrefHelper = new PrefHelper(mContext);
        printHashKey();
        printKeyHash(LoginAct.this);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        loginCard = findViewById(R.id.loginCard);
        logoImg = findViewById(R.id.logoImg);
        loginIcon = findViewById(R.id.loginIcon);
        registrationTv = findViewById(R.id.registrationTv);
        userNameEdt = findViewById(R.id.userNameEdt);
        passwordEdt = findViewById(R.id.passwordEdt);
        loginWithFb = findViewById(R.id.loginWithFb);
        loginWithGoogle = findViewById(R.id.loginWithGoogle);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        MainAct.mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*Directly start MainActivity if Already LoggedIn*/
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            MainAct.account = account;
            startActivity(new Intent(this, MainAct.class));
            finish();
        }
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(loginCard, 1020, 1540, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(logoImg, 512, 512, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(loginIcon, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(loginWithFb, 420, 200, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(loginWithFb, 420, 200, 0, 0, 0, 0, Gravity.CENTER);
        mSizeUtil.setLinearLayout(loginWithGoogle, 420, 160, 0, 0, 0, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        registrationTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, RegistrationAct.class));
            }
        });

        loginCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation(v);
            }
        });

        loginWithGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        callbackManager = CallbackManager.Factory.create();
        loginWithFb.setReadPermissions();
        loginWithFb.setReadPermissions("email", "public_profile", "user_friends");
        loginWithFb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();

                getFBProfileInfo(accessToken);

               /* Profile profile = Profile.getCurrentProfile();
//                String name = profile.getName();
//                String id = profile.getId();
                System.out.println("FACEBOOK : " + profile.getName());
                System.out.println("FACEBOOK : " + profile.getId());
                System.out.println("FACEBOOK : " + "Success");
                mPrefHelper.save_BOOLEAN(Constant.isSocial, true);
                mPrefHelper.save_STRING(Constant.EMAIL, profile.getId());
                mPrefHelper.save_STRING(Constant.USERNAME, profile.getName());
                new bgRegistration(profile.getName(), profile.getId(), profile.getId(), "").execute();
                startActivity(new Intent(LoginAct.this, MainAct.class));*/
            }

            @Override
            public void onCancel() {
                // App code
                System.out.println("FACEBOOK : " + "exception.getMessage()");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                System.out.println("FACEBOOK : " + exception.getMessage());
                Log.d("google", exception.getMessage());
            }
        });

    }

    private void getFBProfileInfo(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        if (response == null) {
                            Toast.makeText(mContext, "facebook Issue", Toast.LENGTH_SHORT).show();

                        } else {
                            try {
                                String id = object.getString("id");
                                String name = object.getString("first_name");
                                String fullName = object.getString("name");
                                String email = object.getString("email");
                                mPrefHelper.save_BOOLEAN(Constant.isSocial, true);
                                mPrefHelper.save_STRING(Constant.EMAIL, id);
                                mPrefHelper.save_STRING(Constant.USERNAME, name);
                                // new bgRegistration(name, id, id, "").execute();
                                postReg( fullName, email, "", id);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(mContext, "FB" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        Log.e("FBRespone", response + "");
                        // Application code
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,name,link,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        System.out.println("FACEBOOK : " + connectionResult);

    }


    @Override
    public void bindMethod() {
        setUpStatusBar();
        System.out.println("LOGIN : inside onCreatw");
    }

    private void signIn() {
        Intent signInIntent = MainAct.mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            if (account != null) {
                System.out.println("GOOGLE : " + account.getDisplayName());
                System.out.println("GOOGLE : " + account.getEmail());
                System.out.println("GOOGLE : " + account.getFamilyName());
//            mPrefHelper.save_STRING(Constant.checkout_user_id, account.getEmail());
                mPrefHelper.save_STRING(Constant.EMAIL, account.getEmail());
                mPrefHelper.save_STRING(Constant.USERNAME, account.getDisplayName());
                mPrefHelper.save_BOOLEAN(Constant.isSocial, true);
               // new bgRegistration(account.getDisplayName(), account.getEmail(), account.getEmail(), "").execute();
                MainAct.account = account;
                //startActivity(new Intent(this, MainAct.class));
                //finish();
                postReg(account.getDisplayName(),account.getEmail(),account.getEmail(),account.getId());
            }
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.w("LoginActivity", "signInResult:failed code=" + e.getStatusCode());
        }

    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "tutorialwing.com.facebookintegrationtutorial",
                    PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }


    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }

    private void checkValidation(View view) {
        // Get email id and password
        String user_email = userNameEdt.getText().toString();
        String password = passwordEdt.getText().toString();

        // Check patter for email id
        Pattern p = Pattern.compile(Utils.regEx);

        Matcher m = p.matcher(user_email);

        // Check for both field is empty or not
        if (user_email.equals("") || user_email.length() == 0
                || password.equals("") || password.length() == 0) {
            new BasicHelper().Show_Toast(mContext, view,
                    "Enter both credentials.");

        } else {
            postCode(user_email, password, false);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void postCode(final String user_email, String password, final boolean isSocial) {
        final Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("user_email", user_email);
        jsonParams.put("password", password);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                (new JSONObject(jsonParams)).toString());

        System.out.println("REGI : response : " + new JSONObject(jsonParams).toString());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(loginApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        loginApi api = retrofit.create(loginApi.class);
        Call<JsonObject> call = api.logion(jsonParams);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                System.out.println("REGI : response : " + response.isSuccessful());
                if (response.isSuccessful()) {
                    String result = response.body().get("result").toString().replaceAll("\"", "");
//                    String msg = response.body().get("message").toString();
                    JSONObject jsonObj = null;
                    try {
                        jsonObj = new JSONObject(String.valueOf(response.body()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("REGI : response : " + response.body().toString());
                    System.out.println("REGI : response : " + Integer.parseInt(result) + " ****");
                    if (result.equals("200") || result.equals("100")) {
                        String message = response.body().get("message").getAsString();
                        Toast.makeText(LoginAct.this, message, Toast.LENGTH_SHORT).show();
                        // new BasicHelper().Show_Toast(mContext, , "Login Fail");
                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                wrong();
                            }
                        })*/
                        ;
//                        new BasicHelper().Show_Toast(mContext, signUpButton, msg);
                    }
                    if (result.equals("300")) {
                        mPrefHelper.save_BOOLEAN(Constant.isSocial, isSocial);
                        if (!isSocial) {
                            try {
                                Toast.makeText(LoginAct.this, "Login Successfully", Toast.LENGTH_SHORT).show();
//                                mPrefHelper.save_STRING(Constant.USERNAME, new JSONObject().getString("user_email"));
                                mPrefHelper.save_STRING(Constant.checkout_user_id, response.body().get("user_id").getAsString());
                                mPrefHelper.save_STRING(Constant.USERNAME, jsonObj.getString("user_name"));
//                                mPrefHelper.save_STRING(Constant.USERNAME, user_email);
                                mPrefHelper.save_STRING(Constant.EMAIL, user_email);
                                finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

//                            Toast.makeText(mContext, "login successful..", Toast.LENGTH_SHORT).show();
//                        Intent returnIntent = new Intent();
//                        setResult(Activity.RESULT_OK, returnIntent);
//                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                System.out.println("REGI : 1*error*1 : " + t.getMessage());
            }
        });
    }

    public String postJson(String user_email, String password, final boolean isSocial) {
        RequestBody formBody = new FormBody.Builder()
                .add("user_email", user_email)
                .add("password", password)
                .build();
        Log.e("payment_id", user_email);
        Log.e("payment", password);
        final OkHttpClient client = new OkHttpClient();
        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url("https://thepublix.com/tpxadmin/Users/publix_login")
                .post(formBody)
                .build();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    okhttp3.Response response = null;
                    try {
                        //getresponse();
                        response = client.newCall(request).execute();
                        String getresponse = response.body().string();
                        Log.e("response", getresponse);
                        JSONObject jsonObj = new JSONObject(getresponse);
                        int result = Integer.parseInt(jsonObj.getString("result"));
                        System.out.println("LOGIN : " + request);
                        System.out.println("LOGIN : " + getresponse);
                        if (result == 300) {
                            mPrefHelper.save_BOOLEAN(Constant.isSocial, isSocial);
                            if (!isSocial)
                                mPrefHelper.save_STRING(Constant.USERNAME, jsonObj.getString("user_name"));
                            mPrefHelper.save_STRING(Constant.checkout_user_id, jsonObj.getString("user_id"));
                            mPrefHelper.save_STRING(Constant.EMAIL, jsonObj.getString("user_email"));
//                            Toast.makeText(mContext, "login successful..", Toast.LENGTH_SHORT).show();
                            finish();
                        } else if (result == 200) {
                            Toast.makeText(mContext, "Try Again..", Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        System.out.println("LOGIN : ERRO 1 : " + e.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("LOGIN : ERRO 2 : " + e.getMessage());
                }
            }
        });

        thread.start();

        return null;
    }

    private void postReg( final String fullname, final String email, final String password, String socialid) {
        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("device_type", "Android");
        jsonParams.put("fullname", fullname);
        jsonParams.put("email", email);
        jsonParams.put("password", password);
        jsonParams.put("socialid", socialid);


        System.out.println("REGI : response : " + new JSONObject(jsonParams).toString());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(loginApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        loginApi api = retrofit.create(loginApi.class);
        Call<JsonObject> call = api.getUser(jsonParams);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                System.out.println("REGI : response : " + response.isSuccessful());
                if (response.isSuccessful()) {
                    String result = response.body().get("result").toString().replaceAll("\"", "");
                    String msg = response.body().get("message").toString();
                    System.out.println("REGI : response : " + response.body().toString());
                    System.out.println("REGI : response : " + Integer.parseInt(result) + " ****");

                    if (result.equals("300") || result.equals("200")) {
                        String user_id = response.body().get("user_id").getAsString();
                        mPrefHelper.save_STRING(Constant.checkout_user_id, user_id);
                        mPrefHelper.save_STRING(Constant.USERNAME, fullname);

                        System.out.println("REGISTER : response : " + message);

                    }
                   finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.out.println("REGI : 1*error*1 : " + t.getMessage());
            }
        });
    }

    private void registerUser(final String device_type,
                              final String fullname,
                              final String user_id,
                              final String email,
                              final String password) {
        RequestBody formBody = new FormBody.Builder()
                .add("device_type", device_type)
                .add("fullname", String.valueOf(fullname))
                .add("username", email)
                .add("email", email)
                .add("password", password/*"90301171XX"*/)
                .build();
        Log.e("payment_id", user_id);
        Log.e("payment", email);
        final OkHttpClient client = new OkHttpClient();
        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url("https://thepublix.com/tpxadmin/Users/publix_register")
                .post(formBody)
                .build();
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    okhttp3.Response response = null;
                    try {
                        //getresponse();
                        response = client.newCall(request).execute();
                        String getresponse = response.body().string();
                        System.out.println("REGISTER : response : " + getresponse);
                        Log.e("response", getresponse);
                        JSONArray arr = new JSONArray(getresponse);
                        if (arr.length() != 0) {
                            JSONObject jsonObj = arr.getJSONObject(0);
                            result = Integer.parseInt(jsonObj.getString("result"));
                            Log.e("getresponse", String.valueOf(result));
                            message = jsonObj.getString("user_id") + "";

                            System.out.println("LOGIN : " + result);
                            Log.e("message", message);
                            if (result == 300) {
                                mPrefHelper.save_STRING(Constant.checkout_user_id, message);
                                System.out.println("REGISTER : response : " + message);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                    showpopup();
                                        mPrefHelper.save_STRING(Constant.USERNAME, fullname);
                                    }
                                });
                            } else if (result == 200) {
                                postJson(user_id, password, true);
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                    failpopup();
                                    }
                                });


                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.out.println("REGISTER : EROR 1 : " + e.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("REGISTER : EROR 2 : " + e.getMessage());
                    postJson(user_id, password, true);
                }
            }
        });

        thread.start();
    }

    private void wrong() {
        final ProgressDialog pdialog = new ProgressDialog(mContext);
        pdialog.setMessage("Loading...");
        pdialog.setCancelable(false);
        pdialog.setInverseBackgroundForced(false);
        pdialog.show();
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.wrong);
        TextView ok = (TextView) dialog.findViewById(R.id.txt_ok);
        TextView fail = (TextView) dialog.findViewById(R.id.fail);
        fail.setVisibility(View.VISIBLE);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                pdialog.dismiss();
            }
        });
        dialog.show();
    }
}