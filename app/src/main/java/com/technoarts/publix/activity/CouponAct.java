package com.technoarts.publix.activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.adapters.CouponAdapter;
import com.technoarts.publix.asyncs.LoadCoupon;
import com.technoarts.publix.callback.onCouponLoad;
import com.technoarts.publix.helpers.BasicHelper;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.CouponPojo;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class CouponAct extends BaseAct {
    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout;
    RecyclerView couponListView;
    TextView emptyTv;
    ProgressBar loadingBar;
    boolean isFromDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_coupon;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        couponListView = findViewById(R.id.couponListView);
        emptyTv = findViewById(R.id.emptyTv);
        loadingBar = findViewById(R.id.loadingBar);
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    public void bindMethod() {
        setUpStatusBar();
        Bundle mBundle = getIntent().getExtras();
        couponListView.setLayoutManager(new GridLayoutManager(mContext, 1));
        if (mBundle != null) {
            isFromDrawer = mBundle.getBoolean("isDrawer");
            final String minPrice = mBundle.getInt("MinPrice") + "";
            new LoadCoupon(mContext, "0", new onCouponLoad() {
                @Override
                public void onLoadComplete(final ArrayList<CouponPojo> coupon) {
                    loadingBar.setVisibility(View.GONE);
                    if (coupon.size() > 0) {
                        emptyTv.setVisibility(View.GONE);
                        couponListView.setAdapter(new CouponAdapter(mContext, coupon, isFromDrawer, new CouponAdapter.onClickCoupon() {
                            @Override
                            public void onClick(int position) {
                                System.out.println("COUPON : min price " + coupon.get(position).getCp_min_price());
                                System.out.println("COUPON : total price " + minPrice);
                                if (Integer.parseInt(minPrice) >= Integer.parseInt(coupon.get(position).getCp_min_price())) {

                                    new PrefHelper(mContext).save_STRING(Constant.getcoupon, coupon.get(position).getCp_price());
                                    postJson(new PrefHelper(mContext).get_STRING(Constant.checkout_user_id, ""),
                                            coupon.get(position).getCp_id());
                                    finish();
                                } else {
                                    new BasicHelper().Show_Toast(mContext, CouponAct.this.emptyTv, "you need to purchase item of rupees " + coupon.get(position).getCp_min_price() + " or more");
//                                    Toast.makeText(mContext, ", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }));
                    } else {
                        emptyTv.setVisibility(View.VISIBLE);
                    }
                }
            }).execute();
        }
    }

    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
        }
    }

    public void postJson(String userid, String cpId) {

        RequestBody formBody = new FormBody.Builder()
                .add("user_id", userid /*"Jurassic@Park.com"*/)
                .add("coupon_id", cpId/*"90301171XX"*/)
                .build();

        final OkHttpClient client = new OkHttpClient();
        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url(mContext.getResources().getString(R.string.prefix_domain)
                        + "Coupon/insert")
                .post(formBody)
                .build();
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    okhttp3.Response response = null;
                    try {
                        //getresponse();
                        response = client.newCall(request).execute();
                        String getresponse = response.body().string();
                        Log.e("response", getresponse);
                        JSONObject jsonObj = new JSONObject(getresponse);
//                        int result = Integer.parseInt(jsonObj.getString("result"));
                        System.out.println("LOGIN : " + getresponse);


                    } catch (IOException e) {
                        System.out.println("LOGIN : 1 " + e.getMessage());
                    }
                } catch (Exception e) {
                    System.out.println("LOGIN : 2 " + e.getMessage());
                }
            }
        });

        thread.start();

    }
}
