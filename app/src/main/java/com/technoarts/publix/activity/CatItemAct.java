package com.technoarts.publix.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.ArrayMap;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.ProductApi;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.paginationAdapter.ProductWithPagination;
import com.technoarts.publix.pojo.CategoryPojo;
import com.technoarts.publix.pojo.ProductPojo;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class CatItemAct extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout, searchLayout;
    CategoryPojo selectedItem;
//    ProductPojo selectedItem;
    TextView titleBarTv;
    RecyclerView productListView;
    ProgressBar loadingBar;
    TextView emptyLogo;

    ProductApi api;
    ArrayList<ProductPojo> productList;
    ArrayList<String> duplicate;
    ProductWithPagination mProductWithPagination;
    EditText searchEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_cat_item;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
        productList = new ArrayList<>();
        duplicate = new ArrayList<>();
        api = ServiceGenerator.createService(ProductApi.class);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        titleBarTv = findViewById(R.id.titleBarTv);
        productListView = findViewById(R.id.productListView);
        loadingBar = findViewById(R.id.loadingBar);
        emptyLogo = findViewById(R.id.emptyLogo);
        searchLayout = findViewById(R.id.searchLayout);
        searchEdt = findViewById(R.id.searchEdt);
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayoutMatchWidth(searchLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        searchEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filterList(editable.toString());
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void bindMethod() {
        setUpStatusBar();
        productListView.setLayoutManager(new GridLayoutManager(mContext, 2));
        Bundle mBundle = getIntent().getExtras();
        if (mBundle != null) {
            selectedItem = (CategoryPojo) mBundle.getSerializable("CatName");
            titleBarTv.setText(selectedItem.getCat_name());
//            new LoadSelectedProducts(mContext, selectedItem.getCat_id(), new onProductLoadComplete() {
//                @Override
//                public void onLoadComplete(final ArrayList<ProductPojo> productList) {
//                    loadingBar.setVisibility(View.GONE);
//                    Collections.sort(productList, new Comparator<ProductPojo>() {
//                        public int compare(ProductPojo v1, ProductPojo v2) {
//                            return v1.getPro_name().compareTo(v2.getPro_name());
//                        }
//                    });
//                    if (productList.size() == 0) {
//                        emptyLogo.setVisibility(View.VISIBLE);
//                    } else {
//                        emptyLogo.setVisibility(View.GONE);
//                        productListView.setAdapter(new ProductListAdapter(mContext, productList, new onProductClick() {
//                            @Override
//                            public void click(int position) {
//                                Intent ip = new Intent(mContext, ProductDetailsAct.class);
//                                ip.putExtra("SelectedProduct", productList.get(position));
//                                startActivity(ip);
//                            }
//                        }));
//                    }
//                }
//            }).execute();


            mProductWithPagination = new ProductWithPagination(mContext, productList, new onProductClick() {
                @Override
                public void click(int position) {
                    Intent ip = new Intent(mContext, ProductDetailsAct.class);
                    ip.putExtra("SelectedProduct", (productList.get(position)));
                    startActivity(ip);
                }
            });
            mProductWithPagination.setLoadMoreListener(new ProductWithPagination.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    productListView.post(new Runnable() {
                        @Override
                        public void run() {
                            int index = productList.size() - 1;
//                            loadMore(index);
                        }
                    });
                }
            });
            productListView.setHasFixedSize(true);
            productListView.setAdapter(mProductWithPagination);
            load(0);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void load(int index) {

        Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("cat_id", selectedItem.getCat_id());
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                (new JSONObject(jsonParams)).toString());

        System.out.println("JIGNESH :  " + new JSONObject(jsonParams));
        Call<List<ProductPojo>> call = api.getMovies(body);
        call.enqueue(new Callback<List<ProductPojo>>() {


            @Override
            public void onResponse(Call<List<ProductPojo>> call, retrofit2.Response<List<ProductPojo>> response) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("JIGNESH : ******** " +response.body());
                System.out.println("JIGNESH : ******** " +response.errorBody());
                System.out.println("JIGNESH : ******** " +response.message());
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().size(); i++) {
                        if (!duplicate.contains(response.body().get(i).getPro_id())) {
                            duplicate.add(response.body().get(i).getPro_id());
                            productList.add(response.body().get(i));
                            System.out.println("JIGNESH : ******** " + (response.body().get(i).getDeliveryCharges()));
                        }
                    }
//                    data.addAll(response.body());
                    mProductWithPagination.notifyDataChanged();
                } else {
                    System.out.println("JIGNESH : >>>> " + response.message());
                    emptyLogo.setText("product not available yet!");
                }

                Collections.sort(productList, new Comparator<ProductPojo>() {
                    public int compare(ProductPojo v1, ProductPojo v2) {
                        return v1.getPro_name().compareTo(v2.getPro_name());
                    }
                });

                if (productList.size() < 10) {
                    mProductWithPagination.setMoreDataAvailable(false);
                }
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                System.out.println("JIGNESH : >>>> " + t.getMessage());
                loadingBar.setVisibility(View.GONE);
                if (isNetworkAvailable()) {
                    emptyLogo.setText("product not available yet!");
                } else {
                    emptyLogo.setText("You are offline, please try again letter");
                    openSupportContactDialog();

                }
                emptyLogo.setVisibility(View.VISIBLE);
                System.out.println("JIGNESH : >>>> " + t.getMessage());
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void loadMore(int index) {
        //add loading progress view
        productList.add(new ProductPojo("load"));
        mProductWithPagination.notifyItemInserted(productList.size() - 1);

        Map<String, Object> jsonParams = new ArrayMap<>();
        jsonParams.put("cat_id", selectedItem.getCat_id());
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                (new JSONObject(jsonParams)).toString());

        System.out.println("VERIFY :  " + new JSONObject(jsonParams));



        Call<List<ProductPojo>> call = api.getMovies(body);
        call.enqueue(new Callback<List<ProductPojo>>() {
            @Override
            public void onResponse(Call<List<ProductPojo>> call, retrofit2.Response<List<ProductPojo>> response) {
                loadingBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    //remove loading view
                    productList.remove(productList.size() - 1);

                    List<ProductPojo> result = response.body();
                    if (result.size() > 0) {
                        //add loaded data
                        for (int i = 0; i < result.size(); i++) {
                            if (!duplicate.contains(result.get(i).getPro_id())) {
                                duplicate.add(result.get(i).getPro_id());
                                productList.add(result.get(i));
                            }
                        }
//                        data.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        mProductWithPagination.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
                        Toast.makeText(mContext, "No More Data Available", Toast.LENGTH_LONG).show();
                    }
                    Collections.sort(productList, new Comparator<ProductPojo>() {
                        public int compare(ProductPojo v1, ProductPojo v2) {
                            return v1.getPro_name().compareTo(v2.getPro_name());
                        }
                    });
                    mProductWithPagination.notifyDataChanged();
                    //should call the custom method adapter.notifyDataChanged here to get the correct loading status
                } else {
                }
            }

            @Override
            public void onFailure(Call<List<ProductPojo>> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                if (isNetworkAvailable()) {
                    emptyLogo.setText("product not available yet!");
                } else {
                    emptyLogo.setText("You are offline, please try again letter");
                    openSupportContactDialog();

                }
                emptyLogo.setVisibility(View.VISIBLE);
                System.out.println("ERROR >>>> " + t.getMessage());
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }

    void filterList(String text) {
        ArrayList<ProductPojo> temp = new ArrayList();
        try {
            for (ProductPojo d : productList) {
                if (d.getPro_name().toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d);
                }
            }
            mProductWithPagination.filterList(temp);

        } catch (Exception e) {
            if (temp != null && mProductWithPagination != null)
                mProductWithPagination.filterList(temp);
            System.out.println("PRODUCT ITEMS : filter error " + e.getMessage());
        }
    }
}
