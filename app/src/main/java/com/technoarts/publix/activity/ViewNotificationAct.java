package com.technoarts.publix.activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.technoarts.publix.BaseAct;

import com.technoarts.publix.R;
import com.technoarts.publix.helpers.NotificationVO;
import com.technoarts.publix.helpers.SizeUtil;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewNotificationAct extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout;
    String selectedItem;
    TextView titleBarTv;
    NotificationVO selectedNotification;
    CircleImageView profile_image;

    TextView notiHeading, notiBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return (R.layout.activity_view_notification);
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        titleBarTv = findViewById(R.id.titleBarTv);
        profile_image = findViewById(R.id.profile_image);
        notiHeading = findViewById(R.id.notiHeading);
        notiBody = findViewById(R.id.notiBody);
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void bindMethod() {
        setUpStatusBar();
        Bundle mBundle = getIntent().getExtras();
        if (mBundle != null) {
            selectedNotification = (NotificationVO) mBundle.getSerializable("selectedNotification");
//            Glide.with(mContext).load(selectedNotification.getIconUrl()).into(profile_image);

            Glide.with(mContext)
                    .applyDefaultRequestOptions(new RequestOptions()
                            .placeholder(R.drawable.launcher)
                            .error(R.drawable.launcher))
                    .load(selectedNotification.getIconUrl()).into(profile_image);

            notiHeading.setText(selectedNotification.getTitle());
            notiBody.setText(selectedNotification.getMessage());
        }
    }

    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }
}
