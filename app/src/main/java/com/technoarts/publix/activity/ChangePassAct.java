package com.technoarts.publix.activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.technoarts.publix.BaseAct;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.ChangePasswordApi;
import com.technoarts.publix.helpers.BasicHelper;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.ServiceGenerator;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.GenericResponseModel;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import retrofit2.Call;
import retrofit2.Callback;

public class ChangePassAct extends BaseAct {

    Context mContext;
    SizeUtil mSizeUtil;
    ImageView backBtn;
    LinearLayout titleBarLayout;

    Button b1, b2;
    EditText copwd, npwd, cpwd;
    private static View view;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    String user_id, getcopwd, getnpwd, getcpwd;
    public static int notificationCountCart = 0;
    int result = 0;
    String response_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_change_pass;
    }

    @Override
    public void bindObjects(Context mContext) {
        this.mContext = mContext;
        mSizeUtil = new SizeUtil(mContext);
    }

    @Override
    public void bindViews() {
        titleBarLayout = findViewById(R.id.titleBarLayout);
        backBtn = findViewById(R.id.backBtn);
        user_id = new PrefHelper(mContext).get_STRING(Constant.checkout_user_id, "");
        Log.e("splanguage", user_id);
        copwd = (EditText) findViewById(R.id.currentpassword);
        npwd = (EditText) findViewById(R.id.newpwd);
        cpwd = (EditText) findViewById(R.id.cpwd);

        b1 = (Button) findViewById(R.id.clear);
        b2 = (Button) findViewById(R.id.submit);
    }

    @Override
    public void bindPixel() {
        mSizeUtil.setLinearLayoutMatchWidth(titleBarLayout, 160, 0, 0, 0, 0, 0);
        mSizeUtil.setLinearLayout(backBtn, 70, 70, 0, 30, 30, 0, Gravity.CENTER);
    }

    @Override
    public void bindListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getcopwd = copwd.getText().toString();
                getnpwd = npwd.getText().toString();
                getcpwd = cpwd.getText().toString();
                Log.e("pwd", String.valueOf(getcopwd));
                Log.e("pwd", String.valueOf(getnpwd));
                if (getcopwd.equals("") || getcopwd.length() == 0
                        || getnpwd.equals("") || getnpwd.length() == 0) {
                    new BasicHelper().Show_Toast(getApplicationContext(), v,
                            "Please enter all details.");
                }
                if (getnpwd.length() > 0 && getcpwd.length() > 0) {
                    if (!getcpwd.equals(getnpwd)) {
                        // give an error that password and confirm password not match
                        new BasicHelper().Show_Toast(getApplicationContext(), v,
                                "Password does not match");
                    } else {
                        Postdata(user_id);
                    }

                }

            }


        });
    }

    @Override
    public void bindMethod() {
        setUpStatusBar();
    }

    void setUpStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.main_dark));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            statusBarBg.setVisibility(View.VISIBLE);
        }
    }

    private void Postdata(String user_id) {
        Map<String, String> jsonParams = new HashMap<>();
        jsonParams.put("user_id", user_id);
        jsonParams.put("password", String.valueOf(getcopwd));
        jsonParams.put("new_password", String.valueOf(getnpwd));


        Call<GenericResponseModel> api = ServiceGenerator.createService(ChangePasswordApi.class).changePassword(jsonParams);
        api.enqueue(new Callback<GenericResponseModel>() {
            @Override
            public void onResponse(Call<GenericResponseModel> call, retrofit2.Response<GenericResponseModel> response) {
                Log.e("FCM", "Response" + response.isSuccessful());
                String message = response.body().getMessage();
                if (response.body().getResult().equals("300")) {
                    finish();
                }
                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GenericResponseModel> call, Throwable t) {
                Log.e("FCM", "Response" + t.getLocalizedMessage());
                finish();

            }
        });
    }

    /*private void Postdata(final String user_id) {

        RequestBody formBody = new FormBody.Builder()
                .add("user_id", user_id*//*"Jurassic@Park.com"*//*)
                .add("password", String.valueOf(getcopwd))
                .add("new_password", String.valueOf(getnpwd))
                .build();

        final OkHttpClient client = new OkHttpClient();
        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url(mContext.getResources().getString(R.string.prefix_domain) + "Users/change_password")
                .post(formBody)
                .build();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Response response = null;

                    //getresponse();
                    response = client.newCall(request).execute();
                    String getresponse = response.body().string();
                    Log.e("response", getresponse);
                    JSONObject jsonObj = new JSONObject(getresponse);
                    result = Integer.parseInt(jsonObj.getString("result"));
                    Log.e("getresponse", String.valueOf(result));
                    response_message = jsonObj.getString("message");
                    Log.e("response_price", response_message);
                    System.out.println("CHANGE ME : "+response);
                    if (result == 300) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ProgressDialog pdialog = new ProgressDialog(mContext);
                                pdialog.setMessage("Loading...");
                                pdialog.setCancelable(false);
                                pdialog.setInverseBackgroundForced(false);
                                pdialog.show();
                                Toast.makeText(getApplicationContext(), "" + response_message, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(getApplicationContext(), MainAct.class);
                                startActivity(i);
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "" + response_message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("CHANGE ME : "+e.getMessage());
                }
            }
        });
        thread.start();
    }*/
}
