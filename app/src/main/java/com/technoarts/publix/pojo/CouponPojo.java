package com.technoarts.publix.pojo;

public class CouponPojo {
    String cp_id,cp_code,pro_id,cp_date,cp_price,cp_min_price;

    public CouponPojo(String cp_id, String cp_code, String pro_id, String cp_date, String cp_price, String cp_min_price) {
        this.cp_id = cp_id;
        this.cp_code = cp_code;
        this.pro_id = pro_id;
        this.cp_date = cp_date;
        this.cp_price = cp_price;
        this.cp_min_price = cp_min_price;
    }

    public String type;

    public CouponPojo(String type) {
        this.type = type;
    }

    public String getCp_id() {
        return cp_id;
    }

    public void setCp_id(String cp_id) {
        this.cp_id = cp_id;
    }

    public String getCp_code() {
        return cp_code;
    }

    public void setCp_code(String cp_code) {
        this.cp_code = cp_code;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getCp_date() {
        return cp_date;
    }

    public void setCp_date(String cp_date) {
        this.cp_date = cp_date;
    }

    public String getCp_price() {
        return cp_price;
    }

    public void setCp_price(String cp_price) {
        this.cp_price = cp_price;
    }

    public String getCp_min_price() {
        return cp_min_price;
    }

    public void setCp_min_price(String cp_min_price) {
        this.cp_min_price = cp_min_price;
    }
}
