package com.technoarts.publix.pojo;

/**
 * Created by NIDHI on 10/30/2018.
 */

public class Slidermodel {

    String s_id;
    String sname;
    String img;

    public Slidermodel(String s_id, String sname, String img) {
        this.s_id = s_id;
        this.sname = sname;
        this.img = img;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
