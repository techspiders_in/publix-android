package com.technoarts.publix.pojo;

import java.io.Serializable;

public class ProductPojo implements Serializable {
    String pro_id;
    String pro_name;
    String pro_cat_id;
    String pro_desc;
    String pro_sale;
    String pro_amount;
    String pro_mrp_ammount;
    String pro_status;
    String date;
    String pro_img;
    String qty;
    String related_product;
    String name_price;
    String deliverycharge;
    String name1;
    String price1;
    String publix_price1;
    String name2;
    String price2;
    String publix_price2;
    boolean is_favorite;

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getPrice1() {
        return price1;
    }

    public void setPrice1(String price1) {
        this.price1 = price1;
    }

    public String getPublix_price1() {
        return publix_price1;
    }

    public void setPublix_price1(String publix_price1) {
        this.publix_price1 = publix_price1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getPrice2() {
        return price2;
    }

    public void setPrice2(String price2) {
        this.price2 = price2;
    }

    public String getPublix_price2() {
        return publix_price2;
    }

    public void setPublix_price2(String publix_price2) {
        this.publix_price2 = publix_price2;
    }

    public String getName3() {
        return name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public String getPrice3() {
        return price3;
    }

    public void setPrice3(String price3) {
        this.price3 = price3;
    }

    public String getPublix_price3() {
        return publix_price3;
    }

    public void setPublix_price3(String publix_price3) {
        this.publix_price3 = publix_price3;
    }

    String name3;
    String price3;
    String publix_price3;

    public String getPublix_price() {
        return publix_price;
    }

    public void setPublix_price(String publix_price) {
        this.publix_price = publix_price;
    }

    String publix_price;

    public String getName_price() {
        return name_price;
    }

    public void setName_price(String name_price) {
        this.name_price = name_price;
    }


    public ProductPojo() {
    }

    public ProductPojo(String pro_id,
                       String pro_name,
                       String pro_cat_id,
                       String deliverycharge,
                       String pro_desc,
                       String pro_sale,
                       String pro_amount,
                       String pro_mrp_ammount,
                       String pro_status,
                       String date,
                       String pro_img,
                       String qty,
                       String related_product,
                       String name_price,
                       String publix_price,
                       String name1,
                       String price1,
                       String publix_price1,
                       String name2,
                       String price2,
                       String publix_price2,
                       String name3,
                       String price3,
                       String publix_price3
    ) {
        this.pro_id = pro_id;
        this.pro_name = pro_name;
        this.pro_cat_id = pro_cat_id;
        this.pro_desc = pro_desc;
        this.pro_sale = pro_sale;
        this.pro_amount = pro_amount;
        this.pro_mrp_ammount = pro_mrp_ammount;
        this.pro_status = pro_status;
        this.date = date;
        this.pro_img = pro_img;
        this.qty = qty;
        this.related_product = related_product;
        this.deliverycharge = deliverycharge;
        this.name_price = name_price;
        this.publix_price = publix_price;
        this.publix_price1 = publix_price1;
        this.publix_price2 = publix_price2;
        this.publix_price3 = publix_price3;
        this.name1 = name1;
        this.name2 = name2;
        this.name3 = name3;
        this.price1 = price1;
        this.price2 = price2;
        this.price3 = price3;
    }

    public String getDeliveryCharges() {
        return deliverycharge;
    }

    public void setDeliveryCharges(String deliveryCharges) {
        this.deliverycharge = deliveryCharges;
    }

    public String getRelated_product() {
        return related_product;
    }

    public void setRelated_product(String related_product) {
        this.related_product = related_product;
    }

    public String type;

    public ProductPojo(String type) {
        this.type = type;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_cat_id() {
        return pro_cat_id;
    }

    public void setPro_cat_id(String pro_cat_id) {
        this.pro_cat_id = pro_cat_id;
    }

    public String getPro_desc() {
        return pro_desc;
    }

    public void setPro_desc(String pro_desc) {
        this.pro_desc = pro_desc;
    }

    public String getPro_sale() {
        return pro_sale;
    }

    public void setPro_sale(String pro_sale) {
        this.pro_sale = pro_sale;
    }

    public String getPro_amount() {
        return pro_amount;
    }

    public void setPro_amount(String pro_amount) {
        this.pro_amount = pro_amount;
    }

    public String getPro_mrp_ammount() {
        return pro_mrp_ammount;
    }

    public void setPro_mrp_ammount(String pro_mrp_ammount) {
        this.pro_mrp_ammount = pro_mrp_ammount;
    }

    public String getPro_status() {
        return pro_status;
    }

    public void setPro_status(String pro_status) {
        this.pro_status = pro_status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPro_img() {
        return pro_img;
    }

    public void setPro_img(String pro_img) {
        this.pro_img = pro_img;
    }

    public Boolean getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(Boolean is_favorite) {
        this.is_favorite = is_favorite;
    }
}
