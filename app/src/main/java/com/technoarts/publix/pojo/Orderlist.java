package com.technoarts.publix.pojo;

/**
 * Created by NIDHI on 11/5/2018.
 */

public class Orderlist {
    String pro_name;
    String qty;
    String pro_amount;
    String pro_img;
    String deliverycharge;


    public Orderlist(String pro_name, String qty, String pro_amount, String pro_img,String deliverycharge) {
        this.pro_name = pro_name;
        this.qty = qty;
        this.pro_amount = pro_amount;
        this.pro_img = pro_img;
        this.deliverycharge = deliverycharge;
    }

//    public String getMobile_no() {
//        return mobile_no;
//    }
//
//    public void setMobile_no(String mobile_no) {
//        this.mobile_no = mobile_no;
//    }

    public String getDeliverycharge() {
        return deliverycharge;
    }

    public void setDeliverycharge(String deliverycharge) {
        this.deliverycharge = deliverycharge;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPro_amount() {
        return pro_amount;
    }

    public void setPro_amount(String pro_amount) {
        this.pro_amount = pro_amount;
    }

    public String getPro_img() {
        return pro_img;
    }

    public void setPro_img(String pro_img) {
        this.pro_img = pro_img;
    }
}
