package com.technoarts.publix.pojo;

import androidx.fragment.app.Fragment;

public class DrawerPojo {
    String name;
    int resourceIcon;
    boolean isSelected;
    Fragment mFragment;

    public DrawerPojo(String name, int resourceIcon, boolean isSelected, Fragment mFragment) {
        this.name = name;
        this.resourceIcon = resourceIcon;
        this.isSelected = isSelected;
        this.mFragment=mFragment;
    }

    public Fragment getmFragment() {
        return mFragment;
    }

    public void setmFragment(Fragment mFragment) {
        this.mFragment = mFragment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResourceIcon() {
        return resourceIcon;
    }

    public void setResourceIcon(int resourceIcon) {
        this.resourceIcon = resourceIcon;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
