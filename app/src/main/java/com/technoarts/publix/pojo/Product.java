package com.technoarts.publix.pojo;

/**
 * Created by NIDHI on 10/20/2018.
 */

public class Product {

    String pro_id;
    String pro_name;
    String pro_img;
    String pro_desc;
    String pro_mrp_ammount;
    String pro_amount;
    String pro_sale;


    public Product(String pro_id, String pro_name, String pro_img, String pro_desc, String pro_mrp_ammount, String pro_amount, String sale) {
        this.pro_id = pro_id;
        this.pro_name = pro_name;
        this.pro_img = pro_img;
        this.pro_desc = pro_desc;
        this.pro_mrp_ammount = pro_mrp_ammount;
        this.pro_amount = pro_amount;
        this.pro_sale = pro_sale;
    }


    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_img() {
        return pro_img;
    }

    public void setPro_img(String pro_img) {
        this.pro_img = pro_img;
    }

    public String getPro_desc() {
        return pro_desc;
    }

    public void setPro_desc(String pro_desc) {
        this.pro_desc = pro_desc;
    }

    public String getPro_mrp_ammount() {
        return pro_mrp_ammount;
    }

    public void setPro_mrp_ammount(String pro_mrp_ammount) {
        this.pro_mrp_ammount = pro_mrp_ammount;
    }

    public String getPro_amount() {
        return pro_amount;
    }

    public void setPro_amount(String pro_amount) {
        this.pro_amount = pro_amount;
    }

    public String getPro_sale() {
        return pro_sale;
    }

    public void setPro_sale(String pro_sale) {
        this.pro_sale = pro_sale;
    }
}
