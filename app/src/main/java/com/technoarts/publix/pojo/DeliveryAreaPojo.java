package com.technoarts.publix.pojo;

public class DeliveryAreaPojo {
    String areaname,id;

    public DeliveryAreaPojo(String areaname, String id) {
        this.areaname = areaname;
        this.id = id;
    }

    public String getAreaname() {
        return areaname;
    }

    public void setAreaname(String areaname) {
        this.areaname = areaname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
