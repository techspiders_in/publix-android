package com.technoarts.publix.pojo;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("all")
public class GenericResponseModel {

    @SerializedName("result")
    private  String result;

    @SerializedName("message")
    private  String message;

    public GenericResponseModel(String result, String message) {
        this.result = result;
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
