package com.technoarts.publix.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CategoryPojo implements Serializable {
    @Expose
    @SerializedName("category_id")
    String cat_id;

    @Expose
    @SerializedName("category_name")
    String cat_name;

    @Expose
    @SerializedName("flag")
    String flag;

    @Expose
    @SerializedName("Image")
    String icon_img;

    @Expose
    @SerializedName("img")
    String img;

    String  deliverycharge;

    boolean isSelected;

    public String type;

    public CategoryPojo(String cat_id,
                        String cat_name,
                        String flag,
                        String icon_img,
                        String img,
                        String deliverycharge,
                        boolean isSelected) {
        this.cat_id = cat_id;
        this.cat_name = cat_name;
        this.flag = flag;
        this.icon_img = icon_img;
        this.img = img;
        this.deliverycharge = deliverycharge;
        this.isSelected = isSelected;
    }

    public String getDeliverycharge() {
        return deliverycharge;
    }

    public void setDeliverycharge(String deliverycharge) {
        this.deliverycharge = deliverycharge;
    }

    public CategoryPojo(String type) {
        this.type = type;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getIcon_img() {
        return icon_img;
    }

    public void setIcon_img(String icon_img) {
        this.icon_img = icon_img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
