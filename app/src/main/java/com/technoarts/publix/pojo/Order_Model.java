package com.technoarts.publix.pojo;

/**
 * Created by NIDHI on 11/2/2018.
 */

public class Order_Model {

    String order_no;
    String order_date;
    String total_amount;
    String order_id;
    String order_status;
    String mobile_no;

    public Order_Model(String order_no, String order_date, String total_amount, String order_id, String order_status,String  mobile_no) {
        this.order_no = order_no;
        this.order_date = order_date;
        this.total_amount = total_amount;
        this.order_id=order_id;
        this.mobile_no=mobile_no;
        this.order_status=order_status;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }
}
