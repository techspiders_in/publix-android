package com.technoarts.publix.pojo;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("all")
public class AreaModel {

    @SerializedName("id")
    private final String id;

    @SerializedName("area")
    private final String area;

    @SerializedName("rate")
    private final String rate;

    @SerializedName("expcharge")
    private final String expcharge;

    public AreaModel(String id, String area, String rate, String expcharge) {
        this.id = id;
        this.area = area;
        this.rate = rate;
        this.expcharge = expcharge;
    }


    public String getId() {
        return id;
    }

    public String getArea() {
        return area;
    }

    public String getRate() {
        return rate;
    }

    public String getExpcharge() {
        return expcharge;
    }

    @Override
    public String toString() {
        return "AreaModel{" +
                "id='" + id + '\'' +
                ", area='" + area + '\'' +
                ", rate='" + rate + '\'' +
                ", expcharge='" + expcharge + '\'' +
                '}';
    }
}
