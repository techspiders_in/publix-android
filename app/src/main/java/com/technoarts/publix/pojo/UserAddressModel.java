package com.technoarts.publix.pojo;

import com.google.gson.annotations.SerializedName;

public class UserAddressModel {

    @SerializedName("address")
    private String address;

    @SerializedName("landmark")
    private String landmark;
    @SerializedName("mobile_no")
    private String mobile_no;

    public UserAddressModel(String address, String landmark, String mobile_no) {
        this.address = address;
        this.landmark = landmark;
        this.mobile_no = mobile_no;
    }

    public String getAddress() {
        return address == null ? "" : address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandmark() {
        return landmark == null ? "" : landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getMobile_no() {
        return mobile_no == null ? "" : mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }
}
