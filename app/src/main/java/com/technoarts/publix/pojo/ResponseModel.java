package com.technoarts.publix.pojo;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("all")
public class ResponseModel {

    @SerializedName("result")
    private final String result;

    @SerializedName("user_name")
    private final String userName;

    @SerializedName("user_email")
    private final String userEmail;

    @SerializedName("user_id")
    private final String userId;

    public ResponseModel(String result, String userName, String userEmail, String userId) {
        this.result = result;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userId = userId;
    }

    public String getResult() {
        return result;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "ResponseModel{" +
                "result='" + result + '\'' +
                ", userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
