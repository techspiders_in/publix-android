package com.technoarts.publix.pojo;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("all")
public class RegModel {
    public RegModel() {

    }
    @SerializedName("result")
    private String result;

    @SerializedName("message")
    private  String message;

    @SerializedName("user_id")
    private  int userId;

    public RegModel(String result, String message, int userId) {
        this.result = result;
        this.message = message;
        this.userId = userId;
    }



    public String getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public int getUserId() {
        return userId;
    }

}
