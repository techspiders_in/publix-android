package com.technoarts.publix.pojo;

public class DeliveryChargesPojo {
    String id,charges;

    public DeliveryChargesPojo(String id, String charges) {
        this.id = id;
        this.charges = charges;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }
}
