package com.technoarts.publix.pojo;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("all")
public class BakeryMolel {
    @SerializedName("pro_id")
    private final String proId;

    @SerializedName("cat_id")
    private final String catId;

    @SerializedName("cat_name")
    private final String catName;

    @SerializedName("pro_name")
    private final String proName;

    @SerializedName("pro_cat_id")
    private final String proCatId;

    @SerializedName("pro_desc")
    private final String proDesc;

    @SerializedName("pro_sale")
    private final String proSale;

    @SerializedName("pro_mrp_ammount")
    private final String proMrpAmmount;

    @SerializedName("pro_size")
    private final String proSize;

    @SerializedName("pro_status")
    private final String proStatus;

    @SerializedName("date")
    private final String date;

    @SerializedName("pro_img")
    private final String proImg;

    @SerializedName("name&price")
    private final String nameprice;

    public BakeryMolel(String proId, String catId, String catName, String proName, String proCatId,
                       String proDesc, String proSale, String proMrpAmmount, String proSize, String proStatus,
                       String date, String proImg, String nameprice) {
        this.proId = proId;
        this.catId = catId;
        this.catName = catName;
        this.proName = proName;
        this.proCatId = proCatId;
        this.proDesc = proDesc;
        this.proSale = proSale;
        this.proMrpAmmount = proMrpAmmount;
        this.proSize = proSize;
        this.proStatus = proStatus;
        this.date = date;
        this.proImg = proImg;
        this.nameprice = nameprice;
    }

    public String getProId() {
        return proId;
    }

    public String getCatId() {
        return catId;
    }

    public String getCatName() {
        return catName;
    }

    public String getProName() {
        return proName;
    }

    public String getProCatId() {
        return proCatId;
    }

    public String getProDesc() {
        return proDesc;
    }

    public String getProSale() {
        return proSale;
    }

    public String getProMrpAmmount() {
        return proMrpAmmount;
    }

    public String getProSize() {
        return proSize;
    }

    public String getProStatus() {
        return proStatus;
    }

    public String getDate() {
        return date;
    }

    public String getProImg() {
        return proImg;
    }

    public String getNameprice() {
        return nameprice;
    }
}
