package com.technoarts.publix.pojo;

public class MiniOrderItem {
    private String charge;

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }
}
