package com.technoarts.publix.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PojoDaily implements Serializable {
    @SerializedName("pro_id")
    String pro_id;
    @SerializedName("cat_id")
    String cat_id;
    @SerializedName("cat_name")
    String cat_name;
    @SerializedName("pro_name")
    String pro_name;
    @SerializedName("pro_cat_id")
    String pro_cat_id;
    @SerializedName("pro_desc")
    String pro_desc;
    @SerializedName("pro_sale")
    String pro_sale;
    @SerializedName("pro_mrp_ammount")
    String pro_mrp_ammount;
    @SerializedName("pro_size")
    String pro_size;
    @SerializedName("pro_status")
    String pro_status;
    @SerializedName("date")
    String date;
    @SerializedName("pro_img")
    String pro_img;
    @SerializedName("name&price")
    String name_price;

    public PojoDaily(String pro_id, String cat_id, String cat_name, String pro_name,
                     String pro_cat_id, String pro_desc, String pro_sale, String pro_mrp_ammount,
                     String pro_size, String pro_status, String date, String pro_img,
                     String name_price) {
        this.pro_id = pro_id;
        this.cat_id = cat_id;
        this.cat_name = cat_name;
        this.pro_name = pro_name;
        this.pro_cat_id = pro_cat_id;
        this.pro_desc = pro_desc;
        this.pro_sale = pro_sale;
        this.pro_mrp_ammount = pro_mrp_ammount;
        this.pro_size = pro_size;
        this.pro_status = pro_status;
        this.date = date;
        this.pro_img = pro_img;
        this.name_price = name_price;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_cat_id() {
        return pro_cat_id;
    }

    public void setPro_cat_id(String pro_cat_id) {
        this.pro_cat_id = pro_cat_id;
    }

    public String getPro_desc() {
        return pro_desc;
    }

    public void setPro_desc(String pro_desc) {
        this.pro_desc = pro_desc;
    }

    public String getPro_sale() {
        return pro_sale;
    }

    public void setPro_sale(String pro_sale) {
        this.pro_sale = pro_sale;
    }

    public String getPro_mrp_ammount() {
        return pro_mrp_ammount;
    }

    public void setPro_mrp_ammount(String pro_mrp_ammount) {
        this.pro_mrp_ammount = pro_mrp_ammount;
    }

    public String getPro_size() {
        return pro_size;
    }

    public void setPro_size(String pro_size) {
        this.pro_size = pro_size;
    }

    public String getPro_status() {
        return pro_status;
    }

    public void setPro_status(String pro_status) {
        this.pro_status = pro_status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPro_img() {
        return pro_img;
    }

    public void setPro_img(String pro_img) {
        this.pro_img = pro_img;
    }

    public String getName_price() {
        return name_price;
    }

    public void setName_price(String name_price) {
        this.name_price = name_price;
    }
}
