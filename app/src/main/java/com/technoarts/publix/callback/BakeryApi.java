package com.technoarts.publix.callback;

import com.technoarts.publix.pojo.PojoDaily;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface BakeryApi {
    String BASE_URL="http://thepublix.com/tpxadmin/";
    @GET("Ballery/json")
    Call<List<ProductPojo>> getBannerss();

    @GET("https://thepublix.com/tpxadmin/Ballery/json")
    Call<List<PojoDaily>> getBanners();
}
