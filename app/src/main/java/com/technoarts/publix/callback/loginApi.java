package com.technoarts.publix.callback;

import com.google.gson.JsonObject;
import com.technoarts.publix.pojo.ResponseModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface loginApi {

    String BASE_URL="https://thepublix.com/tpxadmin/";
//    @FormUrlEncoded
    @GET("Users/publix_register")
    Call<ResponseModel> doRegistration(@Query("device_type") String device_type,
                                       @Query("fullname") String fullname,
                                       @Query("email") String email,
                                       @Query("password ") String password );

    @POST("Users/publix_register")
    Call<JsonObject> getUser(@Body Map<String, Object> body);
    @POST("Users/publix_login")
    Call<JsonObject> logion(@Body Map<String, Object> body);
}
