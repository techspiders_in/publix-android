package com.technoarts.publix.callback;


import com.technoarts.publix.pojo.ProductPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by sab99r
 */
public interface DealApi {
    @GET("https://thepublix.com/tpxadmin/Deal_offers/json")
    Call<List<ProductPojo>> getMovies(@Query("index") int index);
}