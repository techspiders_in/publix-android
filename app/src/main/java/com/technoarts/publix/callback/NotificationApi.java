package com.technoarts.publix.callback;



import com.technoarts.publix.helpers.NotificationVO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by sab99r
 */
public interface NotificationApi {
    @GET("https://thepublix.com/tpxadmin/Notification1/notilist")
    Call<List<NotificationVO>> getMovies(@Query("index") int index);
}