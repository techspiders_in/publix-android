package com.technoarts.publix.callback;

import com.technoarts.publix.pojo.PojoDaily;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface getDaily {

    @GET("https://thepublix.com/tpxadmin/dely/json")
    Call<List<PojoDaily>> getBanners();

    String BASE_URL="https://thepublix.com/tpxadmin/";

    @GET("dely/json")
    Call<List<ProductPojo>> getBannerss();

}