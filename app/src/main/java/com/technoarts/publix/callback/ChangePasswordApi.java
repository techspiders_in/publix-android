package com.technoarts.publix.callback;

import com.technoarts.publix.pojo.GenericResponseModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by sab99r
 */
public interface ChangePasswordApi {

    @POST("https://thepublix.com/tpxadmin/Users/change_password")
    Call<GenericResponseModel> changePassword(@Body Map<String, String> body);
}