package com.technoarts.publix.callback;


import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;

public interface onProductLoadComplete {
    void onLoadComplete(ArrayList<ProductPojo> productList);
}
