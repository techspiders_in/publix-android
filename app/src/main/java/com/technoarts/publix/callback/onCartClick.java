package com.technoarts.publix.callback;

public interface onCartClick {
    void click(int position);
    void onClickPlus(int position);
    void onClickMinus(int position);
    void onClickDelete(int position);
    void onCouponDelete(int position);
    void onName1(int position);
    void onName2(int position);
    void onName3(int position);
}
