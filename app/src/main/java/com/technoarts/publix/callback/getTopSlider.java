package com.technoarts.publix.callback;


import com.technoarts.publix.pojo.BannerModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface getTopSlider {

    @GET("https://thepublix.com/tpxadmin/Slider/json")
    Call<List<BannerModel>> getBanners();

}