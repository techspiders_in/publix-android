package com.technoarts.publix.callback;


import com.technoarts.publix.pojo.CategoryPojo;

import java.util.ArrayList;

public interface onCategoryLoadComplete {
    void onComplete(ArrayList<CategoryPojo> categoryList);
}
