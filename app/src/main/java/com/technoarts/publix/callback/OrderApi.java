package com.technoarts.publix.callback;

import com.technoarts.publix.pojo.MiniOrderItem;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface OrderApi {

    @POST("https://thepublix.com/tpxadmin/Min_charge/min_charge_price")
    Call<ArrayList<MiniOrderItem>> getMiniOrderAmount(@Body Map<String, String> body);
}