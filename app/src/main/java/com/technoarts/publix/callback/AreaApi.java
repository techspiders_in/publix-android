package com.technoarts.publix.callback;

import com.technoarts.publix.pojo.AreaModel;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AreaApi {
    String BASE_URL="https://thepublix.com/tpxadmin/";
    @GET("Deliveryarea/all_list")
    Call<List<AreaModel>> getAreaList1();

    @FormUrlEncoded
    @POST("Deliveryarea/json")
    void login(@Field("area") String area);

    @POST("Deliveryarea/json")
    Call<List<AreaModel>> getAreaList(@Body Map<String, Object> body);
}
