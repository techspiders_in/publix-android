package com.technoarts.publix.callback;


import com.technoarts.publix.pojo.GenericResponseModel;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by sab99r
 */
public interface RelatedApi {
    @POST("https://thepublix.com/tpxadmin/Products/relatedproduct")
    Call<List<ProductPojo>> getMovies(
            @Body Map<String, String> body);

    @POST("https://thepublix.com/tpxadmin/Products/addFavourite")
    Call<GenericResponseModel> addToFav(
            @Body Map<String, Object> body);

    @POST("https://thepublix.com/tpxadmin/Products/removeFavorite")
    Call<GenericResponseModel> removeToFav(
            @Body Map<String, Object> body);

    @POST("https://thepublix.com/tpxadmin/Products/getFavourite")
    Call<List<ProductPojo>> getFavList(@Body() HashMap<String,Object> param);

    @POST("https://thepublix.com/tpxadmin/Products/productdetail1")
    Call<List<ProductPojo>> getSingleProductInfo(@Body() HashMap<String,Object> param);
}