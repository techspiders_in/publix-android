package com.technoarts.publix.callback;

import com.technoarts.publix.pojo.UserAddressModel;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by sab99r
 */
public interface AddressAPI {

    @POST("https://thepublix.com/tpxadmin/Orders/getaddress")
    Call<ArrayList<UserAddressModel>> getUserAddress(@Body HashMap<String,String> param);
}