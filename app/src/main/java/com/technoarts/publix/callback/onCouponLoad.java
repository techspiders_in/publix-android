package com.technoarts.publix.callback;


import com.technoarts.publix.pojo.CouponPojo;

import java.util.ArrayList;

public interface onCouponLoad {
    void onLoadComplete(ArrayList<CouponPojo> coupon);
}
