package com.technoarts.publix.callback;

import com.technoarts.publix.pojo.ProductPojo;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by sab99r
 */
public interface ProductApi {
//    @GET("https://thepublix.com/tpxadmin/Products/product_bycategory")
//    Call<List<ProductPojo>> getMovies(@Query("index") int index, @Query("cat_id") String  ctegoryId);

    @POST("https://thepublix.com/tpxadmin/Products/product_bycategory")
    Call<List<ProductPojo>> getMovies(@Body RequestBody body);
    String BASE_URL = "https://thepublix.com/tpxadmin/";

    @GET("Products/json/")
    Call<List<ProductPojo>> getProducts();
}