package com.technoarts.publix.callback;


import com.technoarts.publix.pojo.CategoryPojo;
import com.technoarts.publix.pojo.ResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by sab99r
 */
public interface MoviesApi {
    @GET("https://thepublix.com/tpxadmin/Maincategory/cat_json")
    Call<List<CategoryPojo>> getMovies();

//    @POST("https://thepublix.com/tpxadmin/Users/publix_login")
//    Call<List<CategoryPojo>> getLogin(@Field("user_email") String email,
//                                      @Field("password")String password);
    @FormUrlEncoded
    @POST("https://thepublix.com/tpxadmin/Users/publix_login")
    Call<List<ResponseModel>> getLogin(@Field("user_email") String user_email, @Field("password") String password);
}