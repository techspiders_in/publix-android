package com.technoarts.publix.callback;

import com.technoarts.publix.activity.CheckoutAct;
import com.technoarts.publix.pojo.GenericResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by sab99r
 */
public interface BookOrderAPI {

    @POST("https://thepublix.com/tpxadmin/Orders/checkout")
    Call<GenericResponseModel> bookOrder(@Body CheckoutAct.CartAPIItem body);
}