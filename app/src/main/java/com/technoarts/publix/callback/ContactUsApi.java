package com.technoarts.publix.callback;

import com.google.gson.JsonObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ContactUsApi {

    @POST("https://thepublix.com/tpxadmin/Contact/contactUs")
    Call<JsonObject> shareContactUsDetail(@Body Map<String, String> body);
}