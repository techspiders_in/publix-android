package com.technoarts.publix.callback;

import com.technoarts.publix.pojo.ResponseModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by sab99r
 */
public interface FCMTokenApi {

    @POST("https://thepublix.com/tpxadmin/Notification1/token")
    Call<ResponseModel> doRegistrationToken(@Body Map<String, String> body);
}