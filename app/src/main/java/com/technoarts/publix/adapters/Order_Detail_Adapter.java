package com.technoarts.publix.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.loopj.android.image.SmartImageView;
import com.technoarts.publix.R;
import com.technoarts.publix.pojo.Orderlist;

import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by NIDHI on 11/5/2018.
 */

public class Order_Detail_Adapter extends BaseAdapter {
    Double totalamt;
    public static String order_id;
    public static CircleImageView circleImageView;
    String Path;
    List<String> imageList;
    String url="http://monalisastores.com/ashokasta/askadmin/orders/order_details";
    public static List<Orderlist> Acatlist;

    Context context;

    public Order_Detail_Adapter(List<Orderlist> catlist, Context context) {
        this.Acatlist = catlist;
        this.context = context;
    }
    @Override
    public int getCount() {
        return Acatlist.size();
    }
    @Override
    public Object getItem(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    public int getItemCount() {
        return Acatlist.size();
    }
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View listviewitem =inflater.inflate(R.layout.orderlist,null,true);


        SmartImageView image=listviewitem.findViewById(R.id.proimage);
        TextView name = (TextView) listviewitem.findViewById(R.id.proname);
        TextView qty = (TextView) listviewitem.findViewById(R.id.proqty);
        final TextView amt = (TextView) listviewitem.findViewById(R.id.proamount);
        Orderlist curCategory = Acatlist.get(position);

//        totalamt= Double.valueOf(curCategory.getTotal_amount().toString());
        String img=curCategory.getPro_img();
        Log.e("image",img);
//        Path = connect.path1 + "assets/admin/category/";
        imageList = Arrays.asList( curCategory.getPro_img().split("\\s*,\\s*"));

        if (imageList.size() > 0) {
//            Glide.with(context).load(imageList.get(0)).into(image);
            Glide.with(context)
                    .applyDefaultRequestOptions(new RequestOptions()
                            .placeholder(R.drawable.launcher)
                            .error(R.drawable.launcher))
                    .load(imageList.get(0)).into(image);

        }

        name.setText(curCategory.getPro_name());
        qty.setText(curCategory.getQty());
        amt.setText(curCategory.getPro_amount());





        return listviewitem;
    }


}
