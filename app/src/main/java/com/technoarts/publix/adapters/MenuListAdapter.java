package com.technoarts.publix.adapters;


import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.technoarts.publix.R;
import com.technoarts.publix.callback.onClickMenuItem;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.DrawerPojo;

import java.util.ArrayList;

public class MenuListAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<DrawerPojo> data;
    int sdk = android.os.Build.VERSION.SDK_INT;
    int selectedPostition = -1;
    onClickMenuItem click;

    public MenuListAdapter(Context context, ArrayList<DrawerPojo> data, onClickMenuItem click) {
        this.context = context;
        this.data = data;
        this.click = click;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.menu_list_adapter, parent, false);
        return new GalleryItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final GalleryItemHolder subjectHolder = (GalleryItemHolder) holder;
        new SizeUtil(context).setLinearLayoutMatchWidth(subjectHolder.mainCard, 120, 30, 10, 10, 0, Gravity.CENTER);
        new SizeUtil(context).setLinearLayout(subjectHolder.navigationThumb, 60, 60, 0, 40, 40, 0, Gravity.CENTER);


        subjectHolder.navigationThumb.setImageResource(data.get(position).getResourceIcon());
        subjectHolder.navigationTitle.setText(data.get(position).getName());


        if (data.get(position).isSelected()) {
            selectedPostition = position;
            subjectHolder.navigationThumb.setColorFilter(ContextCompat.getColor(context, R.color.white),
                    android.graphics.PorterDuff.Mode.SRC_IN);
            subjectHolder.navigationTitle.setTextColor(context.getResources().getColor(R.color.white));


            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                subjectHolder.mainCard.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.rounded_trans));
            } else {
                subjectHolder.mainCard.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_trans));
            }
        } else {
            subjectHolder.navigationThumb.setColorFilter(ContextCompat.getColor(context, R.color.light_black), android.graphics.PorterDuff.Mode.SRC_IN);
            subjectHolder.navigationTitle.setTextColor(context.getResources().getColor(R.color.light_black));
            subjectHolder.mainCard.setBackground(null);
        }

        subjectHolder.mainCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.get(selectedPostition).setSelected(false);
                data.get(position).setSelected(true);
                selectedPostition = position;
                click.onClick(position);
                notifyDataSetChanged();
            }
        });


    }

    public void setSelected(int position)
    {
        data.get(selectedPostition).setSelected(false);
        data.get(position).setSelected(true);
        selectedPostition = position;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        TextView navigationTitle;
        ImageView navigationThumb;
        LinearLayout mainCard;

        public GalleryItemHolder(View itemView) {
            super(itemView);
            mainCard = itemView.findViewById(R.id.mainCard);
            navigationTitle = itemView.findViewById(R.id.navigationTitle);
            navigationThumb = itemView.findViewById(R.id.navigationThumb);
        }
    }

}