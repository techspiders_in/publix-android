package com.technoarts.publix.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.technoarts.publix.pojo.DeliveryAreaPojo;
import java.util.ArrayList;

public class SpinAdapter extends ArrayAdapter<DeliveryAreaPojo> {

    private Context context;
    private ArrayList<DeliveryAreaPojo> values;

    public SpinAdapter(Context context, int textViewResourceId,
                       ArrayList<DeliveryAreaPojo> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public DeliveryAreaPojo getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setText(values.get(position).getAreaname());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setText(values.get(position).getAreaname());
        return label;
    }
}