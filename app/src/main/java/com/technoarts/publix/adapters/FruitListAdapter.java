package com.technoarts.publix.adapters;


import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;

public class FruitListAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<ProductPojo> data;
    onProductClick click;

    public FruitListAdapter(Context context, ArrayList<ProductPojo> data, onProductClick click) {
        this.context = context;
        this.data = data;
        this.click = click;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.fruit_list_adapter, parent, false);
        return new GalleryItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final GalleryItemHolder subjectHolder = (GalleryItemHolder) holder;
        new SizeUtil(context).setLinearLayout(subjectHolder.mainCard, 450, 380, 20, 20, 20, 0, Gravity.CENTER);
        new SizeUtil(context).setLinearLayout(subjectHolder.fruitImage, 325, 190, 0, 10, 10, 0, Gravity.CENTER);

        subjectHolder.fruitNameTv.setText(data.get(position).getPro_name());
        subjectHolder.tvpublixPrice.setText("₹ "+data.get(position).getPrice1());
        subjectHolder.tvMrp.setText("₹ "+data.get(position).getPublix_price1());

//        subjectHolder.tvpublixPrice.setPaintFlags(subjectHolder.tvpublixPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        subjectHolder.tvpublixPrice.setPaintFlags(subjectHolder.tvMrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        subjectHolder.mainCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click.click(position);
            }
        });

        Glide.with(context).load(data.get(position).getPro_img()).into(subjectHolder.fruitImage);

        if (TextUtils.isEmpty(data.get(position).getPro_name())) {
            subjectHolder.fruitNameTv.setVisibility(View.GONE);
        } else {
            subjectHolder.fruitNameTv.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(data.get(position).getPrice1())) {
            subjectHolder.linearmrp.setVisibility(View.GONE);
        } else {
            subjectHolder.linearmrp.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(data.get(position).getPublix_price1())) {
            subjectHolder.linearprice.setVisibility(View.GONE);
        } else {
            subjectHolder.linearprice.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        TextView fruitNameTv, tvpublixPrice, tvMrp;
        CardView mainCard;
        ImageView fruitImage;
        LinearLayout linearprice,linearmrp;

        public GalleryItemHolder(View itemView) {
            super(itemView);
            mainCard = itemView.findViewById(R.id.mainCard);
            fruitNameTv = itemView.findViewById(R.id.fruitNameTv);
            tvpublixPrice = itemView.findViewById(R.id.tvpublixPrice);
            tvMrp = itemView.findViewById(R.id.tvMrp);
            fruitImage = itemView.findViewById(R.id.fruitImage);
            linearprice = itemView.findViewById(R.id.linearprice);
            linearmrp = itemView.findViewById(R.id.linearmrp);

            tvpublixPrice.setPaintFlags(tvpublixPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }
    }

}