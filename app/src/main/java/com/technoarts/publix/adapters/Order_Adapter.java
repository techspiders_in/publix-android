package com.technoarts.publix.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.technoarts.publix.R;
import com.technoarts.publix.activity.OrderDetails;
import com.technoarts.publix.pojo.Order_Model;

import java.util.List;

/**
 * Created by NIDHI on 11/2/2018.
 */

public class Order_Adapter extends BaseAdapter {

    public static List<Order_Model> Acatlist;
    Context context;

    public Order_Adapter(List<Order_Model> catlist, Context context) {
        this.Acatlist = catlist;
        this.context = context;
    }

    @Override
    public int getCount() {
        return Acatlist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public int getItemCount() {
        return Acatlist.size();
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View listviewitem = inflater.inflate(R.layout.datewise, null, true);

        TextView orderno = (TextView) listviewitem.findViewById(R.id.orderno);
        final TextView orderdate = (TextView) listviewitem.findViewById(R.id.orderdate);
        TextView name = (TextView) listviewitem.findViewById(R.id.name);
        TextView view = (TextView) listviewitem.findViewById(R.id.view);
        TextView status = (TextView) listviewitem.findViewById(R.id.status);
        Order_Model curCategory = Acatlist.get(position);

//        totalamt= Double.valueOf(curCategory.getTotal_amount().toString());
        orderno.setText(curCategory.getOrder_no());
        orderdate.setText(curCategory.getOrder_date());
        name.setText(curCategory.getTotal_amount());
        status.setText(curCategory.getOrder_status());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, OrderDetails.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                final String order_id = Acatlist.get(position).getOrder_id();
                Log.e("orderid", order_id);
                i.putExtra("order_id", order_id);
                context.startActivity(i);

            }
        });

        return listviewitem;
    }


    public List<Order_Model> getCartList() {
        return this.Acatlist;
    }


}
