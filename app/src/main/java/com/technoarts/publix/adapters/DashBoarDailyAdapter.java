package com.technoarts.publix.adapters;


import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technoarts.publix.R;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;

public class DashBoarDailyAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<ProductPojo> data;
    onClicFilter click;

    public DashBoarDailyAdapter(Context context, ArrayList<ProductPojo> data, onClicFilter click) {
        this.context = context;
        this.data = data;
        this.click = click;
    }

    public interface onClicFilter {
        void onClick(int pos);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.dashboard_daily_adapter, parent, false);
        return new GalleryItemHolder(row);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        GalleryItemHolder hol = (GalleryItemHolder) holder;

        new SizeUtil(context).setLinearLayout(hol.mainCard, 510, 600, 10, 10, 10, 0, Gravity.CENTER);
        new SizeUtil(context).setLinearLayout(hol.productImage, 510, 400, 0, 0, 0, 0, Gravity.CENTER);

        hol.productNameTv.setText(data.get(position).getPro_name());
        hol.tvpublixPrice.setText("₹ "+data.get(position).getPrice1());
        hol.tvMrp.setText("₹ "+data.get(position).getPublix_price1());
        hol.tvpublixPrice.setPaintFlags(hol.tvpublixPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        Glide.with(context).load(data.get(position).getPro_img().split(",")[0]).into(hol.productImage);
        if (TextUtils.isEmpty(data.get(position).getPro_name())) {
            hol.productNameTv.setVisibility(View.GONE);
        } else {
            hol.productNameTv.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(data.get(position).getPrice1())) {
            hol.linearmrp.setVisibility(View.GONE);
        } else {
            hol.linearmrp.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(data.get(position).getPublix_price1())) {
            hol.linearprice.setVisibility(View.GONE);
        } else {
            hol.linearprice.setVisibility(View.VISIBLE);
        }
        hol.mainCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        TextView productNameTv,tvpublixPrice, tvMrp;;
        ImageView productImage;
        CardView mainCard;
        LinearLayout linearprice,linearmrp;
        public GalleryItemHolder(View itemView) {
            super(itemView);
            productNameTv = itemView.findViewById(R.id.productNameTv);
            productImage = itemView.findViewById(R.id.productImage);
            mainCard = itemView.findViewById(R.id.mainCard);
            tvMrp = itemView.findViewById(R.id.tvMrp);
            tvpublixPrice = itemView.findViewById(R.id.tvpublixPrice);
            linearprice = itemView.findViewById(R.id.linearprice);
            linearmrp = itemView.findViewById(R.id.linearmrp);

        }
    }

}