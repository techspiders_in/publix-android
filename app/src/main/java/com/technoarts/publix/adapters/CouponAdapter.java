package com.technoarts.publix.adapters;


import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.technoarts.publix.R;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.CouponPojo;

import java.util.ArrayList;

public class CouponAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<CouponPojo> data;
    onClickCoupon click;
    boolean isFromDrawer;

    public CouponAdapter(Context context, ArrayList<CouponPojo> data, boolean isFromDrawer, onClickCoupon click) {
        this.context = context;
        this.data = data;
        this.isFromDrawer = isFromDrawer;
        this.click = click;
    }

    public interface onClickCoupon {
        void onClick(int position);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.coupon_adapter, parent, false);
        return new GalleryItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final GalleryItemHolder subjectHolder = (GalleryItemHolder) holder;
        new SizeUtil(context).setLinearLayoutMatchWidth(subjectHolder.mainCard, 440, 10, 20, 20, 0, Gravity.CENTER);
        new SizeUtil(context).setLinearLayoutMatchWidth(subjectHolder.applyButton, 150, 10, 0, 0, 0, Gravity.CENTER);

        if (isFromDrawer) {
            subjectHolder.applyButton.setVisibility(View.GONE);
        } else {
            subjectHolder.applyButton.setVisibility(View.VISIBLE);
        }
        subjectHolder.navigationTitle.setText(data.get(position).getCp_code());
        subjectHolder.expireTv.setText(data.get(position).getCp_date());
        subjectHolder.schemeTv.setText("get ₹" + data.get(position).getCp_price() + " off on every ₹" + data.get(position).getCp_min_price() + " & above");
        subjectHolder.applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.onClick(position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        TextView navigationTitle, expireTv, schemeTv;
        CardView mainCard;
        LinearLayout applyButton;

        public GalleryItemHolder(View itemView) {
            super(itemView);
            mainCard = itemView.findViewById(R.id.mainCard);
            navigationTitle = itemView.findViewById(R.id.navigationTitle);
            applyButton = itemView.findViewById(R.id.applyButton);
            expireTv = itemView.findViewById(R.id.expireTv);
            schemeTv = itemView.findViewById(R.id.schemeTv);
        }
    }

}