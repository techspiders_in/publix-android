package com.technoarts.publix.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.technoarts.publix.R;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class BakeryAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<ProductPojo> data;
    onClicFilter click;

    public BakeryAdapter(Context context, ArrayList<ProductPojo> data, onClicFilter click) {
        this.context = context;
        this.data = data;
        this.click = click;
    }
    public interface onClicFilter {
        void onClick(int pos);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.bakery_row_itemm, parent, false);
        return new GalleryItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        position = 0;
        final GalleryItemHolder subjectHolder = (GalleryItemHolder) holder;
        String pro1 = data.get(position).getPro_img();
        String pro2 = data.get(position + 1).getPro_img();
        String pro3 = data.get(position + 2).getPro_img();

        Glide.with(context).load(pro1).into(subjectHolder.specialProductImage1);
        Glide.with(context).load(pro2).into(subjectHolder.specialProductImage2);
        Glide.with(context).load(pro3).into(subjectHolder.specialProductImage3);

        String name1 = data.get(position).getPro_name();
        String name2 = data.get(position + 1).getPro_name();
        String name3 = data.get(position + 2).getPro_name();
        subjectHolder.tvproductname1.setText(name1);
        subjectHolder.tvproductname2.setText(name2);
        subjectHolder.tvproductname3.setText(name3);

//        subjectHolder.tvmrp1.setText("₹ " + data.get(0).getPublix_price1());
//        subjectHolder.tvmrp2.setText("₹ " + data.get(1).getPublix_price1());
//        subjectHolder.tvmrp3.setText("₹ " + data.get(2).getPublix_price1());
//
//        subjectHolder.tvpublixPrice1.setText("₹ " + data.get(0).getPrice1());
//        subjectHolder.tvpublixPrice2.setText("₹ " + data.get(1).getPrice1());
//        subjectHolder.tvpublixPrice3.setText("₹ " + data.get(2).getPrice1());

        subjectHolder.tvpublixPrice1.setPaintFlags(subjectHolder.tvpublixPrice1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        subjectHolder.tvpublixPrice2.setPaintFlags(subjectHolder.tvpublixPrice2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        subjectHolder.tvpublixPrice3.setPaintFlags(subjectHolder.tvpublixPrice3.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


//        if (TextUtils.isEmpty(data.get(0).getPro_name())) {
//            subjectHolder. tvproductname1.setVisibility(View.GONE);
//        } else {
//            subjectHolder.tvproductname1.setVisibility(View.VISIBLE);
//        }
//        if (TextUtils.isEmpty(data.get(1).getPro_name())) {
//            subjectHolder.tvproductname2.setVisibility(View.GONE);
//        } else {
//            subjectHolder.tvproductname2.setVisibility(View.VISIBLE);
//        }
//        if (TextUtils.isEmpty(data.get(2).getPro_name())) {
//            subjectHolder.tvproductname3.setVisibility(View.GONE);
//        } else {
//            subjectHolder.tvproductname3.setVisibility(View.VISIBLE);
//        }
//
//        //Mrp amount
//        if (TextUtils.isEmpty(data.get(0).getPrice1())) {
//            subjectHolder.linearmrp1.setVisibility(View.GONE);
//        } else {
//            subjectHolder.linearmrp1.setVisibility(View.VISIBLE);
//        }
//        if (TextUtils.isEmpty(data.get(1).getPrice1())) {
//            subjectHolder.linearmrp2.setVisibility(View.GONE);
//        } else {
//            subjectHolder. linearmrp2.setVisibility(View.VISIBLE);
//        }
//        if (TextUtils.isEmpty(data.get(2).getPrice1())) {
//            subjectHolder. linearmrp3.setVisibility(View.GONE);
//        } else {
//            subjectHolder. linearmrp3.setVisibility(View.VISIBLE);
//        }
//        //publix price
//        if (TextUtils.isEmpty(data.get(0).getPublix_price1())) {
//            subjectHolder. linearprice1.setVisibility(View.GONE);
//        } else {
//            subjectHolder. linearprice1.setVisibility(View.VISIBLE);
//        }
//        if (TextUtils.isEmpty(data.get(1).getPublix_price1())) {
//            subjectHolder. linearprice2.setVisibility(View.GONE);
//        } else {
//            subjectHolder.linearprice2.setVisibility(View.VISIBLE);
//        }
//        if (TextUtils.isEmpty(data.get(2).getPublix_price1())) {
//            subjectHolder. linearprice3.setVisibility(View.GONE);
//        } else {
//            subjectHolder.  linearprice3.setVisibility(View.VISIBLE);
//        }
        final int finalPosition = position;
        subjectHolder.mainCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.onClick(finalPosition);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        TextView tvproductname1, tvproductname2, tvproductname3,
                tvmrp1, tvmrp2, tvmrp3, tvpublixPrice1, tvpublixPrice2, tvpublixPrice3;
        CardView spCard1, spCard2, spCard3;
        ImageView specialProductImage1, specialProductImage2, specialProductImage3;
        LinearLayout linearprice1, linearprice2, linearprice3,mainCard, linearmrp1, linearmrp2, linearmrp3;

        public GalleryItemHolder(View itemView) {
            super(itemView);
            mainCard = itemView.findViewById(R.id.mainCard);
            spCard1 = itemView.findViewById(R.id.spCard1);
            spCard2 = itemView.findViewById(R.id.spCard2);
            spCard3 = itemView.findViewById(R.id.spCard3);
            linearprice1 = itemView.findViewById(R.id.linearprice1);
            linearprice2 = itemView.findViewById(R.id.linearprice2);
            linearprice3 = itemView.findViewById(R.id.linearprice3);

            specialProductImage1 = itemView.findViewById(R.id.specialProductImage1);
            specialProductImage2 = itemView.findViewById(R.id.specialProductImage2);
            specialProductImage3 = itemView.findViewById(R.id.specialProductImage3);

            tvproductname1 = itemView.findViewById(R.id.tvproductname1);
            tvproductname2 = itemView.findViewById(R.id.tvproductname2);
            tvproductname3 = itemView.findViewById(R.id.tvproductname3);

            tvmrp1 = itemView.findViewById(R.id.tvmrp1);
            tvmrp2 = itemView.findViewById(R.id.tvMrp2);
            tvmrp3 = itemView.findViewById(R.id.tvMrp3);

            tvpublixPrice1 = itemView.findViewById(R.id.tvpublixPrice1);
            tvpublixPrice2 = itemView.findViewById(R.id.tvpublixPrice2);
            tvpublixPrice3 = itemView.findViewById(R.id.tvpublixPrice3);

        }
    }

}
