package com.technoarts.publix.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;


import com.technoarts.publix.R;
import com.technoarts.publix.pojo.Slidermodel;

import java.util.List;

/**
 * Created by NIDHI on 10/30/2018.
 */

public class Slideradapter extends BaseAdapter {
    String Path;
    private List<Slidermodel> catlist;
    Context context;
    Button btn;


    public Slideradapter(List<Slidermodel> catlist, LayoutInflater layoutInflater, Context context) {
        this.catlist = catlist;
        this.context = context;
    }


    @Override
    public int getCount() {
        return catlist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View listviewitem =inflater.inflate(R.layout.slider,null,true);

//        final SmartImageView item_img = (SmartImageView) listviewitem.findViewById(R.id.ImageViewStulist);
//        TextView item_name = (TextView) listviewitem.findViewById(R.id.name);

        String img=catlist.get(position).getImg();
        Log.e("image",img);
 //       Path = connect.path1 + "assets/admin/slider/";
//        item_img.setImageUrl(Path+catlist.get(position).getImg());
//
//        item_name.setText(catlist.get(position).getSname());

//        http://yourownstore.co/assets/admin/slider/20181030152307.jpg
        return listviewitem;
    }
}
