package com.technoarts.publix.adapters;


import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.technoarts.publix.R;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class RelatedProductListAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<ProductPojo> data;
    onProductClick click;

    public RelatedProductListAdapter(Context context, ArrayList<ProductPojo> data, onProductClick click) {
        this.context = context;
        this.data = data;
        this.click = click;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.related_product_adapter, parent, false);
        return new GalleryItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final GalleryItemHolder subjectHolder = (GalleryItemHolder) holder;
        new SizeUtil(context).setLinearLayout(subjectHolder.mainCard, 260, 350, 0, 5, 5, 0, Gravity.CENTER);
        new SizeUtil(context).setLinearLayout(subjectHolder.profile_image, 260, 200, 0, 0, 0, 0, Gravity.CENTER);


        subjectHolder.headingTv.setText(data.get(position).getPro_name());
        subjectHolder.description.setText(data.get(position).getPro_desc());
        subjectHolder.priceTv.setText("₹ " + data.get(position).getPro_mrp_ammount());


        Glide.with(context).load(data.get(position).getPro_img()).into(subjectHolder.profile_image);

        subjectHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.click(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        TextView headingTv, description, priceTv;
        LinearLayout mainLayout;
        ImageView profile_image, alphaThumb;
        LinearLayout mainCard;

        public GalleryItemHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            mainCard = itemView.findViewById(R.id.mainCard);
            headingTv = itemView.findViewById(R.id.headingTv);
            profile_image = itemView.findViewById(R.id.profile_image);
            alphaThumb = itemView.findViewById(R.id.alphaThumb);
            description = itemView.findViewById(R.id.description);
            priceTv = itemView.findViewById(R.id.priceTv);

        }
    }

}