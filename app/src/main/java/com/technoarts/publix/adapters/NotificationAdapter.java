package com.technoarts.ashokasta.adapters;


import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.technoarts.publix.R;
import com.technoarts.publix.callback.onProductClick;
import com.technoarts.publix.helpers.NotificationVO;
import com.technoarts.publix.helpers.SizeUtil;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<NotificationVO> data;
    onProductClick click;

    public NotificationAdapter(Context context, ArrayList<NotificationVO> data, onProductClick click) {
        this.context = context;
        this.data = data;
        this.click = click;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.notification_adapter, parent, false);
        return new GalleryItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final GalleryItemHolder subjectHolder = (GalleryItemHolder) holder;
        new SizeUtil(context).setLinearLayout(subjectHolder.mainCard, 1060, 220, 20, 0, 0, 0, Gravity.CENTER);
        new SizeUtil(context).setLinearLayout(subjectHolder.profile_image, 120, 120, 0, 10, 10, 0, Gravity.CENTER);


        subjectHolder.headingTv.setText(data.get(position).getTitle());
        subjectHolder.description.setText(data.get(position).getMessage());


        Glide.with(context).load(data.get(position).getIconUrl()).into(subjectHolder.profile_image);
        subjectHolder.mainCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.click(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        TextView headingTv, description;
        ImageView profile_image;
        CardView mainCard;

        public GalleryItemHolder(View itemView) {
            super(itemView);
            mainCard = itemView.findViewById(R.id.mainCard);
            headingTv = itemView.findViewById(R.id.headingTv);
            profile_image = itemView.findViewById(R.id.profile_image);
            description = itemView.findViewById(R.id.description);

        }
    }

}