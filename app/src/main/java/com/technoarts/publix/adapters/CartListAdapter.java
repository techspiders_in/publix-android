package com.technoarts.publix.adapters;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technoarts.publix.R;
import com.technoarts.publix.activity.CartAct;
import com.technoarts.publix.activity.ProductDetailsAct;
import com.technoarts.publix.callback.onCartClick;
import com.technoarts.publix.dbhelper.DbCart;
import com.technoarts.publix.helpers.Constant;
import com.technoarts.publix.helpers.PrefHelper;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.ArrayList;

public class CartListAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<ProductPojo> data;
    onCartClick click;
    ProductPojo SelectedProduct;
  public   Integer totalAmount=0;
    ProductDetailsAct act;
    String bsFromActivity;
    CartAct CartAct;    // it's Activity
    int price;
    DbCart mDbCart;
    CartListAdapter(CartAct activity) {
        CartAct = activity;
    }
    String ItemName,ItemName1,ItemName2;

    public CartListAdapter(Context context, ArrayList<ProductPojo> data,int  price, onCartClick click) {
        this.context = context;
        this.data = data;
        this.click = click;
        this.price = price;
    }
    public CartListAdapter(Context context, int  price) {
        this.context = context;
        this.price = price;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.cart_adapter, parent, false);
        return new GalleryItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        SelectedProduct = data.get(position);
        final GalleryItemHolder subjectHolder = (GalleryItemHolder) holder;
        new SizeUtil(context).setLinearLayout(subjectHolder.mainCard, 1040, 360, 20, 0, 0, 0, Gravity.CENTER);
        new SizeUtil(context).setLinearLayout(subjectHolder.profile_image, 430, 500, 0, 10, 10, 0, Gravity.CENTER);
        new SizeUtil(context).setLinearLayout(subjectHolder.deleteButton, 90, 90, 20, 10, 10, 0, 0);


        subjectHolder.headingTv.setText(data.get(position).getPro_name());
        if(price==1) {
            subjectHolder.priceTv.setText(" ₹ " + data.get(position).getPublix_price1());
            subjectHolder.descriptionTv.setTextColor(Color.parseColor("#fd1d53"));
        }else if(price==2) {
            subjectHolder.priceTv.setText(" ₹ " + data.get(position).getPublix_price2());
            subjectHolder.tvname2.setTextColor(Color.parseColor("#fd1d53"));
        }if(price==3) {
            subjectHolder.priceTv.setText(" ₹ " + data.get(position).getPublix_price3());
            subjectHolder.tvname3.setTextColor(Color.parseColor("#fd1d53"));
        }


//        subjectHolder.priceTv.setText(" ₹ "+price);
//        System.out.println("priceCart"+price);

        Glide.with(context).load(data.get(position).getPro_img()).error(R.mipmap.ic_launcher).into(subjectHolder.profile_image);
        System.out.println("CART : ### " + data.get(position).getPro_img());
//        subjectHolder.priceTv.setVisibility(View.VISIBLE);
        subjectHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.click(position);
            }
        });

//        subjectHolder.descriptionTv.setTextColor(Color.parseColor("#fd1d53"));
        subjectHolder.descriptionTv.setText(data.get(position).getName1());
        subjectHolder.tvname2.setText(data.get(position).getName2());
        subjectHolder.tvname3.setText(data.get(position).getName3());

        if (TextUtils.isEmpty(SelectedProduct.getName1())) {
            subjectHolder.descriptionCard.setVisibility(View.GONE);
        } else {
            subjectHolder.descriptionCard.setVisibility(View.VISIBLE);

        }
        if (TextUtils.isEmpty(SelectedProduct.getName2())) {
            subjectHolder.name2.setVisibility(View.GONE);
        } else {
            subjectHolder.name2.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(SelectedProduct.getName3())) {
            subjectHolder.name3.setVisibility(View.GONE);
        } else {
            subjectHolder.name3.setVisibility(View.VISIBLE);
        }

        subjectHolder.descriptionCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subjectHolder.descriptionTv.setTextColor(Color.parseColor("#fd1d53"));
                subjectHolder.tvname2.setTextColor(Color.parseColor("#111111"));
                subjectHolder.tvname3.setTextColor(Color.parseColor("#111111"));
                subjectHolder.priceTv.setText(data.get(position).getPublix_price1());
                subjectHolder.priceTv.setVisibility(View.VISIBLE);
                subjectHolder.priceTv1.setVisibility(View.GONE);
                subjectHolder.priceTv2.setVisibility(View.GONE);
                click.onName1(Integer.parseInt(data.get(position).getPro_id()));
                new DbCart(context).getAllBookmark().get(position).getPro_id();
                notifyDataSetChanged();
;
            }
        });
        subjectHolder.name2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subjectHolder.tvname2.setTextColor(Color.parseColor("#fd1d53"));
                subjectHolder.tvname3.setTextColor(Color.parseColor("#111111"));
                subjectHolder.descriptionTv.setTextColor(Color.parseColor("#111111"));
                subjectHolder.priceTv1.setText(data.get(position).getPublix_price2());
//                price=subjectHolder.priceTv.getText().toString();
                subjectHolder.priceTv1.setVisibility(View.VISIBLE);
                subjectHolder.priceTv.setVisibility(View.GONE);
                subjectHolder.priceTv2.setVisibility(View.GONE);
                click.onName2(Integer.parseInt(data.get(position).getPro_id()));
                new DbCart(context).getAllBookmark().get(position).getPro_id();
                notifyDataSetChanged();
            }
        });
        subjectHolder.name3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subjectHolder.tvname3.setTextColor(Color.parseColor("#fd1d53"));
                subjectHolder.tvname2.setTextColor(Color.parseColor("#111111"));
                subjectHolder.descriptionTv.setTextColor(Color.parseColor("#111111"));
                subjectHolder.priceTv2.setText(data.get(position).getPublix_price3());
                subjectHolder.priceTv2.setVisibility(View.VISIBLE);
                subjectHolder.priceTv.setVisibility(View.GONE);
                subjectHolder.priceTv1.setVisibility(View.GONE);
                click.onName3(Integer.parseInt(data.get(position).getPro_id()));
                new DbCart(context).updateQuantity(data.get(position).getPro_id(),""+ItemName);
                notifyDataSetChanged();
            }
        });
//         ItemName =subjectHolder.priceTv.getText().toString();
//         ItemName1 =subjectHolder.priceTv1.getText().toString();
//         ItemName2 =subjectHolder.priceTv2.getText().toString();
//        String qty = subjectHolder.quantityTv.getText().toString();
//        Intent intent = new Intent("custom-message");
//        //            intent.putExtra("quantity",Integer.parseInt(quantity.getText().toString()));
//        intent.putExtra("quantity",qty);
//        intent.putExtra("item",ItemName);
//        intent.putExtra("item1",ItemName1);
//        intent.putExtra("item2",ItemName2);
//        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        subjectHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.onClickDelete(position);
                new DbCart(context).deleteBookmark(data.get(position).getPro_id());
                data.remove(position);
                notifyDataSetChanged();
            }
        });

        subjectHolder.plusQuantityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int totalQuantity = Integer.parseInt(subjectHolder.quantityTv.getText().toString()) + 1;
                subjectHolder.quantityTv.setText("" + totalQuantity);
                new DbCart(context).updateQuantity(data.get(position).getPro_id(), "" + totalQuantity);
                for (int i = 0; i < new DbCart(context).getAllBookmark().size(); i++) {
                    System.out.println("YOS CART : " + new DbCart(context).getAllBookmark().get(i).getQty());
                }
                click.onClickPlus(position);
            }
        });
        subjectHolder.minusQuantityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!new PrefHelper(context).get_STRING(Constant.getcoupon, "0").equals("0")) {
                    new AlertDialog.Builder(context)
                            .setTitle("Title")
                            .setMessage("Do you want to remove coupon.?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    new PrefHelper(context).save_STRING(Constant.getcoupon, "0");
                                    click.onCouponDelete(position);
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                } else {
                    if (Integer.parseInt(subjectHolder.quantityTv.getText().toString()) == 1) {

                    } else {
                        int totalQuantity = Integer.parseInt(subjectHolder.quantityTv.getText().toString()) - 1;
                        subjectHolder.quantityTv.setText("" + totalQuantity);
                        new DbCart(context).updateQuantity(data.get(position).getPro_id(), "" + totalQuantity);
                        click.onClickMinus(position);
                    }
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        TextView headingTv, priceTv, quantityTv, deleteButton, priceTv1, priceTv2;
        LinearLayout mainLayout;
        ImageView profile_image, alphaThumb, deleteBtn, plusQuantityBtn, minusQuantityBtn;
        LinearLayout mainCard;
        TextView productNameTv, productPriceTv, descriptionTv, tvname2, tvname3, originalAmount;
        LinearLayout descriptionCard, name2, name3;

        public GalleryItemHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            mainCard = itemView.findViewById(R.id.mainCard);
            headingTv = itemView.findViewById(R.id.headingTv);
            profile_image = itemView.findViewById(R.id.profile_image);
            alphaThumb = itemView.findViewById(R.id.alphaThumb);
            priceTv = itemView.findViewById(R.id.priceTv);
            priceTv1 = itemView.findViewById(R.id.priceTv1);
            priceTv2 = itemView.findViewById(R.id.priceTv2);
            deleteBtn = itemView.findViewById(R.id.deleteBtn);
            plusQuantityBtn = itemView.findViewById(R.id.plusQuantityBtn);
            minusQuantityBtn = itemView.findViewById(R.id.minusQuantityBtn);
            quantityTv = itemView.findViewById(R.id.quantityTv);
            deleteButton = itemView.findViewById(R.id.deleteButton);

            descriptionTv = itemView.findViewById(R.id.descriptionTv);
            productNameTv = itemView.findViewById(R.id.productNameTv);
            productPriceTv = itemView.findViewById(R.id.productPriceTv);
            descriptionCard = itemView.findViewById(R.id.descriptionCard);

            name2 = itemView.findViewById(R.id.name2);
            name3 = itemView.findViewById(R.id.name3);
            tvname2 = itemView.findViewById(R.id.tvname2);
            tvname3 = itemView.findViewById(R.id.tvname3);
            originalAmount = itemView.findViewById(R.id.originalAmount);


        }

    }
    public  Integer getTotalAmount(){
        return totalAmount;
    }

}