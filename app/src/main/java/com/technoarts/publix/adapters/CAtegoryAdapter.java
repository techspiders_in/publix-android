package com.technoarts.publix.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.technoarts.publix.R;
import com.technoarts.publix.pojo.CategoryPojo;

import java.util.ArrayList;

public class CAtegoryAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<CategoryPojo> data;
    onClicFilter click;

    public CAtegoryAdapter(Context context, ArrayList<CategoryPojo> data, onClicFilter click) {
        this.context = context;
        this.data = data;
        this.click = click;
    }

    public interface onClicFilter {
        void onClick(int pos);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.category_adapter, parent, false);
        return new GalleryItemHolder(row);
    }
     int sdk = android.os.Build.VERSION.SDK_INT;
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final GalleryItemHolder subjectHolder = (GalleryItemHolder) holder;

        subjectHolder.headingTv.setText(data.get(position).getCat_name());

        if (data.get(position).isSelected())
           {
               if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                   subjectHolder.iconImage.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_dark) );
                   subjectHolder.mainCard.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.rounded_yellow) );
               } else {
                   subjectHolder.iconImage.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_dark));
                   subjectHolder.mainCard.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_yellow));
               }
           }
        else
        {
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                subjectHolder.iconImage.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_bg) );
                subjectHolder.mainCard.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_dark));
            } else {
                subjectHolder.iconImage.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_bg));
                subjectHolder.mainCard.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_dark));
            }
        }

        Glide.with(context).load(data.get(position).getIcon_img()).into(subjectHolder.iconImage);

        subjectHolder.mainCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        TextView headingTv;
        ImageView iconImage;
        LinearLayout mainCard;

        public GalleryItemHolder(View itemView) {
            super(itemView);
            headingTv = itemView.findViewById(R.id.headingTv);
            iconImage = itemView.findViewById(R.id.iconImage);
            mainCard = itemView.findViewById(R.id.mainCard);

        }
    }

}