package com.technoarts.publix.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technoarts.publix.R;
import com.technoarts.publix.paginationAdapter.CategoryWithPagination;
import com.technoarts.publix.pojo.ProductPojo;

import java.util.List;

public class MainCategoryRecyclerAdapter extends RecyclerView.Adapter<MainCategoryRecyclerAdapter.ViewHolder> {
   List<ProductPojo> listdata;
    Context context;
    int position;
    CategoryWithPagination.onClicFilter click;

    public MainCategoryRecyclerAdapter(List<ProductPojo> listdata, Context context, CategoryWithPagination.onClicFilter click) {
        this.listdata = listdata;
        this.context = context;
        this.click = click;
    }

    public interface onClicFilter {
        void onClick(int pos);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.category_row_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        final ProductPojo myListData = listdata.get(position);
        viewHolder.text.setText(myListData.getPro_name());
        viewHolder.tvdiscount.setText("₹ " + myListData.getPrice1());
        viewHolder.tvmrp.setPaintFlags(viewHolder.tvmrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        viewHolder.tvmrp.setText("₹ " + myListData.getPublix_price1());
//        viewHolder.tvdetails.setText(myListData.getPro_desc());

//        ((ProductWithPagination.MovieHolder) holder).headingTv.setText(movies.get(position).getPro_name());
//        ((ProductWithPagination.MovieHolder) holder).originalAmt.setText("₹ " + movies.get(position).getPro_mrp_ammount());
//        ((ProductWithPagination.MovieHolder) holder).description.setText(movies.get(position).getPro_sale());
//        ((ProductWithPagination.MovieHolder) holder).priceTv.setText("₹ " + movies.get(position).getPro_amount());

        Glide.with(context).load(myListData.getPro_img().split(",")[0]).into(viewHolder.image);

        System.out.println("CatName" + myListData.getPro_name());

//        Log.e("name",myListData.getCat_name());
//        Uri uri= Uri.parse(myListData.getImage());
        viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                click.onClick(position);
//                Intent intent=new Intent(context, CatItemAct.class);
//                intent.putExtra("name",myListData.getCategoryName());
//                intent.putExtra("id",listdata.get(position).getCategoryId());
//                intent.putExtra("CatName", position);
//                context.startActivity(intent);
//                Toast.makeText(view.getContext(),"click on item: "+myListData.getCategoryName(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView text,tvmrp,tvdiscount,tvdetails;
        public LinearLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.categoryimage);
            text = itemView.findViewById(R.id.categoryName);
            tvmrp = itemView.findViewById(R.id.ffMrp);
            tvdiscount = itemView.findViewById(R.id.ffdiscount);
            tvdetails = itemView.findViewById(R.id.ffdetail);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
        }
    }
}
