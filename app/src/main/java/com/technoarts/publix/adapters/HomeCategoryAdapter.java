package com.technoarts.publix.adapters;


import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.technoarts.publix.R;
import com.technoarts.publix.helpers.SizeUtil;
import com.technoarts.publix.pojo.CategoryPojo;

import java.util.ArrayList;

public class HomeCategoryAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<CategoryPojo> data;
    onClicFilter click;

    public HomeCategoryAdapter(Context context, ArrayList<CategoryPojo> data, onClicFilter click) {
        this.context = context;
        this.data = data;
        this.click = click;
    }

    public interface onClicFilter {
        void onClick(int pos);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.home_category_adapter, parent, false);
        return new GalleryItemHolder(row);
    }

    int sdk = android.os.Build.VERSION.SDK_INT;

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final GalleryItemHolder subjectHolder = (GalleryItemHolder) holder;
        subjectHolder.headingTv.setText(data.get(position).getCat_name());
        new SizeUtil(context).setLinearLayout(subjectHolder.mainCard, 520, 350, 20, 20, 20, 0, Gravity.CENTER);
        new SizeUtil(context).setLinearLayout(subjectHolder.iconImage, 170, 170, 0, 0, 0, 0, Gravity.CENTER);


        Glide.with(context)
                .applyDefaultRequestOptions(new RequestOptions()
                        .placeholder(R.drawable.launcher)
                        .error(R.drawable.launcher))
                .load(data.get(position).getImg())
                .into(subjectHolder.iconImage);


        subjectHolder.mainCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryItemHolder extends RecyclerView.ViewHolder {
        TextView headingTv;
        ImageView iconImage;
        LinearLayout mainCard;

        public GalleryItemHolder(View itemView) {
            super(itemView);
            headingTv = itemView.findViewById(R.id.headingTv);
            iconImage = itemView.findViewById(R.id.iconImage);
            mainCard = itemView.findViewById(R.id.mainCard);

        }
    }

}